package base

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

// UTC is a 7-byte array to store UTC

//	Position  Range   Values     Description
//	========  ======  =========  ===============
//	       0  10-99   2010-2099  Year
//         1  1-12    jan-dec    Month of year
//	       2  1-31    1-31       Day of month
//	       3  0-23    0-23       Hour of the day
//         4  0-59    0-59       Minute
//         5  0-59    0-59       Second
//         6  0-99    0-99       Cents of second
type UTC []byte

func NewUTCTime(t time.Time) (UTC, error) {
	// maximum resolution of 1 cent of second
	t = t.Truncate(10 * time.Millisecond)
	return NewUTC(
		byte(t.Year()%100),
		byte(t.Month()), 
		byte(t.Day()),
		byte(t.Hour()), 
		byte(t.Minute()), 
		byte(t.Second()), 
		byte(t.Nanosecond() / 10000000),
	)
}

// UTCNow returns a UTC of current time.
func UTCNow() (UTC, error) {
	t := time.Now().UTC()
	return NewUTCTime(t)
}

var (
	invalidYear  = fmt.Errorf("Invalid year")
	invalidMonth = fmt.Errorf("Invalid month")
	invalidDay   = fmt.Errorf("Invalid day of month")
	invalidHrs   = fmt.Errorf("Invalid hours")
	invalidMins  = fmt.Errorf("Invalid minutes")
	invalidSecs  = fmt.Errorf("Invalid seconds")
	invalidCents = fmt.Errorf("Invalid cents of seconds")
	invalidYMD   = fmt.Errorf("Invalid year-month-day")
	invalidTime  = fmt.Errorf("Invalid time")

	hex = []byte{
		'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f',
	}
)

// NewUTC builds a Time UTC with small arguments whithin these ranges:
//	- y (0 - 99) represent years from 2000 to 2099.
//	- m (1 - 12) represent january to december.
//	- d (1 - 31) represent day of month.
//	- hrs (0 - 23) represent hours.
//	- min (0 - 59) represent minutes.
//	- sec (0 - 59) represent seconds.
//	- min (0 - 99) represent cents of seconds.
func NewUTC(y, m, d, hrs, min, sec, cent byte) (utc UTC, err error) {
	if y > 100 {
		err = invalidYear
	
	} else if m < 1 || m > 12 {
		err = invalidMonth
	
	} else if d < 1 || d > 31 {
		err = invalidDay
	
	} else if hrs > 23 {
		err = invalidHrs
	
	} else if min > 59 {
		err = invalidMins
	
	} else if sec > 59 {
		err = invalidSecs
	
	} else if cent > 99 {
		err = invalidCents
	
	} else {
		utc = UTC([]byte{
			byte(y),
			byte(m), 
			byte(d),
			byte(hrs),
			byte(min),
			byte(sec),
			byte(cent),
		})
	}
	return
}

// NewUTCYMD returns a UTC build from stringed year-month-date
// usually in filesystem databases and individual bytes for hours.
// minutes, seconds and seconds/100.
func NewUTCYMD(ymd string, hrs, min, sec, cent byte) (utc UTC, e error) {
	if len(ymd) != 6 {
		e = invalidYMD
	
	} else if y, err := strconv.Atoi(ymd[:2]); err != nil {
		e = invalidYear

	} else if m, err := strconv.Atoi(ymd[2:4]); err != nil {
		e = invalidMonth

	} else if d, err := strconv.Atoi(ymd[4:]); err != nil {
		e = invalidDay

	} else {
		utc, e = NewUTC(byte(y), byte(m), byte(d), hrs, min, sec, cent)
	}
	return
}

func (t *UTC) MarshalJSON() ([]byte, error) {
	if len(*t) != 7 {
		return nil, fmt.Errorf("Invalid UTC")
	}
	s := fmt.Sprintf("%02d%02d%02d%02d%02d%02d.%02d", 
		(*t)[0],
		(*t)[1],
		(*t)[2],
		(*t)[3],
		(*t)[4],
		(*t)[5],
		(*t)[6],
	)
	return []byte(s), nil
}

func (u *UTC) UnmarshalJSON(data []byte) (e error) {
	s := string(data)
	if len(s) != 15 { // yyyymmddhhmmss.cc
		e = invalidTime
		return
	}
	if y, err := strconv.Atoi(s[:2]); err != nil {
		e = invalidYear
	
	} else if m, err := strconv.Atoi(s[2:4]); err != nil {
		e = invalidMonth
	
	} else if d, err := strconv.Atoi(s[4:6]); err != nil {
		e = invalidDay
	
	} else if h, err := strconv.Atoi(s[6:8]); err != nil {
		e = invalidHrs
	
	} else if min, err := strconv.Atoi(s[8:10]); err != nil {
		e = invalidMins
	
	} else if sec, err := strconv.Atoi(s[10:12]); err != nil {
		e = invalidSecs
	
	} else if cent, err := strconv.Atoi(s[13:]); err != nil {
		e = invalidCents
	
	} else if utc, err := NewUTC(
		byte(y), byte(m), byte(d),
		byte(h), byte(min), byte(sec),
		byte(cent),
	); err != nil {
		e = err
	} else {
		*u = utc
	}
	return
}

// YMD returns a string of the form yymmdd where
//	- yy is a valid year 10 - 99 representing years 2010 to 2099
//	- mm is a valid month from 1 to 12
//	- dd is a valid day of month from 1 to 31
func (t UTC) YMD() string {
	//return fmt.Sprintf("%02d%02d%02d", t[0], t[1], t[2])
	return string([]byte{
		hex[t[0] / 10], // year-decade
		hex[t[0] % 10], // year-unit
		hex[t[1] / 10], // month-decade
		hex[t[1] % 10], // month-unit
		hex[t[2] / 10], // day-decade
		hex[t[2] % 10], // day-unit
	})
}

// Clock returns four bytes:
// hour, minute, second and second/100.
func (t UTC) Clock() (byte, byte, byte, byte) {
	return t[3], t[4], t[5], t[6]
}

// HMSC returns a string of the form hhmmss.cc where
//	- hh is a valid hour 0 - 23
//	- mm is a valid minute from 0 - 59
//	- ss is a valid second from 0 - 59
//  - cc is a valid second/100 from 0 - 99
func (t UTC) HMSC() string {
	//return fmt.Sprintf("%02d%02d%02d.%02d", t[3], t[4], t[5], t[6])
	return string([]byte{
		hex[t[3] / 10], // hour-decade
		hex[t[3] % 10], // hour-unit
		hex[t[4] / 10], // minute-decade
		hex[t[4] % 10], // minute-unit
		hex[t[5] / 10], // second-decade
		hex[t[5] % 10], // second-unit
		'.',
		hex[t[6] / 10], // second/100-decade
		hex[t[6] % 10], // second/100-unit
	})
}

func (u UTC) Equal(other UTC) bool {
	return bytes.Compare(u, other) == 0
}

func (u UTC) Clone() UTC {
	u2 := make([]byte, len(u))
	copy(u2, u)
	return UTC(u2)
}



type Time struct {
	time.Duration
}

func NewTime(d time.Duration) *Time {
	return &Time{
		Duration: d,
	}
}

func (t Time) MarshalJSON() ([]byte, error) {
    return json.Marshal(t.String())
}

func (t *Time) UnmarshalJSON(b []byte) error {
    var v interface{}
    if err := json.Unmarshal(b, &v); err != nil {
        return err
    }
    switch value := v.(type) {
    case string:
        var err error
        t.Duration, err = time.ParseDuration(value)
        if err != nil {
            return err
        }
        return nil
    default:
        return fmt.Errorf("invalid time")
    }
}

func (t *Time) LowerThan(d time.Duration) bool {
	return t.Duration < d
}

func (t *Time) GreaterThan(d time.Duration) bool {
	return t.Duration > d
}



