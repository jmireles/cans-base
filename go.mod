module gitlab.com/jmireles/cans-base

go 1.18

require (
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sync v0.0.0-20220929204114-8fcdb60fdcc0
	golang.org/x/sys v0.5.0
	gotest.tools/v3 v3.4.0
)

require github.com/google/go-cmp v0.5.5 // indirect
