package base

import (
	"encoding/json"
	"fmt"
	"testing"
	//"time"
)


func TestBaseUTC(t *testing.T) {
	tt := &Test{t}
	type T struct {
		Int    int    `json:"i"`
		Utc    UTC    `json:"utc"`
		String string `json:"s"`
	}

	// real now
	now, _ := UTCNow()
	now1 := &T{ Int:100, Utc:now, String:"string" }
	_, err := json.Marshal(now1)
	tt.Ok(err)
    // generate new now from real
    var now2 T
    now3 := now1.Utc.YMD() + now1.Utc.HMSC()
    j := fmt.Sprintf(`{"i":100,"utc":%s,"s":"string"}`, now3)
    err = json.Unmarshal([]byte(j), &now2)
    tt.Ok(err)
    tt.Assert(now1.Utc.Equal(now2.Utc), "Expected:%02x, got:%02x", now1.Utc, now2.Utc)

    t.Run("bad", func(t *testing.T) {
	    for _, bad := range []string {
	    	// invalid sizes
	    	"123",
	    	"11111111111111111111",
	    	"991231235959.9",
	    	"991231235959.999",
	    	// invalid years
	    	"--1231235959.99",
	    	"091231235959.99",
	    	// invalid months
	    	"10--31235959.99",
	    	"100031235959.99",
	    	"101331235959.99",
	    	// invalid days
	    	"1012--235959.99",
	    	"101200235959.99",
	    	"101232235959.99",
	    	// invalid hrs
	    	"101231--5959.99",
	    	"101231245959.99",
	    	// invalid min
	    	"10123159--59.99",
	    	"101231236059.99",
	    	// invalid sec
	    	"1012315959--.99",
	    	"101231235960.99",
	    	// invalid cents
	    	"101231235959.--",
	    } {
	    	var test T
	    	j := fmt.Sprintf(`{"utc":%s}`, bad)
		    err := json.Unmarshal([]byte(j), &test)
		    tt.Assert(err != nil, "bad accepted:%s", bad)
	    }
	})
	t.Run("good", func(t *testing.T) {
    	var got T
    	min, _ := NewUTC(10, 1, 1, 0, 0, 0, 0)
    	mid, _ := NewUTC(14, 6, 5, 12, 34, 56, 78)
    	max, _ := NewUTC(99, 12, 31, 23, 59, 59, 99)
		for good, exp := range map[string]UTC {
			"100101000000.00": min,
			"140605123456.78": mid,
			"991231235959.99": max,
		} {
	    	j := fmt.Sprintf(`{"utc":%s}`, good)
		    err := json.Unmarshal([]byte(j), &got)
		    tt.Ok(err)
		    tt.Assert(exp.Equal(got.Utc), "Exp:%v, got:%v", exp, got)
		}
	})
}

