# cans-base

Packages of `cans-base` repo

```
┌────────────────┬─────────┬─────────────┐
│  ┌────────┐ ┌──┴──┐      │   cans-base │
│  │ socket │ │ dom │      │             │
│  └───┬────┘ └─────┘      │             │
│  ┌───│───────────────┬───┴───────────┐ │
│  │   │               │          milk │ │
│  │   │    ┌──────┬───┴──┬──────┐     │ │
│  │   │  ┌─┴──┐ ┌─┴──┐ ┌─┴──┐ ┌─┴──┐  │ │
│  │   │  │ i4 │ │ p1 │ │ rs │ │ t4 │  │ │
│  │   │  └─┬──┘ └─┬──┘ └─┬──┘ └─┬──┘  │ │
│  │   │    └──────┴───┬──┴────┬─┘     │ │
│  │   │               │    ┌──┴──┐    │ │
│  │   │               │    │  to │    │ │
│  │   │               │    └─┬─┬─┘    │ │
│  │   │               │  ┌───┘ │      │ │ 
│  │   │             ┌─┴──┴─┐   │      │ │
│  │   │             │ from │   │      │ │
│  │   │             └──┬───┘   │      │ │
│  │   │ ┌──────────────┴───┬───┘      │ │ 
│  │ ┌─┴─┴─┐           ┌────┴────┐     │ │
│  │ │ srv │           │   line  │     │ │ 
│  │ └──┬──┘           └──┬─────┬┘     │ │
│  │    └───┬─────────────┘ ┌───┴──┐   │ │
│  │        │               │ recs │   │ │
│  │        │               └──────┘   │ │
│  └────────│──────────────────────────┘ │
│        ┌──┴───┐                        │
│        │ sock │                        │
│        └──────┘                        │
└────────────────────────────────────────┘
```

`cans-base` is the first layer of boumatic CAN equipment protocols.

Boumatic lines are defined with four-bits.

| Line | Binary | Description             | Code
|------|--------|-------------------------|-------
| PC   | 0000   | computer                | 
| P1   | 0001   | pulsator monitor        | [P1 <-> PC](milk/p1)
| TO   | 0010   | single takeoff          |
| MM   | 0011   | milk-meter              |
| RF   | 0100   | rfid                    |
| SO   | 0101   | sorter                  | 
| RS   | 0110   | rotary stall identifier | [RS <-> PC](milk/rs)
| T4   | 0111   | four-stalls stakeoffs   | [T4 <-> PC](milk/t4)
| I4   | 1000   | four-digital inputs     | [I4 <-> PC](milk/i4)

Every Boumatic CAN message has 4-bits for the source line and 4-bits of the destination line.

Every Boumatic CAN message also has 8-bits of the sender ID and 8-bits of the destination ID.

Finally every Boumatic CAN message has a 5-bits command and 0-8 extra bytes of payload.

This repo defines all the 5-byte commands send between line=PC and lines P1,RS,T4,I4.

For every 5-byte command is defined the number of bytes of the payload but not the details.

The details of payload content is defined in other repo [cans-pack](../cans-pack).
