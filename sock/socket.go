package sock

import (
	"fmt"
)

type Socket struct {
	Type string
	Dev  string
	Net  byte
}

func NewSocketVcan(dev string, net byte) *Socket {
	return &Socket{
		Type: "vcan",
		Dev:  dev, // examples vcan0, vcan1...
		Net:  net,
	}
}

func NewSocketCan(dev string, net byte) *Socket {
	return &Socket{
		Type: "can",
		Dev:  dev, // examples can0, can1...
		Net:  net,
	}
}

func (s *Socket) Use() string {
	return fmt.Sprintf("sudo modprobe %s", s.Type) +
		fmt.Sprintf("; sudo ip link add dev %s type %s", s.Dev, s.Type) +
		fmt.Sprintf("; sudo ip link set up %s", s.Dev)
}


