package sock

import (
	"bytes"
	"encoding/binary"
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/line"
)

const (
	MaxFrameDataLength = 8
)

// Frame represents a standard CAN data frame
type Frame struct {
	// bit 0-28: CAN identifier (11/29 bit)
	// bit 29: error message flag (ERR)
	// bit 30: remote transmision request (RTR)
	// bit 31: extended frame format (EFF)
	ID     uint32
	Length uint8
	Flags  uint8
	Res0   uint8
	Res1   uint8
	Data   [MaxFrameDataLength]uint8
}

func NewFrameData(data milk.Data) (*Frame, error) {
	if can, err := line.CanTx(data); err != nil {
		return nil, err
	} else {
		return NewFrame(can), nil
	}
}

func NewFrame(can *line.Can) *Frame {
	id := can.ID
	if can.IsExtended {
		id |= 0x80000000
	}
	if can.IsRemote {
		id |= 0x40000000
	}
	f := &Frame{
		ID:     id,
		Length: can.Length,
	}
	copy(f.Data[:], can.Extra[:])
	return f
}

func (f *Frame) String() string {
	format := "%04x %d %d %d %d % 02x"
	return fmt.Sprintf(format, f.ID, f.Length, f.Flags, f.Res0, f.Res1, f.Data)
}

func (f *Frame) Can() (*line.Can, error) {
	id := uint32(0)
	ext := f.ID & 0x80000000 != 0 // bit 31: extended frame format (EFF)
	rem := f.ID & 0x40000000 != 0 // bit 30: remote transmision request (RTR)
	err := f.ID & 0x20000000 != 0 // bit 29: error message flag (ERR)
	if ext {
		id = f.ID & 0x1fffffff
	} else {
		return nil, fmt.Errorf("Frame is not 29 bits %v", f)
	}
	if err {
		return nil, fmt.Errorf("Frame is error %v", f)
	} 
	if rem { 
		// TODO when remote???
		//t.Logf("Frame is remote %v", f)
	} 
	can := &line.Can{
		ID:         id,
		Length:     f.Length,
		Extra:      base.Extra(f.Data),
		IsExtended: ext,
	}
	if err := can.Validate(); err != nil {
		return nil, err
	} else {
		return can, nil
	}
}


// frameMarshal returns the byte encoding of frm.
func frameMarshal(frm Frame) (b []byte, err error) {
	wr := errWriter{
		buf: bytes.NewBuffer([]byte{}),
	}
	wr.write(&frm.ID)
	wr.write(&frm.Length)
	wr.write(&frm.Flags)
	wr.write(&frm.Res0)
	wr.write(&frm.Res1)
	wr.write(&frm.Data)
	return wr.buf.Bytes(), wr.err
}

// frameUnmarshal parses the bytes b and stores the result in the value
// pointed to by frm.
func frameUnmarshal(b []byte, frm *Frame) (err error) {
	cr := &errReader{
		buf: bytes.NewBuffer(b),
	}
	cr.read(&frm.ID)
	cr.read(&frm.Length)
	cr.read(&frm.Flags)
	cr.read(&frm.Res0)
	cr.read(&frm.Res1)
	cr.read(&frm.Data)
	return cr.err
}

type errReader struct {
	buf *bytes.Buffer
	err error
}

func (r *errReader) read(v interface{}) {
	if r.err == nil {
		r.err = binary.Read(r.buf, binary.LittleEndian, v)
	}
}

type errWriter struct {
	buf *bytes.Buffer
	err error
}

func (wr *errWriter) write(v interface{}) {
	if wr.err == nil {
		wr.err = binary.Write(wr.buf, binary.LittleEndian, v)
	}
}

// The Handler interfaces defines a method to receive a frame.
type Handler interface {
	Handle(frame Frame)
}

// HandlerFunc defines the function type to handle a frame.
type HandlerFunc func(frame Frame)

type handler struct {
	fn HandlerFunc
}

// NewHandler returns a new handler which calls fn when a frame is received.
func NewHandler(fn HandlerFunc) Handler {
	return &handler{fn}
}

func (h *handler) Handle(frame Frame) {
	h.fn(frame)
}

