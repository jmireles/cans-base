package sock

import (
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/srv"
)

func TestVcanSimple(t *testing.T) {
	log := func(mess string) {
		t.Log(mess)
	}
	sock := NewSocketVcan("vcan0", 1)
	bus, err := NewMessBus(sock, log)
	if err != nil {
		t.Logf("%s%s%s", base.Red, sock.Use(), base.Reset)
		t.Skip(err)
	}
	// parse requests get/set
	bus.Subscribe(func(mess *Mess, publish func(*Mess)) error {
		net := mess.Net
		can := mess.Can
		t.Logf("net=%d can=%v", net, can)

		// ┌────────┬────────┬────────┬────┬──┬──┬──┬──┬──┬──┬──┬──┐
		// │0000LLLL     D        0    comm xx xx xx xx xx xx xx xx│
		// └────────┴────────┴────────┴────┴──┴──┴──┴──┴──┴──┴──┴──┘
		resp1, _ := line.CanTx([]byte{ 0x01, 7, 0, 3, 1, 0x10, 0x11 })
		resp2, _ := line.CanTx([]byte{ 0x01, 7, 0, 3, 2, 0x10, 0x11 })
		resp3, _ := line.CanTx([]byte{ 0x01, 7, 0, 3, 3, 0x10, 0x11 })
		resp4, _ := line.CanTx([]byte{ 0x01, 7, 0, 3, 4, 0x10, 0x11 })
		for _, can := range []*line.Can{
			resp1,
			resp2,
			resp3,
			resp4,
		} {
			publish(&Mess{ Net:net, Can:can })
			time.Sleep(100 * time.Millisecond)
		}
		return nil
	})
	// parse unsol responses
	//s.sim.SetUnsolFunc(vcanNet.UnsolFunc)
}

func TestVcanUnsols(t *testing.T) {
	net := byte(1)
	sock := NewSocketVcan("vcan0", net)
	bus, err := NewMessBus(sock, func(mess string) {
		t.Logf("%s%s%s", base.Blue, mess, base.Reset)
	})
	if err != nil {
		// skip on error print how to activate it
		//fmt.Printf("%s%v%s", base.Yellow, err, base.Reset)
		t.Logf("%s%s%s", base.Red, sock.Use(), base.Reset)
		t.Skip(err)
		return
	}
	tt := base.Test{t}

	bus.Subscribe(func(mess *Mess, publish func(*Mess)) error {
		t.Log("mess", mess)
		return nil
	})
	pm7, err := from.NewSrc(net, from.P1, 7)
	tt.Ok(err)

	type U struct { comm milk.Unsol; extra []byte }
	unsols := []U {
		U{ comm:milk.Unsol01_Restart    },
		U{ comm:p1.Unsol05_Idle         },
		U{ comm:p1.Unsol06_Working      },
		U{ comm:p1.Unsol07_Alarmed,     extra:[]byte{1,2} },
		U{ comm:p1.Unsol16_NoPhaseFront },
		U{ comm:p1.Unsol17_NoPhaseRear  },
		U{ comm:p1.Unsol26_Notify,      extra:[]byte{1,2,3,4} },
	}

	nUnsols := 0

	resp := make(chan from.Resp, 1)
	go func() {
		for {
			r := <- resp
			tt.Equals(net, byte(r.NetId()))
			line, err := r.From()
			tt.Ok(err)
			tt.Equals(from.P1, line)
			switch r.BoxId() {
			case 7:
				_, sol, unsol, err := r.Comm()
				tt.Ok(err)
				if sol != nil {
					t.Logf("sol=%d", *sol) // 15 send below
				}
				if unsol != nil {
					u := unsols[nUnsols]
					nUnsols++
					tt.Equals(u.comm, *unsol)
					t.Logf("unsol=%d extra=%v utc=%d", *unsol, u.extra, r.UTC())
				}
			default:
				t.Fatal("Wrong src")			
			}
		}
	}()
	
	socket := srv.NewSock("vcan0", resp)
	err = socket.Open(net)
	tt.Ok(err)

	// wrong can unsol
	sol, err := from.NewCan(pm7, 15, nil)
	tt.Ok(err)
	err = bus.PublishFrom(sol)
	//if err != nil {}
	//tt.Assert(err != nil, "accepted wrongPmUnsol")
	
	// send good pm7 unsols
	go func() {
		for _, u := range unsols {
			can, err := from.NewCanUnsol(pm7, u.comm, u.extra)
			tt.Ok(err)
			err = bus.PublishFrom(can)
			tt.Ok(err)
			time.Sleep(10 * time.Millisecond)
		}
	}()
	time.Sleep(1 * time.Second)
}

func TestVcanSols(t *testing.T) {
	net := byte(1)
	sock := NewSocketVcan("vcan0", net)
	bus, err := NewMessBus(sock, func(mess string) {
		t.Logf("%s%s%s", base.Blue, mess, base.Reset)
	})
	if err != nil {
		// skip on error print how to activate it
		//fmt.Printf("%s%v%s", base.Yellow, err, base.Reset)
		t.Logf("%s%s%s", base.Red, sock.Use(), base.Reset)
		t.Skip(err)
		return
	}
	tt := base.Test{t}
	pm7, err := from.NewSrc(net, from.P1, 7)
	tt.Ok(err)
	type sec struct { sol milk.Sol; extra []byte; can string }
	for _, s := range []*sec {
		&sec{ p1.Sol09_AvgPhaseRear,   []byte{1},               "020000E9#01"},
		&sec{ p1.Sol10_AvgLevelFront,  []byte{1,2},             "020000EA#0102"},
		&sec{ p1.Sol11_AvgLevelRear,   []byte{1,2,3},           "020000EB#010203"},
		&sec{ p1.Sol12_ValPhasesFront, []byte{1,2,3,4},         "020000EC#01020304"},
		&sec{ p1.Sol13_ValPhasesRear,  []byte{1,2,3,4,5},       "020000ED#0102030405"},
		&sec{ p1.Sol14_ValLevelsFront, []byte{1,2,3,4,5,6},     "020000EE#010203040506"},
		&sec{ p1.Sol15_ValLevelsRear,  []byte{1,2,3,4,5,6,7},   "020000EF#01020304050607"},
		&sec{ p1.Sol18_Calibr,         []byte{1,2,3,4,5,6,7,8}, "020000F2#0102030405060708"},
		&sec{ p1.Sol21_Options,        []byte{1,2,3,4,5,6,7},   "020000F5#01020304050607"},
		&sec{ p1.Sol22_Profiles,       []byte{1,2,3,4,5,6},     "020000F6#010203040506"},
		&sec{ p1.Sol23_Display,        []byte{1,2,3,4,5},       "020000F7#0102030405"},
		&sec{ p1.Sol24_Config,         []byte{1,2,3,4},         "020000F8#01020304"},
		&sec{ p1.Sol25_Alarms,         []byte{1,2,3},           "020000F9#010203"},
		&sec{ p1.Sol27_Stalls,         []byte{1,2},             "020000FB#0102"},
	} {
		if sol, err := from.NewCanSol(pm7, s.sol, s.extra); err != nil {
			t.Fatal(err)
		} else if frame, err := bus.fromFrame(sol); err != nil {
			t.Fatal(err)
		} else if can, err := frame.Can(); err != nil {
			t.Fatal(err)
		} else {
			tt.Equals(s.can, can.String())
		}
	}
}

