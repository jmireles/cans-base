package sock

import (
	"fmt"

	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
)


/*func MessUsage(err error, name string) string {
	return fmt.Sprintf(`vcan: %v
1) Check vcan is active:

  ip a | grep "vcan"

2) Empty? Do this once:

  sudo modprobe vcan
  sudo ip link add dev %s type vcan
  sudo ip link set up %s

`, err, name, name)
}*/

type Mess struct {
	Net byte
	Can *line.Can
}

type MessFunc func(*Mess, func(*Mess)) error

type MessBus struct {
	sock *Socket
	bus  *Bus
	log  func(string)
}

func NewMessBus(sock *Socket, log func(string)) (*MessBus, error) {
	if bus, err := NewBusInterfaceByName(sock.Dev); err != nil {
		return nil, err
	} else {
		go func() {
			err = bus.ConnectAndPublish()
			if log != nil {
				log(fmt.Sprintf("ABORT: %v", err))
			}
		}()
		return &MessBus{
			sock: sock,
			bus:  bus,
			log:  log,
		}, nil
	}
}


func (s *MessBus) Subscribe(messFunc MessFunc) {
	s.bus.SubscribeFunc(func(frame Frame) {
		if can, err := frame.Can(); err != nil {
			if s.log != nil {
				s.log(fmt.Sprintf("req frame CAN ERROR: %v %v", err, frame))
			}
		} else if err = messFunc(&Mess{ Net:s.sock.Net, Can:can }, s.sol); err != nil {
			if s.log != nil {
				s.log(fmt.Sprintf("req message ERROR: %v", err))
			}
		} else {
			if s.log != nil {
				s.log(fmt.Sprintf("req net:%d frame:%v", s.sock.Net, frame))
			}
		}
	})
}
 
// PublishFrom publishes to bus a CAN response solicited or unsolicited
func (s *MessBus) PublishFrom(can *from.Can) error {
	if frame, err := s.fromFrame(can); err != nil {
		return err
	} else {
		s.bus.Publish(*frame)
		if s.log != nil {
			s.log(fmt.Sprintf("from net:%d frame:%v", can.Src.Net, frame))
		}
		return nil
	}
}

// fromFrame converts a from.Can to a Frame
func (s *MessBus) fromFrame(can *from.Can) (*Frame, error) {
	net := can.Src.Net
	if s.sock.Net != byte(net) {
		return nil, fmt.Errorf("Invalid net:%d", net)
	}
	var bytes []byte
	if can.Unsol != nil {
		bytes = can.Src.Line.RespUnsolBytes(can.Src.Box, *can.Unsol, can.Extra)
	} else if can.Sol != nil {
		bytes = can.Src.Line.RespSolBytes(can.Src.Box, *can.Sol, can.Extra)
	} else {
		return nil, fmt.Errorf("Invalid command no unsol/sol")	
	}
	return s.bytesFrame(bytes)
}

// PublishBytes is published as a frame for valid solicited responses bytes
// including serials.
func (s *MessBus) PublishBytes(bytes []byte) error {
	if frame, err := s.bytesFrame(bytes); err != nil {
		return err
	} else {
		s.bus.Publish(*frame)
		if s.log != nil {
			s.log(fmt.Sprintf("bytes net:%d frame:%v", s.sock.Net, frame))
		}
		return nil
	}
}

// bytesFrame converts bytes to a Frame
func (s *MessBus) bytesFrame(bytes []byte) (*Frame, error) {
	if data, err := milk.NewData(bytes); err != nil {
		return nil, err
	} else {
		return NewFrameData(data)
	}
}

func (s *MessBus) sol(mess *Mess) {
	switch mess.Net {
	case s.sock.Net:
		frame := NewFrame(mess.Can)
		s.bus.Publish(*frame)
		if s.log != nil {
			s.log(fmt.Sprintf("sol net:%d frame:%v", s.sock.Net, frame))
		}
	}
}



