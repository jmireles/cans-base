package sock	

import (
	"io"
)

type readWriteCloser struct {
	rwc io.ReadWriteCloser
}

func (rwc *readWriteCloser) ReadFrame(frame *Frame) error {
	b := make([]byte, 32) // maybe 16 as MTU for vcan interface
	n, err := rwc.Read(b)
	if err != nil {
		return err
	}
	err = frameUnmarshal(b[:n], frame)
	return err
}

func (rwc *readWriteCloser) WriteFrame(frame Frame) error {
	b, err := frameMarshal(frame)
	if err != nil {
		return err
	}
	_, err = rwc.Write(b)
	return err
}

func (rwc *readWriteCloser) Read(b []byte) (n int, err error) {
	return rwc.rwc.Read(b)
}

func (rwc *readWriteCloser) Write(b []byte) (n int, err error) {
	return rwc.rwc.Write(b)
}

func (rwc *readWriteCloser) Close() error {
	return rwc.rwc.Close()
}





