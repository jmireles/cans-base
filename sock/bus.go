package sock

import (
	"fmt"
	"io"
	"net"
	"os"
	"syscall"

	"golang.org/x/sys/unix"
)

// Based on https://github.com/brutella/can

type Bus struct {
	rwc     readWriteCloser
	handler []Handler
}

func NewBusInterfaceByName(name string) (*Bus, error) {
	iface, err := net.InterfaceByName(name)
	if err != nil {
		return nil, err
	}
	s, _ := syscall.Socket(syscall.AF_CAN, syscall.SOCK_RAW, unix.CAN_RAW)
	addr := &unix.SockaddrCAN{
		Ifindex: iface.Index,
	}
	err = unix.Bind(s, addr)
	if err != nil {
		return nil, err
	}
	f := os.NewFile(uintptr(s), fmt.Sprintf("fd %d", s))
	rwc := readWriteCloser{f}
	return &Bus{
		rwc: rwc,
		handler: make([]Handler, 0),
	}, nil
}

// ConnectAndPublish starts handling CAN frames to publish them to handlers.
func (b *Bus) ConnectAndPublish() error {
	for {
		err := b.publishNextFrame()
		if err != nil {
			return err
		}
	}

	return nil
}

// Disconnect stops handling CAN frames.
func (b *Bus) Disconnect() error {
	return b.rwc.Close()
}

// Subscribe adds a handler to the bus.
func (b *Bus) Subscribe(handler Handler) {
	b.handler = append(b.handler, handler)
}

// SubscribeFunc adds a function as handler.
func (b *Bus) SubscribeFunc(fn HandlerFunc) {
	handler := NewHandler(fn)
	b.Subscribe(handler)
}

// Unsubscribe removes a handler.
func (b *Bus) Unsubscribe(handler Handler) {
	for i, h := range b.handler {
		if h == handler {
			b.handler = append(b.handler[:i], b.handler[i+1:]...)
			return
		}
	}
}

// Publish publishes a frame on the bus.
//
// Frames publishes with the Publish methods are not received by handlers.
func (b *Bus) Publish(frame Frame) error {
	return b.rwc.WriteFrame(frame)
}

func (b *Bus) contains(handler Handler) bool {
	for _, h := range b.handler {
		if h == handler {
			return true
		}
	}
	return false
}

func (b *Bus) publishNextFrame() error {
	frame := Frame{}
	err := b.rwc.ReadFrame(&frame)
	if err != nil {
		b.rwc.Close()
		if err != io.EOF {
			// EOF is not an error, it happens when calling rwc.Close()
			return err
		}
		return nil
	}

	b.publish(frame)

	return nil
}

func (b *Bus) publish(frame Frame) {
	for _, h := range b.handler {
		h.Handle(frame)
	}
}
