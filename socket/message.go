package socket

// Message is anything that can marshal and unmarshal itself to/from a CAN frame.
type Message interface {
	CanMarshaler
	CanUnmarshaler
}

// FrameMarshaler can marshal itself to a CAN frame.
type CanMarshaler interface {
	MarshalCan() (Can, error)
}

// FrameUnmarshaler can unmarshal itself from a CAN frame.
type CanUnmarshaler interface {
	UnmarshalCan(Can) error
}
