package socket

import (
	"bufio"
	"io"

	"gitlab.com/jmireles/cans-base/socket/errs"
)

type RxOption func(*rxOpts)

type rxOpts struct {
	frameInterceptor FrameInterceptor
}

type Rx struct {
	opts  rxOpts
	rc    io.ReadCloser
	sc    *bufio.Scanner
	frame frame
}

func NewRx(rc io.ReadCloser, opt ...RxOption) *Rx {
	opts := rxOpts{}
	for _, f := range opt {
		f(&opts)
	}
	sc := bufio.NewScanner(rc)
	sc.Split(scanFrames)
	return &Rx{
		rc:   rc,
		opts: opts,
		sc:   sc,
	}
}

func scanFrames(data []byte, _ bool) (int, []byte, error) {
	if len(data) < frameLen {
		// not enough data for a full frame
		return 0, nil, nil
	}
	return frameLen, data[0:frameLen], nil
}

func (r *Rx) Scan() bool {
	ok := r.sc.Scan()
	r.frame = frame{}
	if ok {
		r.frame.unmarshalBinary(r.sc.Bytes())
		if r.opts.frameInterceptor != nil {
			r.opts.frameInterceptor(r.frame.decode())
		}
	}
	return ok
}

func (r *Rx) HasErrorFrame() bool {
	return r.frame.isError()
}

func (r *Rx) Read() Can {
	return r.frame.decode()
}

func (r *Rx) ErrorFrame() errs.Frame {
	return r.frame.decodeErrorFrame()
}

func (r *Rx) Err() error {
	return r.sc.Err()
}

func (r *Rx) Close() error {
	return r.rc.Close()
}

// RxFrameInterceptor returns a RxOption that sets the
// FrameInterceptor for the rx. Only one frame interceptor
// can be installed.
func RxFrameInterceptor(i FrameInterceptor) RxOption {
	return func(o *rxOpts) {
		o.frameInterceptor = i
	}
}
