//go:build linux
// +build linux

package socket

import (
	"context"
	"fmt"
	"net"
	"os"

	"golang.org/x/sys/unix"
)

// Dial connects to the address on the named net.
//
// Linux only: If net is "can" it creates a SocketCAN connection to the device
// (address is interpreted as a device name).
//
// If net is "udp" it assumes UDP multicast and sets up 2 connections, one for
// receiving and one for transmitting.
// See: https://golang.org/pkg/net/#Dial
func Dial(network, address string) (net.Conn, error) {
	switch network {
	//case udp:
	//	return udpTransceiver(network, address)
	case canName:
		return dialRaw(address) // platform-specific
	default:
		return net.Dial(network, address)
	}
}

func DialCAN(address string) (net.Conn, error) {
	return Dial(canName, address)
}

func DialContext(ctx context.Context, network, address string) (net.Conn, error) {
	switch network {
	case canName:
		return dialCtx(ctx, func() (net.Conn, error) {
			return dialRaw(address)
		})
	default: // tcp,...
		var d net.Dialer
		return d.DialContext(ctx, network, address)
	}
}

func DialContextCan(ctx context.Context, address string) (net.Conn, error) {
	return DialContext(ctx, canName, address)
}

func dialCtx(ctx context.Context, connProvider func() (net.Conn, error)) (net.Conn, error) {
	resultChan := make(chan struct {
		conn net.Conn
		err  error
	})
	go func() {
		conn, err := connProvider()
		resultChan <- struct {
			conn net.Conn
			err  error
		}{
			conn: conn,
			err:  err,
		}
	}()
	// connect or timeout
	select {

	case result := <-resultChan:
		return result.conn, result.err

	case <-ctx.Done():
		go func() {
			result := <-resultChan
			if result.conn != nil {
				_ = result.conn.Close()
			}
		}()
		return nil, ctx.Err()
	}
}

func dialRaw(device string) (conn net.Conn, err error) {
	defer func() {
		if err != nil {
			err = &net.OpError{
				Op:   "dial",
				Net:  canName,
				Addr: &canRawAddr{device: device},
				Err:  err,
			}
		}
	}()
	ifi, err := net.InterfaceByName(device)
	if err != nil {
		return nil, fmt.Errorf("interface %s: %v", device, err)
	}
	fd, err := unix.Socket(unix.AF_CAN, unix.SOCK_RAW, unix.CAN_RAW)
	if err != nil {
		return nil, fmt.Errorf("socket: %v", err)
	}
	// Put fd in non-blocking mode so the created file will be registered
	// by the runtim poller (go >= 1.12)
	if err := unix.SetNonblock(fd, true); err != nil {
		return nil, fmt.Errorf("set nonblock: %v", err)
	}
	if err := unix.Bind(fd, &unix.SockaddrCAN{Ifindex: ifi.Index}); err != nil {
		return nil, fmt.Errorf("bind: %v", err)
	}
	return &fileConn{
		ra: &canRawAddr{device: device},
		f:  os.NewFile(uintptr(fd), canName),
	}, nil

}

type canRawAddr struct {
	device string
}

var _ net.Addr = &canRawAddr{}

func (a *canRawAddr) Network() string {
	return canName
}

func (a *canRawAddr) String() string {
	return a.device
}
