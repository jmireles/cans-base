package socket

import (
	"context"
	//"errors"
	"fmt"
	"io"
	"net"
	"testing"
	"time"

	"golang.org/x/sync/errgroup"
	"gotest.tools/v3/assert"
)

func TestTx_Message(t *testing.T) {
	testTransmit := func(opt TxOption) {
		w, r := net.Pipe()
		f := Can{
			ID:     0x12,
			Length: 8,
			Data:   Data{0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0},
		}
		msg := &testMessage{can: f}
		expected := []byte{
			// id---------------> | dlc | padding-------> | data----------------------------------------> |
			0x12, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0,
		}
		// write
		var g errgroup.Group
		g.Go(func() error {
			tx := NewTx(w, opt)
			ctx, done := context.WithTimeout(context.Background(), time.Second)
			defer done()
			if err := tx.WriteMessage(ctx, msg); err != nil {
				return err
			}
			return w.Close()
		})
		// read
		actual := make([]byte, len(expected))
		_, err := io.ReadFull(r, actual)
		assert.NilError(t, err)
		assert.NilError(t, r.Close())
		// assert
		assert.DeepEqual(t, expected, actual)
		assert.NilError(t, g.Wait())
	}

	// No opts
	testTransmit(func(*txOpts) {})

	// Frame Interceptor
	run := false
	intFunc := func(can Can) {
		run = true
	}
	testTransmit(TxFrameInterceptor(intFunc))
	assert.Assert(t, run)
}


func TestTx_MessageError(t *testing.T) {
	cause := fmt.Errorf("boom")
	msg := &testMessage{err: cause}
	tx := NewTx(nil)
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	err := tx.WriteMessage(ctx, msg)
	assert.Error(t, err, "tx message: boom")
	//assert.Equal(t, cause, errors.Unwrap(err)) fails (<nil>)
}

func TestTx_FrameError(t *testing.T) {
	t.Run("set deadline", func(t *testing.T) {
		cause := fmt.Errorf("boom")
		w := &errCon{deadlineErr: cause}
		tx := NewTx(w)
		ctx, done := context.WithTimeout(context.Background(), time.Second)
		defer done()
		err := tx.Write(ctx, Can{})
		assert.ErrorContains(t, err, "boom")
		//assert.Equal(t, cause, errors.Unwrap(err))
	})
	t.Run("write", func(t *testing.T) {
		cause := fmt.Errorf("boom")
		w := &errCon{writeErr: cause}
		tx := NewTx(w)
		ctx, done := context.WithTimeout(context.Background(), time.Second)
		defer done()
		err := tx.Write(ctx, Can{})
		assert.ErrorContains(t, err, "boom")
		//assert.Equal(t, cause, errors.Unwrap(err))
	})
}


type testMessage struct {
	can Can
	err error
}

// MarshalCan implements Message
func (t *testMessage) MarshalCan() (Can, error) {
	return t.can, t.err
}

// UnmarshalCan implements Message
func (t *testMessage) UnmarshalCan(Can) error {
	panic("should not be called")
}

type errCon struct {
	deadlineErr error
	writeErr    error
}

func (e *errCon) Write(b []byte) (n int, err error) {
	return 0, e.writeErr
}

func (e *errCon) SetWriteDeadline(t time.Time) error {
	return e.deadlineErr
}

func (e *errCon) Read(b []byte) (n int, err error) {
	panic("should not be called")
}

func (e *errCon) Close() error {
	panic("should not be called")
}

func (e *errCon) LocalAddr() net.Addr {
	panic("should not be called")
}

func (e *errCon) RemoteAddr() net.Addr {
	panic("should not be called")
}

func (e *errCon) SetDeadline(t time.Time) error {
	panic("should not be called")
}

func (e *errCon) SetReadDeadline(t time.Time) error {
	panic("should not be called")
}
