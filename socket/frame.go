package socket

import (
	"encoding/binary"

	"gitlab.com/jmireles/cans-base/socket/errs"
)

const (
	// frameLen is the length of a SocketCAN frame in bytes.
	frameLen = 16
	// indexId is the index of the first byte of the frame ID.
	indexId = 0
	// lenId is the length of a frame ID in bytes.
	lenId = 4
	// indexDataCodeLen is the index of the first byte of the frame dataCodeLen.
	indexDataCodeLen = indexId + lenId
	// lenDataCodeLen is the length of a frame dataCodeLen in bytes.
	lenDataCodeLen = 1
	// indexPad is the index of the first byte of frame padding.
	indexPad = indexDataCodeLen + lenDataCodeLen
	// lenPad is the length of frame padding in bytes.
	lenPad = 3
	// indexOfData is the index of the first byte of data in a frame.
	indexOfData = indexPad + lenPad
)

var _ [frameLen]struct{} = [indexOfData + dataMaxLength]struct{}{}

// id flags (copied from x/sys/unix).
const (
	idFlagExtended = 0x80000000
	idFlagError    = 0x20000000
	idFlagRemote   = 0x40000000
	idMaskExtended = 0x1fffffff
	idMaskStandard = 0x7ff
)

// FrameInterceptor provides a hook to intercept the transmission of a CAN frame.
// The Interceptor is called if and only if the transmission/receival
// is success.
type FrameInterceptor func(fr Can)

// frame represents a SocketCAN frame.
//
// The format specified in the Linux SocketCAN kernel mode:
//
//	struct can_frame {
//		canid_t can_id;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
//		__u8    can_dld; /* frame payload length in byte (0 .. 8) */
//		__u8    __pad;   /* padding */
//		__u8    __res0;  /* reserved / padding */
//		__u8    __res1;  /* reserved / padding */
//		__u8    data[8] __attribute__((aligned(8)));
//	};
type frame struct {
	// idAndFlags is the combined CAN ID and flags.
	idAndFlags uint32
	// dataCodeLen is the frame payload length in bytes.
	dataCodeLen uint8
	// padding + reserved fields
	_ [3]byte
	// bytes contains the frame payload
	data [8]byte
}

func (f *frame) unmarshalBinary(b []byte) {
	_ = b[frameLen-1] // bounds check
	f.idAndFlags = binary.LittleEndian.Uint32(b[indexId : indexId+lenId])
	f.dataCodeLen = b[indexDataCodeLen]
	copy(f.data[:], b[indexOfData:frameLen])
}

func (f *frame) marshalBinary(b []byte) {
	_ = b[frameLen-1] // bounds check
	binary.LittleEndian.PutUint32(b[indexId:indexId+lenId], f.idAndFlags)
	b[indexDataCodeLen] = f.dataCodeLen
	copy(b[indexOfData:], f.data[:])
}

func (f *frame) decode() Can {
	return Can{
		ID:         f.id(),
		Length:     f.dataCodeLen,
		Data:       f.data,
		IsExtended: f.isExtended(),
		IsRemote:   f.isRemote(),
	}
}

func (f *frame) encode(cf Can) {
	f.idAndFlags = cf.ID
	if cf.IsRemote {
		f.idAndFlags |= idFlagRemote
	}
	if cf.IsExtended {
		f.idAndFlags |= idFlagExtended
	}
	f.dataCodeLen = cf.Length
	f.data = cf.Data
}

func (f *frame) isExtended() bool {
	return f.idAndFlags&idFlagExtended > 0
}

func (f *frame) isRemote() bool {
	return f.idAndFlags&idFlagRemote > 0
}

func (f *frame) isError() bool {
	return f.idAndFlags&idFlagError > 0
}

func (f *frame) id() uint32 {
	if f.isExtended() {
		return f.idAndFlags & idMaskExtended
	}
	return f.idAndFlags & idMaskStandard
}

func (f *frame) decodeErrorFrame() errs.Frame {
	return errs.Frame{
		Class:                  f.errorClass(),
		LostArbitrationBit:     f.lostArbitrationBit(),
		Controller:             f.controllerError(),
		Protocol:               f.protocolError(),
		ProtocolViolationLoc:   f.protocolErrorLoc(),
		Transceiver:            f.transceiverError(),
		ControllerSpecificInfo: f.controllerSpecificInfo(),
	}
}

func (f *frame) errorClass() errs.Class {
	return errs.Class(f.idAndFlags &^ idFlagError)
}

func (f *frame) lostArbitrationBit() uint8 {
	return f.data[errs.IndexLostArbitrationBit]
}

func (f *frame) controllerError() errs.Controller {
	return errs.Controller(f.data[errs.IndexController])
}

func (f *frame) protocolError() errs.ProtocolViolation {
	return errs.ProtocolViolation(f.data[errs.IndexProtocol])
}

func (f *frame) protocolErrorLoc() errs.ProtocolViolationLoc {
	return errs.ProtocolViolationLoc(f.data[errs.IndexProtocolViolationLoc])
}

func (f *frame) transceiverError() errs.Transceiver {
	return errs.Transceiver(f.data[errs.IndexTransceiver])
}

func (f *frame) controllerSpecificInfo() [errs.LengthControllerSpecificInfo]byte {
	var ret [errs.LengthControllerSpecificInfo]byte
	start := errs.IndexControllerSpecificInfo
	end := start + errs.LengthControllerSpecificInfo
	copy(ret[:], f.data[start:end])
	return ret
}
