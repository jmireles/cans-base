package socket

import (
	"testing"
	"testing/quick"

	"gotest.tools/v3/assert"

	"gitlab.com/jmireles/cans-base/socket/errs"
)

func TestFrame_MarshalUnmarshalBinary_Property_Idempotent(t *testing.T) {
	f := func(data [frameLen]byte) [frameLen]byte {
		data[5], data[6], data[7] = 0, 0, 0 // padding+reserved fields
		return data
	}
	g := func(data [frameLen]byte) [frameLen]byte {
		var f frame
		f.unmarshalBinary(data[:])
		var newData [frameLen]byte
		f.marshalBinary(newData[:])
		return newData
	}
	assert.NilError(t, quick.CheckEqual(f, g, nil))
}

func TestFrame_EncodeDecode(t *testing.T) {
	for _, tt := range []struct {
		msg   string
		can   Can
		frame frame
	}{
		{
			msg: "data",
			can: Can{
				ID:     0x00000001,
				Length: 8,
				Data:   Data{1, 2, 3, 4, 5, 6, 7, 8},
			},
			frame: frame{
				idAndFlags:  0x00000001,
				dataCodeLen: 8,
				data:        [8]byte{1, 2, 3, 4, 5, 6, 7, 8},
			},
		},
		{
			msg: "extended",
			can: Can{
				ID:         0x00000001,
				IsExtended: true,
			},
			frame: frame{
				idAndFlags: 0x80000001,
			},
		},
		{
			msg: "remote",
			can: Can{
				ID:       0x00000001,
				IsRemote: true,
			},
			frame: frame{
				idAndFlags: 0x40000001,
			},
		},
		{
			msg: "extended and remote",
			can: Can{
				ID:         0x00000001,
				IsExtended: true,
				IsRemote:   true,
			},
			frame: frame{
				idAndFlags: 0xc0000001,
			},
		},
	} {
		tt := tt
		t.Run(tt.msg, func(t *testing.T) {
			t.Run("encode", func(t *testing.T) {
				var actual frame
				actual.encode(tt.can)
				assert.Equal(t, tt.frame, actual)
			})
			t.Run("decode", func(t *testing.T) {
				assert.Equal(t, tt.can, tt.frame.decode())
			})
		})
	}
}

func TestFrame_IsError(t *testing.T) {
	assert.Assert(t, (&frame{idAndFlags: 0x20000001}).isError())
	assert.Assert(t, !(&frame{idAndFlags: 0x00000001}).isError())
}

func TestFrame_DecodeErrorFrame(t *testing.T) {
	for _, tt := range []struct {
		msg      string
		f        frame
		expected errs.Frame
	}{
		{
			msg: "lost arbitration",
			f: frame{
				idAndFlags: 0x20000002,
				dataCodeLen: 8,
				data: [8]byte{
					42,
				},
			},
			expected: errs.Frame{
				Class: errs.ClassLostArbitration,
				LostArbitrationBit: 42,
			},
		},
		{
			msg: "controller",
			f: frame{
				idAndFlags: 0x20000004,
				dataCodeLen: 8,
				data: [8]byte{
					0,
					0x04,
				},
			},
			expected: errs.Frame{
				Class:      errs.ClassController,
				Controller: errs.ControllerRxWarning,
			},
		},
		{
			msg: "protocol violation",
			f: frame{
				idAndFlags: 0x20000008,
				dataCodeLen: 8,
				data: [8]byte{
					0,
					0,
					0x10,
					0x02,
				},
			},
			expected: errs.Frame{
				Class:                errs.ClassProtocolViolation,
				Protocol:             errs.ProtocolViolationBit1,
				ProtocolViolationLoc: errs.ProtocolViolationLocID28To21,
			},
		},
		{
			msg: "transceiver",
			f: frame{
				idAndFlags: 0x20000010,
				dataCodeLen: 8,
				data: [8]byte{
					0,
					0,
					0,
					0,
					0x07,
				},
			},
			expected: errs.Frame{
				Class:       errs.ClassTransceiver,
				Transceiver: errs.TransceiverCANHShortToGND,
			},
		},
		{
			msg: "controller-specific information",
			f: frame{
				idAndFlags: 0x20000001,
				dataCodeLen: 8,
				data: [8]byte{
					0,
					0,
					0,
					0,
					0,
					1,
					2,
					3,
				},
			},
			expected: errs.Frame{
				Class:                  errs.ClassTxTimeout,
				ControllerSpecificInfo: [3]byte{1, 2, 3},
			},
		},
	} {
		tt := tt
		t.Run(tt.msg, func(t *testing.T) {
			assert.Equal(t, tt.expected, tt.f.decodeErrorFrame())
		})
	}
}
