package socket

import (
	"context"
	"fmt"
	"net"
)

type TxOption func(*txOpts)

type txOpts struct {
	frameInterceptor FrameInterceptor
}

// Tx transmits CAN frames.
type Tx struct {
	opts txOpts
	conn net.Conn
}

// NewTx creates a new Tx that transmits CAN frames to
// the provided io.Writer.
func NewTx(conn net.Conn, opt ...TxOption) *Tx {
	opts := txOpts{}
	for _, f := range opt {
		f(&opts)
	}
	return &Tx{
		conn: conn,
		opts: opts,
	}
}

// WriteMessage transmits a message.
func (t *Tx) WriteMessage(ctx context.Context, m Message) error {
	can, err := m.MarshalCan()
	if err != nil {
		return fmt.Errorf("tx message: %v", err)
	}
	return t.Write(ctx, can)
}

// Write transmits a CAN frame.
func (t *Tx) Write(ctx context.Context, f Can) error {
	var scf frame
	scf.encode(f)
	data := make([]byte, frameLen)
	scf.marshalBinary(data)
	if deadline, ok := ctx.Deadline(); ok {
		if err := t.conn.SetWriteDeadline(deadline); err != nil {
			return fmt.Errorf("tx frame: %v", err)
		}
	}
	if _, err := t.conn.Write(data); err != nil {
		return fmt.Errorf("tx frame: %v", err)
	}
	if t.opts.frameInterceptor != nil {
		t.opts.frameInterceptor(f)
	}
	return nil
}

// Close the tx's underlying connection.
func (t *Tx) Close() error {
	return t.conn.Close()
}

// TxFrameInterceptor returns a TxOption that sets the FrameInterceptor for the
// tx. Only one frame interceptor can be installed.
func TxFrameInterceptor(i FrameInterceptor) TxOption {
	return func(o *txOpts) {
		o.frameInterceptor = i
	}
}
