package socket

import (
	"bytes"
	"io/ioutil"
	"testing"

	"gotest.tools/v3/assert"

	"gitlab.com/jmireles/cans-base/socket/errs"
)

func TestRx_Opt(t *testing.T) {
	testRx := func(opt RxOption) {
		input := []byte{
			// id---------------> | dlc | padding-------> | data----------------------------------------> |
			0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		}
		expected := Can{ID: 0x01, Length: 2, Data: Data{0x12, 0x34}}
		rx := NewRx(ioutil.NopCloser(bytes.NewReader(input)), opt)
		assert.Assert(t, rx.Scan(), "expecting 1 Can frames")
		assert.NilError(t, rx.Err())
		assert.Assert(t, !rx.HasErrorFrame())
		assert.DeepEqual(t, expected, rx.Read())
		assert.Assert(t, !rx.Scan(), "expecting exactly 1 Can frames")
		assert.NilError(t, rx.Err())
	}

	// no options
	testRx(func(*rxOpts) {})

	// frame interceptor
	run := false
	intFunc := func(Can) {
		run = true
	}
	testRx(RxFrameInterceptor(intFunc))
	assert.Assert(t, run)
}

func TestRx_Frames(t *testing.T) {
	for _, tt := range []struct {
		msg      string
		input    []byte
		expected []Can
	}{
		{
			msg: "no data",
			input: []byte{},
			expected: []Can{},
		},
		{
			msg: "incomplete frame",
			input: []byte{
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			},
			expected: []Can{},
		},
		{
			msg: "whole single frame",
			input: []byte{
				// id---------------> | dlc | padding-------> | data----------------------------------------> |
				0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			},
			expected: []Can{
				{ID: 0x01, Length: 2, Data: Data{0x12, 0x34}},
			},
		},
		{
			msg: "one whole one incomplete",
			input: []byte{
				// id---------------> | dlc | padding-------> | data----------------------------------------> |
				0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00,
			},
			expected: []Can{
				{ID: 0x01, Length: 2, Data: Data{0x12, 0x34}},
			},
		},
		{
			msg: "two whole frames",
			input: []byte{
				// id---------------> | dlc | padding-------> | data----------------------------------------> |
				0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				// id---------------> | dlc | padding-------> | data----------------------------------------> |
				0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x56, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			},
			expected: []Can{
				{ID: 0x01, Length: 2, Data: Data{0x12, 0x34}},
				{ID: 0x02, Length: 2, Data: Data{0x56, 0x78}},
			},
		},
	} {
		tt := tt
		t.Run(tt.msg, func(t *testing.T) {
			rx := NewRx(ioutil.NopCloser(bytes.NewReader(tt.input)))
			for i, expected := range tt.expected {
				assert.Assert(t, rx.Scan(), "expecting %d Can frames", i+1)
				assert.NilError(t, rx.Err())
				assert.Assert(t, !rx.HasErrorFrame())
				assert.DeepEqual(t, expected, rx.Read())
			}
			assert.Assert(t, !rx.Scan(), "expecting exactly %d Can frames", len(tt.expected))
			assert.NilError(t, rx.Err())
		})
	}
}

func TestRx_Errors(t *testing.T) {
	input := []byte{
		// frame
		// id---------------> | dlc | padding-------> | data----------------------------------------> |
		0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		// error frame
		// id---------------> | dlc | padding-------> | data----------------------------------------> |
		0x01, 0x00, 0x00, 0x20, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		// frame
		// id---------------> | dlc | padding-------> | data----------------------------------------> |
		0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x12, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	}
	rx := NewRx(ioutil.NopCloser(bytes.NewReader(input)))
	// expect frame
	assert.Assert(t, rx.Scan())
	assert.Assert(t, !rx.HasErrorFrame())
	assert.Equal(t, Can{ID: 0x01, Length: 2, Data: Data{0x12, 0x34}}, rx.Read())
	// expect error frame
	assert.Assert(t, rx.Scan())
	assert.Assert(t, rx.HasErrorFrame())
	assert.Equal(t, errs.Frame{Class: errs.ClassTxTimeout}, rx.ErrorFrame())
	// expect frame
	assert.Assert(t, rx.Scan())
	assert.Assert(t, !rx.HasErrorFrame())
	assert.Equal(t, Can{ID: 0x02, Length: 2, Data: Data{0x12, 0x34}}, rx.Read())
	// expect end of stream
	assert.Assert(t, !rx.Scan())
	assert.NilError(t, rx.Err())
}
