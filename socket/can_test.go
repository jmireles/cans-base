package socket

import (
	"fmt"
	"testing"
	"unsafe"

	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

// If this mocks ever starts failing, the documentation needs to be updated
// to prefer pass-by-pointer over pass-by-value.
func TestCan_Size(t *testing.T) {
	assert.Assert(t, unsafe.Sizeof(Can{}) <= 16, "Can size is <= 16 bytes")
}

func TestCan_Validate(t *testing.T) {
	for _, tt := range []Can{
		{ID: canMaxId + 1},
		{ID: canMaxExtId + 1, IsExtended: true},
	} {
		tt := tt
		t.Run(fmt.Sprintf("%v", tt), func(t *testing.T) {
			assert.Check(t, tt.Validate() != nil, "should return validation error")
		})
	}
}

func TestCan_String(t *testing.T) {
	for _, tt := range []struct {
		can Can
		str string
	}{
		{
			can: Can{ID: 0x62e, Length: 2, Data:Data{0x10, 0x44}},
			str: "62E#1044",
		},
		{
			can: Can{ID: 0x410, IsRemote: true, Length: 3},
			str: "410#R3",
		},
		{
			can: Can{ID: 0xd2, Length: 2, Data: Data{0xf0, 0x31}},
			str: "0D2#F031",
		},
		{
			can: Can{ID: 0xee},
			str:   "0EE#",
		},
		{
			can: Can{ID: 0},
			str:   "000#",
		},
		{
			can: Can{ID: 0, IsExtended: true},
			str:   "00000000#",
		},
		{
			can: Can{ID: 0x1234abcd, IsExtended: true},
			str:   "1234ABCD#",
		},
	} {
		tt := tt
		t.Run(fmt.Sprintf("String|can=%v,str=%v", tt.can, tt.str), func(t *testing.T) {
			assert.Check(t, cmp.Equal(tt.str, tt.can.String()))
		})
		t.Run(fmt.Sprintf("UnmarshalString|can=%v,str=%v", tt.can, tt.str), func(t *testing.T) {
			var actual Can
			if err := actual.UnmarshalString(tt.str); err != nil {
				t.Fatal(err)
			}
			assert.Check(t, cmp.DeepEqual(actual, tt.can))
		})
	}
}

func TestCan_Errors(t *testing.T) {
	for _, tt := range []string{
		"foo",                    // invalid
		"foo#",                   // invalid ID
		"0D23#F031",              // invalid ID length
		"62E#104400000000000000", // invalid data length
	} {
		tt := tt
		t.Run(fmt.Sprintf("str=%v", tt), func(t *testing.T) {
			var can Can
			err := can.UnmarshalString(tt)
			assert.ErrorContains(t, err, "invalid")
			assert.Check(t, cmp.DeepEqual(Can{}, can))
		})
	}
}
