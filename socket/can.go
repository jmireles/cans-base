package socket

import (
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"
)

const canName = "can"

const (
	canIdBits    = 11 // standard
	canExtIdBits = 29 // extended
)

// CAN format constants
const (
	canMaxId    = 0x7ff      // standard
	canMaxExtId = 0x1fffffff // extended
)

// CAN represents a CAN frame.
//
// A CAN fit into 16 bytes on common architectures
// and is therefore amenable to pass-by-value and judicious copying.
type Can struct {
	// ID is the CAN ID
	ID uint32
	// Length is the number of bytes of data in the can.
	Length uint8
	// Data is the can data
	Data Data
	// IsRemote is true for remote cans.
	IsRemote bool
	// IsExtended is true for extended cans (29 bits IDs).
	IsExtended bool
}

// Validates returns anr eror if the CAN is not a valid CAN can.
func (c *Can) Validate() error {
	// Validate ID
	if c.IsExtended && c.ID > canMaxExtId {
		return fmt.Errorf(
			"invalid extended CAN id: %v does not fit in %v bits",
			c.ID,
			canExtIdBits,
		)
	} else if !c.IsExtended && c.ID > canMaxId {
		return fmt.Errorf(
			"invalid standard CAN id: %v does not fit in %v bits",
			c.ID,
			canIdBits,
		)
	}
	// Validate Data
	if c.Length > dataMaxLength {
		return fmt.Errorf("invalid data length: %v", c.Length)
	}
	return nil
}

// String returns an ASCII representation of the CAN can.
//
// Format:
//
//	([0-9A-F]{3}|[0-9A-F]{3})#(R[0-8]?[0-9A-F]{0,16})
//
// The format is compatible with the candump(1) log file format.
func (c Can) String() string {
	var id string
	if c.IsExtended {
		id = fmt.Sprintf("%08X", c.ID)
	} else {
		id = fmt.Sprintf("%03X", c.ID)
	}
	if c.IsRemote && c.Length == 0 {
		return id + "#R"
	} else if c.IsRemote {
		return id + "#R" + strconv.Itoa(int(c.Length))
	}
	return id + "#" + strings.ToUpper(hex.EncodeToString(c.Data[:c.Length]))
}

// UnmarshalString sets *c using the provided ASCII representation of a CAN.
func (c *Can) UnmarshalString(s string) error {
	// split into parts
	parts := strings.Split(s, "#")
	if len(parts) != 2 {
		return fmt.Errorf("invalid can format: %v", s)
	}
	id, data := parts[0], parts[1]
	var can Can
	// Parse: IsExtended
	if len(id) != 3 && len(id) != 8 {
		return fmt.Errorf("invalid ID length: %v", s)
	}
	can.IsExtended = len(id) == 8
	// Parse: ID
	uid, err := strconv.ParseUint(id, 16, 32)
	if err != nil {
		return fmt.Errorf("invalid can ID: %v", s)
	}
	can.ID = uint32(uid)
	if len(data) == 0 {
		*c = can
		return nil
	}
	// Parse: IsRemote
	if data[0] == 'R' {
		can.IsRemote = true
		if len(data) > 2 {
			return fmt.Errorf("invalid remote length: %v", s)
		} else if len(data) == 2 {
			dataLength, err := strconv.Atoi(data[1:2])
			if err != nil {
				return fmt.Errorf("invalid remote length: %v: %w", s, err)
			}
			can.Length = uint8(dataLength)
		}
		*c = can
		return nil
	}
	// Parse: Length
	if len(data) > 16 || len(data)%2 != 0 {
		return fmt.Errorf("invalid data length: %v", s)
	}
	can.Length = uint8(len(data) / 2)
	// Parse: Data
	dataBytes, err := hex.DecodeString(data)
	if err != nil {
		return fmt.Errorf("invalid data: %v: %w", s, err)
	}
	copy(can.Data[:], dataBytes)
	*c = can
	return nil
}
