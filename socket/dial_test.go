package socket

import (
	"context"
	"errors"
	"fmt"
	"net"
	"runtime"
	"testing"
	"time"

	"golang.org/x/sync/errgroup"
	"gotest.tools/v3/assert"
	"gotest.tools/v3/assert/cmp"
)

// Adapted from https://github.com/einride/can-go

// $ sudo modprobe can
// $ sudo modprobe can_raw
// $ lsmod | grep "can"
// can_raw                20480  0
// can                    28672  1 can_raw
// can_dev                28672  1 mcp251x
//
// $ sudo ip link set can0 type can bitrate 100000 restart-ms 100
// $ sudo ip link set up can0
// $ ip addr | grep "can"
// 4: can0: <NOARP,UP,LOWER_UP,ECHO> mtu 16 qdisc pfifo_fast state UP group default qlen 10
//    link/can 



func TestDial_TCP_(t *testing.T) {
	lis, err := net.Listen("tcp", "localhost:0")
	assert.NilError(t, err)
	var g errgroup.Group
	g.Go(func() error {
		conn, err := lis.Accept()
		if err != nil {
			return err
		}
		return conn.Close()
	})
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	conn, err := DialContext(ctx, "tcp", lis.Addr().String())
	assert.NilError(t, err)
	assert.NilError(t, conn.Close())
	assert.NilError(t, g.Wait())
}

func TestDial_TCPTxRx(t *testing.T) {
	// Given: A TCP listener that writes a frame on an accepted connection
	lis, err := net.Listen("tcp", "localhost:0")
	assert.NilError(t, err)
	var g errgroup.Group
	can := Can{ID: 42, Length: 5, Data: Data{'H', 'e', 'l', 'l', 'o'}}
	g.Go(func() error {
		conn, err := lis.Accept()
		if err != nil {
			return err
		}
		tx := NewTx(conn)
		ctx, done := context.WithTimeout(context.Background(), time.Second)
		defer done()
		if err := tx.Write(ctx, can); err != nil {
			return err
		}
		return conn.Close()
	})
	// When: We connect to the listener
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	conn, err := DialContext(ctx, "tcp", lis.Addr().String())
	assert.NilError(t, err)
	rec := NewRx(conn)
	assert.Assert(t, rec.Scan())
	assert.Assert(t, !rec.HasErrorFrame())
	assert.DeepEqual(t, can, rec.Read()) // received "42#Hello"
	assert.NilError(t, conn.Close())
	assert.NilError(t, g.Wait())
}


// For TestDialRaw_* this is needed:
//
// Load the vcan kernel module, create the virtual Can interface,
// bring the virtual Can interface online.
// ip a | grep "vcan0" should show state as UNKNOWN... is ok for a virtual interface
// $ sudo modprobe vcan
// $ sudo ip link add dev vcan0 type vcan
// $ sudo ip link set up vcan0

func TestDialRaw_(t *testing.T) {
	testDialRawValidate(t)
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	conn, err := DialContext(ctx, canName, "vcan0")
	assert.NilError(t, err)
	assert.NilError(t, conn.Close())
}

func TestDialRaw_Fail(t *testing.T) {
	testDialRawValidate(t)
	t.Run("bad file name", func(t *testing.T) {
		_, err := DialContext(context.Background(), canName, "badFileName#")
		assert.ErrorContains(t, err, "dial")
	})
	t.Run("cancel", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		cancel()
		_, err := DialContext(ctx, canName, "vcan0")
		assert.ErrorContains(t, err, "context canceled")
	})
}

func TestDial_RawAddr(t *testing.T) {
	testDialRawValidate(t)
	conn, err := Dial(canName, "vcan0")
	assert.NilError(t, err)
 	// SocketCan connections don't have a local connection	
	assert.Assert(t, cmp.Nil(conn.LocalAddr()))
	assert.Equal(t, canName, conn.RemoteAddr().Network())
	assert.Equal(t, "vcan0", conn.RemoteAddr().String())
}

func TestDialRaw_SetDeadline(t *testing.T) {
	testDialRawValidate(t)
	// Given that a vcan device exists and that I can open a connection to it
	receiver, err := Dial(canName, "vcan0")
	assert.NilError(t, err)
	// When I set the can
	timeout := 20 * time.Millisecond
	assert.NilError(t, receiver.SetDeadline(time.Now().Add(timeout)))
	// Then I expect a read without a corresponding write to time out
	data := make([]byte, frameLen)
	n, err := receiver.Read(data)
	assert.Equal(t, 0, n)
	assert.Assert(t, cmp.ErrorContains(err, ""))
	// When I clear the timeouts
	assert.NilError(t, receiver.SetDeadline(time.Time{}))
	// Then I don't expect the read to timeout anymore
	errChan := make(chan error, 1)
	go func() {
		_, err = receiver.Read(data)
		errChan <- err
	}()
	select {
	case <-errChan:
		t.Fatal("unexpected read result")
	case <-time.After(timeout):
		assert.NilError(t, receiver.Close())
		assert.Assert(t, cmp.ErrorContains(<-errChan, ""))
	}
}

func TestDialRaw_ReadWrite(t *testing.T) {
	testDialRawValidate(t)
	// given a reader and writer
	reader, err := Dial(canName, "vcan0")
	assert.NilError(t, err)
	writer, err := Dial(canName, "vcan0")
	assert.NilError(t, err)
	// when the reader reads
	var g errgroup.Group
	var readCan Can
	g.Go(func() error {
		rx := NewRx(reader)
		if !rx.Scan() {
			return fmt.Errorf("receive scan")
		}
		readCan = rx.Read()
		return reader.Close()
	})
	// and the writer writes
	writeCan := Can{ID: 32}
	tx := NewTx(writer)
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	assert.NilError(t, tx.Write(ctx, writeCan))
	assert.NilError(t, writer.Close())
	// then the written and read frames should be identical
	assert.NilError(t, g.Wait())
	assert.DeepEqual(t, writeCan, readCan)
}

func TestDialRaw_WriteOnClosedFails(t *testing.T) {
	testDialRawValidate(t)
	conn, err := Dial(canName, "vcan0")
	assert.NilError(t, err)
	tx := NewTx(conn)
	ctx, done := context.WithTimeout(context.Background(), time.Second)
	defer done()
	assert.NilError(t, tx.Write(ctx, Can{}))
	// When I close the connection and then write to it
	assert.NilError(t, conn.Close())
	// Then it should fail
	assert.Assert(t, cmp.ErrorContains(tx.Write(ctx, Can{}), ""), "WriteFrame on a closed Conn should fail")
}

func TestDialRaw_ReadOnClose(t *testing.T) {
	testDialRawValidate(t)
	t.Run("close then read", func(t *testing.T) {
		conn, err := Dial(canName, "vcan0")
		assert.NilError(t, err)
		// When I close the connection and then read from it
		assert.NilError(t, conn.Close())
		rx := NewRx(conn)
		assert.Assert(t, !rx.Scan())
		assert.Assert(t, cmp.ErrorContains(rx.Err(), ""))
	})
	t.Run("read then close", func(t *testing.T) {
		conn, err := Dial(canName, "vcan0")
		assert.NilError(t, err)
		// And when I read from a connection
		var g errgroup.Group
		var receiveErr error
		g.Go(func() error {
			rx := NewRx(conn)
			if rx.Scan() {
				return fmt.Errorf("receive")
			}
			receiveErr = rx.Err()
			return nil
		})
		runtime.Gosched()
		// And then close it
		assert.NilError(t, conn.Close())
		// Then the read operation should fail
		assert.NilError(t, g.Wait())
		assert.Assert(t, cmp.ErrorContains(receiveErr, ""))
	})
}



func testDialRawValidate(t *testing.T) {
	t.Helper()
	if _, err := net.InterfaceByName("vcan0"); err != nil {
		t.Skip("device vcan0 not available %w", err)
	}
}

//
// $ GOFLAGS="-count=1" go test ./socket -run TestDialCan0_Rx -v
//
func TestDialCan0_Rx(t *testing.T) {
	conn, err := DialContextCan(context.Background(), "can0")
	if err != nil {
		t.Skip("can0 not available %w", err)
	}
	t.Logf("Connected %v", conn)

	rxC := make(chan Can, 1)
	rxErrC := make(chan error, 1)
	end := make(chan error, 1)
	go func() { // consumer
		for {
			select {
			case rxFrame := <-rxC:
				t.Logf("rx ok %s", rxFrame.String())

			case err := <-rxErrC:
				end <- err
				return
			}
		}
	}()
	go func() { // receiver
		recv := NewRx(conn)
		for recv.Scan() {
			// a frame was received
			rxC <-recv.Read()
		}
		if err := recv.Err(); err != nil {
			// receiver stop receiving frames
			rxErrC <-errors.New(fmt.Sprintf("Recv error %v", err.Error()))
			return
		} else {
			rxErrC <-errors.New("Recv stopped with no errors")
			return
		}
	}()
	if err := <-end; err != nil {
		t.Fatalf("%v", err)
	}
}

func TestDialCan0_TxPM(t *testing.T) {
	pmReqs := []string{
		"00200000#", // request all PMs version
		"00200001#", // request all PMs serials
		"00200002#", // request all PMs status
		"00200003#", // request all PMs config
	}
	testDialCan0_Tx(t, pmReqs)
}

func TestDialCan0_TxT4(t *testing.T) {
	t4Reqs := []string{
		"00E00000#", // request all T4s version
		"00E00001#", // request all T4s serials
		"00E00002#", // request all T4s status
		"00E00003#", // request all T4s config
	}
	testDialCan0_Tx(t, t4Reqs)

}

// This test send common request for boumatic lines  without data
// such as 0=version, 1=serials, 2=status and 3=config
// This sends a request one every second and finishes.
// Run TestCan0Rx in another console to receive the responses
// $ GOFLAGS="-count=1" go test ./socket -run TestDialCan0_Tx -v
func testDialCan0_Tx(t *testing.T, reqs []string) {
	conn, err := DialContextCan(context.Background(), "can0")
	if err != nil {
		t.Skip("can0 not available %w", err)
	}
	t.Logf("Connected %v", conn)
	tx := NewTx(conn)
	can := Can{}

	for _, req := range reqs {
		if err := can.UnmarshalString(req); err != nil {
			t.Fatalf("Unexpected frame unmarshal %v", err)
			return
		}
		if err := tx.Write(context.Background(), can); err != nil {
			t.Fatalf("Unexpected transmitt error %v", err)
			return
		}
		t.Logf("tx ok %v", can.String())
		time.Sleep(1 * time.Second)
	}
}
