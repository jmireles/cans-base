package base

import (
	"fmt"
	"path/filepath"
	"reflect"
	"runtime"
	"testing"
)

// CAN network
type Net byte

func NewNet(net byte) (Net, error) {
	if net < 1 || net > 15 {
		return Net(0), fmt.Errorf("Net out of range [1-15]")
	} else {
		return Net(net), nil
	}
}

func (n Net) NetId() Net {
	return n
}

func BoxKey(net Net, line byte, id byte) string {
	return fmt.Sprintf("%01d%02x%02x", net, line, id)
}


// from https://github.com/benbjohnson/testing
type Test struct {
	T testing.TB
}

// Assert fails the test if the condition is false.
func (t *Test) Assert(condition bool, msg string, v ...any) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: "+msg+"\033[39m\n\n", append([]interface{}{filepath.Base(file), line}, v...)...)
		t.T.FailNow()
	}
}

// Ok fails the test if an err is not nil.
func (t *Test) Ok(err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		t.T.FailNow()
	}
}

// Equals fails the test if exp is not equal to act.
func (t *Test) Equals(exp, act any) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		t.T.FailNow()
	}
}

func (t *Test) Log(f string, args ...any) {
	fmt.Printf(f, args...)
}

