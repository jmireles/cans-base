package p1

import (
	"fmt"
	"math/rand"
	"sync"
	"time"

	"gitlab.com/jmireles/cans-base/milk"
)

// Get/Set Requests
const (
	Get03_Config   milk.Get = 3  // cfg 1
	Set04_Config   milk.Set = 4  // cfg 1
	
	Get05_Values   milk.Get = 5
	Get06_Averages milk.Get = 6

	Get07_Calibr   milk.Get = 7  // cfg 2
	Set08_Calibr   milk.Set = 8  // cfg 2
	Get09_Options  milk.Get = 9  // cfg 3
	Set10_Options  milk.Set = 10 // cfg 3
	Get11_Profiles milk.Get = 11 // cfg 4
	Set12_Profiles milk.Set = 12 // cfg 4
	Get13_Display  milk.Get = 13 // cfg 5
	Set14_Display  milk.Set = 14 // cfg 5
	Get15_Config   milk.Get = 15 // cfg 6
	Set16_Config   milk.Set = 16 // cfg 6
	Get17_Alarms   milk.Get = 17 // cfg 7
	Set18_Alarms   milk.Set = 18 // cfg 7
	Get19_Stalls   milk.Get = 19 // cfg 8
	Set20_Stalls   milk.Set = 20 // cfg 8
)

// Solicited/Unsolicited responses
const (
	Sol03_Config         milk.Sol   = 3  // cfg 1
	Sol04_ValCurves      milk.Sol   = 4
	Unsol05_Idle         milk.Unsol = 5
	Unsol06_Working      milk.Unsol = 6
	Unsol07_Alarmed      milk.Unsol = 7
	Sol08_AvgPhaseFront  milk.Sol   = 8
	Sol09_AvgPhaseRear   milk.Sol   = 9
	Sol10_AvgLevelFront  milk.Sol   = 10
	Sol11_AvgLevelRear   milk.Sol   = 11
	Sol12_ValPhasesFront milk.Sol   = 12
	Sol13_ValPhasesRear  milk.Sol   = 13
	Sol14_ValLevelsFront milk.Sol   = 14
	Sol15_ValLevelsRear  milk.Sol   = 15

	Unsol16_NoPhaseFront milk.Unsol = 16
	Unsol17_NoPhaseRear  milk.Unsol = 17

	Sol18_Calibr         milk.Sol   = 18 // cfg 2

	Unsol20_Stimulation  milk.Unsol = 20
	Sol21_Options        milk.Sol   = 21 // cfg 3
	Sol22_Profiles       milk.Sol   = 22 // cfg 4
	Sol23_Display        milk.Sol   = 23 // cfg 5
	Sol24_Config         milk.Sol   = 24 // cfg 6
	Sol25_Alarms         milk.Sol   = 25 // cfg 7
	Unsol26_Notify       milk.Unsol = 26 
	Sol27_Stalls         milk.Sol   = 27 // cfg 8
)

func Req(comm byte) (*milk.Get, *milk.Set) {
	for _, get := range Gets {
		if comm == byte(get) {
			return &get, nil
		}
	}
	for _, set := range Sets {
		if comm == byte(set) {
			return nil, &set
		}
	}
	return nil, nil
}

var Gets = []milk.Get {
	milk.Get00_Version,
	milk.Get01_Serial,
	milk.Get02_Status, 
	Get03_Config,
	Get05_Values,
	Get06_Averages, 
	Get07_Calibr,
	Get09_Options,
	Get11_Profiles, 
	Get13_Display,
	Get15_Config,
	Get17_Alarms,
	Get19_Stalls,
}

var Sets = []milk.Set {
	Set04_Config,
	Set08_Calibr,
	Set10_Options, 
	Set12_Profiles,
	Set14_Display,
	Set16_Config, 
	Set18_Alarms,
	Set20_Stalls,
}

func Resp(comm byte) (*milk.Sol, *milk.Unsol) {
	for _, unsol := range unsols {
		if comm == byte(unsol) {
			return nil, &unsol
		}
	}
	for _, sol := range sols {
		if comm == byte(sol) {
			return &sol, nil
		}
	}
	return nil, nil
}

func Resps() ([]milk.Sol, []milk.Unsol) {
	return sols, unsols
}

var sols = []milk.Sol {
	milk.Sol00_Version,
	milk.Sol02_Status,
	Sol03_Config,
	Sol04_ValCurves,
	Sol08_AvgPhaseFront,
	Sol09_AvgPhaseRear,
	Sol10_AvgLevelFront,
	Sol11_AvgLevelRear,
	Sol12_ValPhasesFront,
	Sol13_ValPhasesRear,
	Sol14_ValLevelsFront,
	Sol15_ValLevelsRear,
	Sol18_Calibr,
	Sol21_Options,
	Sol22_Profiles,
	Sol23_Display,
	Sol24_Config,
	Sol25_Alarms,
	Sol27_Stalls,
}

var unsols = []milk.Unsol {
	milk.Unsol01_Restart,
	Unsol05_Idle,
	Unsol06_Working,
	Unsol07_Alarmed,
	Unsol16_NoPhaseFront,
	Unsol17_NoPhaseRear,
	Unsol20_Stimulation,
	Unsol26_Notify,
}

func PayloadFrom(comm byte) (int, error) {
	switch comm {
	case byte(Sol03_Config):         return 8, nil
	case byte(Sol04_ValCurves):      return 8, nil
	case byte(Sol08_AvgPhaseFront):  return 8, nil
	case byte(Sol09_AvgPhaseRear):   return 8, nil
	case byte(Sol10_AvgLevelFront):  return 8, nil
	case byte(Sol11_AvgLevelRear):   return 8, nil
	case byte(Sol12_ValPhasesFront): return 8, nil
	case byte(Sol13_ValPhasesRear):  return 8, nil
	case byte(Sol14_ValLevelsFront): return 8, nil
	case byte(Sol15_ValLevelsRear):  return 8, nil
	case byte(Sol18_Calibr):         return 8, nil
	case byte(Sol21_Options):        return 8, nil
	case byte(Sol22_Profiles):       return 8, nil
	case byte(Sol23_Display):        return 8, nil
	case byte(Sol24_Config):         return 8, nil
	case byte(Sol25_Alarms):         return 8, nil
	case byte(Sol27_Stalls):         return 8, nil

	case byte(Unsol05_Idle):         return 0, nil
	case byte(Unsol06_Working):      return 0, nil
	case byte(Unsol07_Alarmed):      return 2, nil
	case byte(Unsol16_NoPhaseFront): return 0, nil
	case byte(Unsol17_NoPhaseRear):  return 0, nil
	case byte(Unsol20_Stimulation):  return 0, nil
	case byte(Unsol26_Notify):       return 6, nil

	default:
		return 0, fmt.Errorf("Invalid from comm:%d", comm)
	}

}

func PayloadTo(comm byte) (int, error) {
	switch comm {
	// basic config needs to payload
	case byte(Get03_Config),
		byte(Get07_Calibr),
		byte(Get09_Options),
		byte(Get13_Display),
		byte(Get15_Config),
		byte(Get17_Alarms),
		byte(Get19_Stalls):
		return 0, nil

	// Get value sends as payload
	case byte(Get05_Values):
		return 1, nil
	// Get averages needs no payload:
	case byte(Get06_Averages):
		return 0, nil

	// Get profiles sends a single byte with the profile
	case byte(Get11_Profiles):
		return 1, nil

	// All sets needs at least 8 bytes for payload
	case byte(Set04_Config),
		byte(Set08_Calibr),
		byte(Set10_Options),
		byte(Set12_Profiles),
		byte(Set14_Display),
		byte(Set16_Config),
		byte(Set18_Alarms),
		byte(Set20_Stalls):
		return 8, nil
	
	default:
		return 0, fmt.Errorf("Invalid to comm:%v", comm)
	}
}

type Sols struct {
	sync.Mutex
	
	vals    map[milk.Sol][]byte
	cfgs    map[milk.Sol][][]byte
	pending int
	rows4   [][]uint8
}

func NewSimSols(sols4 [][]uint8) milk.SimSols {
	return &Sols{
		vals: map[milk.Sol][]byte{
			milk.Sol02_Status: []byte{ 
				0,0,0,0,0,0,0,0,
			},
		},
		cfgs: map[milk.Sol][][]byte{
			Sol03_Config: [][]byte {
				[]byte{ 0,0,0,0,0,0,0,0 },
				[]byte{ 1,0,0,0,0,0,0,0 },
				[]byte{ 2,0,0,0,0,0,0,0 },
				[]byte{ 3,0,0,0,0,0,0,0 },
			},
		},
		pending: 0,
		rows4:   sols4,
	}
}

// Req implements milk.SimSols
func (s *Sols) Req(data milk.Data, resp milk.SolResp) {
	switch data.Comm() {
	case byte(milk.Get02_Status): s.resp2(resp)
	case byte(Get03_Config):      s.resp3(resp)
	case byte(Set04_Config):      s.resp4(data)
	case byte(Get05_Values):      s.resp5(data, resp)
	}
}

func (s *Sols) resp2(resp milk.SolResp) {
	if val := s.vals[milk.Sol02_Status]; len(val) > 0 {
		resp(milk.Sol02_Status, val)
		time.Sleep(100 * time.Millisecond)
	}
}

func (s *Sols) resp3(resp milk.SolResp) {
	for _, cfg := range s.cfgs[Sol03_Config] {
		resp(Sol03_Config, cfg)
		time.Sleep(100 * time.Millisecond)
	}
}

func (s *Sols) resp4(data milk.Data) {
	if cfg := data.Extra(); len(cfg) == 8 {
		switch cfg[0] {
		case 0: copy(s.cfgs[Sol03_Config][0], cfg)
		case 1: copy(s.cfgs[Sol03_Config][1], cfg)
		case 2: copy(s.cfgs[Sol03_Config][2], cfg)
		case 3: copy(s.cfgs[Sol03_Config][3], cfg)
		}
		time.Sleep(100 * time.Millisecond)
	}
}

func (s *Sols) resp5(data milk.Data, resp milk.SolResp) {
	s.Lock()
	defer s.Unlock()
	if extra := data.Extra(); len(extra) < 1 {
		return
	} else if extra[0] == 1 {
		if s.pending <= 0 {
			s.pending = 1
			go func() {
				for {
					time.Sleep(100 * time.Millisecond)
					for _, row := range s.rows4 {
						resp(Sol04_ValCurves, s.noise(row))
						time.Sleep(20 * time.Millisecond)
					}
					s.Lock()
					s.pending--
					p := s.pending
					s.Unlock()
					if p <= 0 {
						return
					}
				}
			}()
		} else if s.pending == 1 {
			s.pending = 2
		}
	} else if extra[0] == 0 {
		s.pending = 0
	}
}

func (s *Sols) noise(row []byte) []byte {
	const max = 5
	const min = -1
	out := make([]byte, len(row))
	for i, r := range row {
		if r == 0xf5 {
			out[i] = row[i]
		} else {
			if o := int(row[i]) + rand.Intn(max - min) + min; o < 0 {
				out[i] = 0
			} else if o > 255 {
				out[i] = 255
			} else {
				out[i] = byte(o)
			}
		}
	}
	return out
}