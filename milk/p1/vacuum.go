package p1

import (
	"fmt"
)

// See gitlab.com/jmireles/rt2019src/java/com/boumatic/lines/pm/Vacuum.java

const (
	// 1 inch of mercury equals 3.386388 kPa

	// These constants are used to convert from 8 bits to Hg inches.
	// To convert to kPa we use k = 62.22222 * (8bits - offset) / 238,
	// where offset must be 17. Finally to convert from kPa to Hg
	// inches we divide by 3.38638.
	HG_O8  = 17
	HG_F8  = float64(62.2222 / (238 * 3.386388)) // 0.0772
	KPA_O8 = 17
	KPA_F8 = float64(62.2222 / 238)

	// These constants are used to convert from 10 bits to Hg inches.
	// To convert to kPa we use k = 62.2222 * (10bits - offset) / 955,
	// where offset must be 68. Finally to convert from kPa to Hg
	// inches we divide 3.386388. */
	HG_O10 = 68
	HG_F10 = float64(62.2222 / (955 * 3.386388)) // 0.01924
	KPA_O10 = 68
	KPA_F10 = float64(62.2222 / 955)
)

const (
	NORMAL     = byte(1)
	NOT_MARKED = byte(2)
	MARK       = byte(245) // -11
	MAX_SIZE   = 120
)

type Pressure string

const (
	HgInch     Pressure = "\"Hg"
	KiloPascal Pressure = "kPa"
)

type Bytes bool

const (
	Eight Bytes = false
	Ten   Bytes = true
)

func PressureValue(p Pressure, counts int, bytes Bytes) (v float64, e error) {
	
	switch p {

	case HgInch:
		if bytes == Eight {
			v = HG_F8 * (float64(0xFF & counts) - HG_O8)
		} else { // Ten
			v = HG_F10 * float64(counts - HG_O10)
		}

	case KiloPascal:
		if bytes == Eight {
			v = KPA_F8 * (float64(0xFF & counts) - KPA_O8)
		} else { // Ten
			v = KPA_F10 * float64(counts - KPA_O10)
		}

	default:
		e = fmt.Errorf("Invalid pressure: %v", p)
	}
	return
}


func PressureCounts(p Pressure, value float64, bytes Bytes) (c int, e error) {

	switch p {

	case HgInch:
		if bytes == Eight {
			c = int(float64(HG_O8)  + 0.5 + value / HG_F8)
		} else { // Ten
			c = int(float64(HG_O10) + 0.5 + value / HG_F10)
		}

	case KiloPascal:
		if bytes == Eight {
			c = int(float64(KPA_O8)  + 0.5 + value / KPA_F8) 
		} else { // Ten
			c = int(float64(KPA_O10) + 0.5 + value / KPA_F10)
		}

	default:
		e = fmt.Errorf("Invalid pressure: %v", p)
	}	
	return
}
