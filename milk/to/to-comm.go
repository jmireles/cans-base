package to

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

// A Comm is a request where command is not type serial.
type Comm struct {
	to    To
	dst   byte
	comm  byte
	extra []byte
	utc   base.UTC
}

func NewComm(to To, dst byte, utc base.UTC) *Comm {
	return &Comm{
		to:  to,
		dst: dst,
		utc: utc,
	}
}

func (b *Comm) UTC() base.UTC {
	return b.utc
}

func (c *Comm) GetVersion() {
	c.comm = byte(milk.Get00_Version)
}

func (r *Comm) GetSerial() {
	r.comm = byte(milk.Get01_Serial)
}

func (r *Comm) GetStatus() {
	r.comm = byte(milk.Get02_Status)
}

func (r *Comm) Get(comm milk.Get, extra []byte) {
	r.comm = byte(comm)
	r.extra = extra
}

func (r *Comm) Set(comm milk.Set, extra []byte) {
	r.comm = byte(comm)
	r.extra = extra
}

func (r *Comm) Bytes() Bytes {
	out := make([]byte, 4 + len(r.extra))
	out[0] = byte(r.to)
	out[1] = r.dst
	out[2] = byte(0) // src always 0
	out[3] = r.comm
	copy(out[4:], r.extra)
	return Bytes(out)
}

