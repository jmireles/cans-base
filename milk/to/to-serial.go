package to

import (
	"errors"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

// A Serial is a request where command is type serial
type Serial struct {
	serial []byte
	utc    base.UTC
}

func NewSerial(serial []byte, utc base.UTC) (*Serial, error) {
	if len(serial) != 3 {
		return nil, errors.New("Serial length not 3")
	}
	return &Serial{
		serial: serial,
		utc:    utc,
	}, nil
}

func (s *Serial) UTC() base.UTC {
	return s.utc
}

// +----+----+----+------+------+
// | S1 | S2 | S3 | 0x1F | 0x03 |
// +----+----+----+------+------+
func (s *Serial) ShowBytes() Bytes {
	return Bytes([]byte{
		s.serial[0],
		s.serial[1],
		s.serial[2],
		byte(milk.Get31_Serial),
		milk.Serial03_Show,
	})
}

// +----+----+----+------+------+-----+----+----+----+
// | S1 | S2 | S3 | 0x1F | 0x02 | dst | S1 | S2 | S3 |
// +----+----+----+------+------+-----+----+----+----+
func (s *Serial) NewIdBytes(dst byte) Bytes {
	return Bytes([]byte{
		s.serial[0],
		s.serial[1],
		s.serial[2],
		byte(milk.Get31_Serial),
		milk.Serial02_Set,
		dst,
		s.serial[0],
		s.serial[1],
		s.serial[2],
	})
}

