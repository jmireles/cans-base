package to

import (
	"fmt"
)

import (
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
)

type To byte

const (
	PC To = 0x00 // computer
	P1 To = 0x01 // pulsator monitor
	TO To = 0x02 // single takeoff
	MM To = 0x03 // milk-meter
	RF To = 0x04 // rfid
	SO To = 0x05 // sorter
	RS To = 0x06 // rotary stall identifier
	T4 To = 0x07 // four takeoffs
	I4 To = 0x08 // four inputs
)

func New(s string) (To, error) {
	switch s {
	case milk.PC:
		return PC, nil
	case milk.P1:
		return P1, nil
	case milk.RS:
		return RS, nil
	case milk.T4:
		return T4, nil
	case milk.I4:
		return I4, nil
	default:
		return To(0x0F), fmt.Errorf("Invalid string:%s", s)
	}
}

func NewTo(b byte) (To, error) {
	switch b {
	case 0x00:
		return PC, nil
	case 0x01:
		return P1, nil
	case 0x06:
		return RS, nil
	case 0x07:
		return T4, nil
	case 0x08:
		return I4, nil
	default:
		return To(0x0F), fmt.Errorf("To: Invalid byte:%d", b)
	}
}

func (to To) ReqBytes(dst, comm byte, extra []byte) []byte {
	data := make([]byte, 4 + len(extra))
	data[0] = byte(to) // 0000LLLL
	data[1] = dst      // destination (0,1,2...) 0=broadcast
	data[2] = byte(0)  // source always = 0 (this computer)
	data[3] = comm
	copy(data[4:], extra) // command + data
	return data
}

func (to To) ReqGetBytes(dst byte, comm milk.Get, extra []byte) []byte {
	return to.ReqBytes(dst, byte(comm), extra)
}

func (to To) ReqSetBytes(dst byte, comm milk.Set, extra []byte) []byte {
	return to.ReqBytes(dst, byte(comm), extra)
}

func (to To) Name() (name string, err error) {
	switch to {
	case PC:
		name = milk.PC
	case P1:
		name = milk.P1
	case TO:
		name = milk.TO
	case MM:
		name = milk.MM
	case RF:
		name = milk.RF
	case SO:
		name = milk.SO
	case RS:
		name = milk.RS
	case T4:
		name = milk.T4
	case I4:
		name = milk.I4
	default:
		err = fmt.Errorf("Invalid name [%d]", to)
	}
	return
}

func (to To) Req(comm byte) (*milk.Get, *milk.Set) {
	switch to {
	case PC:
		get := milk.Get(comm)
		return &get, nil
	case P1:
		return p1.Req(comm)
	case RS:
		return rs.Req(comm)
	case T4:
		return t4.Req(comm)
	case I4:
		return i4.Req(comm)
	default:
		return nil, nil
	}
}

func (to To) Payload(comm byte) (int, error) {

	switch comm {
	case byte(milk.Get00_Version): return 0, nil
	case byte(milk.Get01_Serial):  return 0, nil
	case byte(milk.Get02_Status):  return 0, nil
	}

	switch to {
	case P1:
		return p1.PayloadTo(comm)
	//case RS:
	//	return rs.PayloadTo(comm)
	//case T4:
	//	return t4.PayloadTo(comm)
	//case I4:
	//	return i4.PayloadTo(comm)
	default:
		return 0, fmt.Errorf("Invalid line:%d", to)
	}
}


type Bytes []byte