package to

import (
	"fmt"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

type Dst struct {
	Net  base.Net
	Line To
	Box  byte
}

// NewDst creates a Dst with given net, line and box.
func NewDst(net byte, line To, box byte) (*Dst, error) {
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else {
		return &Dst{Net:net, Line:line, Box:box}, nil
	}
}

func (d *Dst) Key() string {
	return base.BoxKey(d.Net, byte(d.Line), d.Box)
}

type Can struct {
	*Dst
	Get   *milk.Get
	Set   *milk.Set
	Extra []byte
}

func NewCan(dst *Dst, comm byte, extra []byte) (*Can, error) {
	if err := base.ExtraValid(extra); err != nil {
		return nil, err
	} else if get, set := dst.Line.Req(comm); get == nil && set == nil {
		return nil, fmt.Errorf("Invalid line:%d, comm:%d", dst.Line, comm)
	} else {
		return &Can{
			Dst:   dst,
			Get:   get,
			Set:   set,
			Extra: extra,
		}, nil
	}
}

func NewCanGet(dst *Dst, get milk.Get, extra []byte) (*Can, error) {
	return NewCan(dst, byte(get), extra)
}

func NewCanSet(dst *Dst, set milk.Set, extra []byte) (*Can, error) {
	return NewCan(dst, byte(set), extra)
}

func (c *Can) Comm() (byte, error) {
	if c.Get != nil {
		return byte(*c.Get), nil
	} else if c.Set != nil {
		return byte(*c.Set), nil
	} else {
		return 0, fmt.Errorf("Invalid get/set command")
	}
}

// NewCanReq returns a Can from cans-server API post URL. Examples:
//	"/can/2/req/1,7,0,4,1,17,33,49,65,81,97,113"
func NewCanHttp(path string) (*Can, error) {
	format := "/can/%d/req/%d,%d,0,%s"
	can := &Can{
		Dst: &Dst{},
	}
	var net byte
	var line byte
	var lastS string 
	if _, err := fmt.Sscanf(path, format, &net, &line, &can.Box, &lastS); err != nil {
		return nil, fmt.Errorf("Invalid path:%s", path)
	}
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else {
		can.Net = net
	}
	if line, err := NewTo(line); err != nil {
		return nil, err
	} else {
		can.Line = line
	
		last := make([]byte, 0)
		for _, se := range strings.Split(lastS, ",") {
			var e int
			if _, err := fmt.Sscanf(se, "%d", &e); err != nil {
				return nil, err
			} else if e < 0 || e > 255 {
				return nil, fmt.Errorf("Byte out of range [0-255]")
			} else {
				last = append(last, byte(e))
			}
		}
		if len(last) > 9 {
			return nil, fmt.Errorf("Extras bytes length greater than 8")
		}
		comm := last[0]
		can.Get, can.Set = line.Req(comm)
		can.Extra = last[1:]
		return can, nil
	}
}

// Key returns an Identifier for this HttpCan
// this.net + this.line.ReqBytes(this.box, comm, extra)
func (c *Can) Key(comm byte, extra []byte) string {
	return fmt.Sprintf("%1d%02x", c.Net, c.Line.ReqBytes(c.Box, comm, extra))
}

func (c *Can) Data() []byte {
	if c.Get != nil {
		return c.Line.ReqGetBytes(c.Box, *c.Get, c.Extra)
	} 
	if c.Set != nil {
		return c.Line.ReqSetBytes(c.Box, *c.Set, c.Extra)
	}
	return nil
}

func (c *Can) HttpResp() []byte {
	data := c.Data()
	r, _ := milk.NewHttpResp(byte(c.Net), data, uint(len(data)))
	return []byte(r.Json())
}

