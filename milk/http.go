package milk

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/jmireles/cans-base"
)

// HttpResp is a cans request response used as JSON for HTTP posts
// Normally includes three fields can number, data bytes and sent size posted
// Eventually can include single field error as string
type HttpResp struct {
	base.Net
	Data
	Sent  uint
	Error string
}

// NewHttpResp builds a HttpResp with can, data and sent bytes
// Returns error when can and data are out of range
func NewHttpResp(net byte, data []byte, sent uint) (*HttpResp, error) {
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else if data, err := NewData(data); err != nil {
		return nil, err
	} else {
		return &HttpResp{Net:net, Data:data, Sent:sent}, nil
	}
}

// NewHttpRespError builds a HttpResp with field error set
// Used to send error detail to HTTP clients.
func NewHttpRespError(err string) *HttpResp {
	return &HttpResp{Error:err}
}

// NewHttpRespJson builds a HttpResp parsing a JSON string.
func NewHttpRespJson(s string) (*HttpResp, error) {
	type T struct {
		Error string `json:"error,omitempty"`
		Net   int    `json:"net,omitempty"`
		Data  []int  `json:"data,omitempty"`
		Sent  uint   `json:"sent,omitempty"`
	}
	var t T
	if err := json.Unmarshal([]byte(s), &t); err != nil {
		return nil, err
	}
	if t.Error != "" {
		return NewHttpRespError(t.Error), nil
	}
	data := make([]byte, len(t.Data))
	for i, d := range t.Data {
		if d < 0 || d > 255 {
			return nil, errors.New("Data byte out of range")
		} else {
			data[i] = byte(d)
		}
	}
	return NewHttpResp(byte(t.Net), data, t.Sent)

}

// Json returns the JSON representation of HttpResp
// For errors returns single field error
// For success returns three fields:
//	net, data, sent
// always in this order.
func (h *HttpResp) Json() string {
	comma := ""
	var sb strings.Builder
	sb.WriteString(`{`)
	if h.Error != "" {
		sb.WriteString(fmt.Sprintf(`%s"error":%q`, comma, h.Error))
		comma = ","
	}
	if h.Net > 0 {
		sb.WriteString(fmt.Sprintf(`%s"net":%d`, comma, int(h.Net)))
		comma = ","
	}
	if len(h.Data) > 0 {
		sb.WriteString(fmt.Sprintf(`%s"data":[`, comma))
		for pos, b := range h.Bytes() {
			if pos > 0 {
				sb.WriteString(",")
			}
			sb.WriteString(fmt.Sprintf("%d", int(b)))
		}
		sb.WriteString(`]`)
		comma = ","		
	}
	if h.Sent > 0 {
		sb.WriteString(fmt.Sprintf(`%s"sent":%d`, comma, h.Sent))
		comma = ","
	}
	sb.WriteString(`}`)
	return sb.String()
}
