package line

import (
	"context"
	"encoding/hex"
	"fmt"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/to"
)

func NewSims(ctx context.Context, net byte, boxes map[string]*Sim) (*Msgs, chan []byte) {
	resp    := make(chan *Msg, 1)
	req     := make(chan *Msg, 1)
	unsol   := make(chan []byte, 1)
	payload := make(chan []byte, 1)
	go func() {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()
		for {
			select {

			case p := <-unsol:
				if utc, err := base.UTCNow(); err != nil {
					// silent error
				} else if msg, err := NewMsg(net, p, utc); err != nil {
					// silent error
				} else {
					resp<- msg
				}
			
			case p := <-payload:
				if utc, err := base.UTCNow(); err != nil {
					// silent error
				} else if msg, err := NewMsg(net, p, utc); err != nil {
					// silent error
				} else {
					resp<- msg
				}

			case msg := <-req:
				if net != byte(msg.Net) {
					break
				}
				data := msg.Data
				to, err := to.NewTo(data.Line())
				if err != nil {
					break // silent error
				}
				line, err := from.NewFrom(byte(to << 4))
				if err != nil {
					break // silent error
				}
				dst := data.Dst()
				switch data.Comm() {

				case byte(milk.Get00_Version):
					for _, b := range boxes {
						if b.line == line && (b.id == dst || 0 == dst) {
							go b.reqVersion(payload)
						}
					}

				case byte(milk.Get01_Serial):
					for key, b := range boxes {
						if serial, err := hex.DecodeString(key); err != nil {
							// silent error
						} else {
							go b.reqSerial(payload, serial)
						}
					}

				case byte(milk.Get31_Serial):
					if extra := data.Extra(); len(extra) == 0 {
						// return
					} else if extra[0] == 3 { // show
						serial := fmt.Sprintf("%02x", data[:3])
						for key, b := range boxes {
							if key == serial {
								go b.reqShow()
							}
						}
					}

				default:
					for _, b := range boxes {
						if b.line == line && (b.id == dst || 0 == dst) {
							go b.req(payload, data)
						}
					}
				}

			case <- ctx.Done():
				return
			}
		}
	}()
	return NewMsgs(resp, req), unsol
}

var (
	delay100 = 100 * time.Millisecond
)



type Sim struct {
	line    from.From
	version byte
	id      byte
	sols    milk.SimSols
}

func NewSim(line from.From, version, id byte) *Sim {
	sim := &Sim{
		line:    line,
		version: version,
		id:      id,
	}
	switch line {
	case from.P1:
		sim.sols = p1.NewSimSols(pmSols4)
	}
	return sim
}

func (s *Sim) reqVersion(payload chan []byte) {
	// TODO synch requests
	payload<- s.line.SolSim(s.id, milk.Sol00_Version, []byte{
		s.version,
	})
	time.Sleep(delay100)
}

func (s *Sim) reqSerial(payload chan []byte, serial []byte) {
	payload<- s.line.SolSimSerial(s.id, serial)
	time.Sleep(delay100)
}

func (s *Sim) reqShow() {
	time.Sleep(delay100)
}

func (s *Sim) req(payload chan []byte, data milk.Data) {
	if s.sols == nil {
		return
	}
	s.sols.Req(data, func(sol milk.Sol, extra []byte) {
		payload<- s.line.SolSim(s.id, sol, extra)
	})
}






var pmSols4 = [][]uint8{
	[]uint8{0xf5, 0xb7, 0x28, 0xa8, 0x3f, 0x99, 0x56, 0x8a},
	[]uint8{0x6d, 0x7b, 0x84, 0x6c, 0x9b, 0x5e, 0xb2, 0x4f},
	[]uint8{0xb9, 0x40, 0xb9, 0x31, 0xb9, 0x22, 0xb9, 0x13},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11, 0xb9, 0x11},
	[]uint8{0xb9, 0x11, 0xb9, 0x27, 0xb9, 0x3c, 0xb9, 0x51},
	[]uint8{0xb9, 0x66, 0xa2, 0x7b, 0x8b, 0x91, 0x73, 0xa6},
	[]uint8{0x5c, 0xb7, 0x45, 0xb7, 0x2d, 0xb7, 0x16, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
	[]uint8{0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7, 0x11, 0xb7},
}






