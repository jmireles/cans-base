package line

import(
	"fmt"
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/t4"
	"gitlab.com/jmireles/cans-base/milk/to"
)


func TestMsg20(t *testing.T) {
	tt := base.Test{ T:t }
	for _, bad := range [][]byte {
		[]byte {},
		[]byte {  1,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19       },
		[]byte {  1,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20,21 },
		[]byte {  0,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20    },
		[]byte { 16,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20    },
	} {
		if _, err := NewMsg20(bad); err == nil {
			t.Fatalf("No error bad bytes %02x", bad)
		} else {
			//t.Logf(": %v", err)
		}
	}
	data := "02030405060708090a0b0c0d"
	utc  := "0e0f1011121314"
	for _, ok := range [][]byte {
		[]byte { 1,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20 },
		[]byte { 2,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20 },
		[]byte { 3,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20 },
		[]byte { 4,2,3,4,5,6,7,8,9,10,11,12,13, 14,15,16,17,18,19,20 },
	} {
		m, err := NewMsg20(ok)
		tt.Ok(err)
		tt.Equals(fmt.Sprintf("%02x", m.Data), data)
		tt.Equals(fmt.Sprintf("%02x", m.utc), utc)
		//t.Logf("m net=%d data=%02x utc=%02x", m.Net, m.Data, m.Utc)
	}
}


func TestMsgLine(t *testing.T) {
	tt := base.Test{t}
	type params struct {
		can   byte
		bytes []byte
	}
	for _, bad := range []*params {
		&params{can:0, bytes:[]byte{1,2,3,4}}, // invalid net=0
		&params{can:16, bytes:[]byte{1,2,3,4}}, // invalid net=16
		&params{can:1}, // invalid bytes
		&params{can:1, bytes:[]byte{1,2,3}}, // bytes len < 4
		&params{can:1, bytes:[]byte{1,2,3,4,5,6,7,8,9,10,11,12,13}}, // bytes len > 12
	} {
		now, err := base.UTCNow()
		tt.Ok(err)
		_, err = NewMsg(bad.can, bad.bytes, now)
		tt.Assert(err != nil, "Unexpected mess can:%d bytes:% 02x", bad.can, bad.bytes)
	}
	// requests
	now, err := base.UTCNow()
	tt.Ok(err)
	mess, err := NewMsg(1, []byte{1,7,0,4,1,11,12,13,14,15,16}, now)
	tt.Ok(err)
	serial, lineT, id, err := mess.ParseReq()
	tt.Ok(err)
	tt.Equals(false, serial)
	tt.Equals(to.P1, lineT)
	tt.Equals(byte(7), id)
	// responses
	now, err = base.UTCNow()
	tt.Ok(err)
	mess, err = NewMsg(1, []byte{0x10,0,7,3}, now)
	tt.Ok(err)
	serial, lineF, id, err := mess.ParseResp()
	tt.Ok(err)
	tt.Equals(false, serial)
	tt.Equals(from.P1, lineF)
	tt.Equals(byte(7), id)

	// p1 get
	g := p1.Get07_Calibr
	get, set := p1.Req(byte(g))
	tt.Assert(!(get == nil || set != nil), "get == nil || set != nil")
	tt.Equals(g, *get)
	// p1 set
	s := p1.Set14_Display
	get, set = p1.Req(byte(s))
	tt.Assert(!(get != nil || set == nil), "get != nil || set == nil")
	tt.Equals(s, *set)

	// t4 get
	g = t4.Get04_Equipment
	get, set = t4.Req(byte(g))
	tt.Assert(!(get == nil || set != nil), "get == nil || set != nil")
	tt.Equals(g, *get)
	// t4 set
	s = t4.Set13_Colors
	get, set = t4.Req(byte(s))
	tt.Assert(!(get != nil || set == nil), "get != nil || set == nil")
	tt.Equals(s, *set)

	// p1 sol
	rs := p1.Sol14_ValLevelsFront
	sol, unsol := p1.Resp(byte(rs))
	tt.Assert(!(sol == nil || unsol != nil), "sol == nil || unsol != nil")
	tt.Equals(rs, *sol)
	// p1 unsol
	u := p1.Unsol16_NoPhaseFront
	sol, unsol = p1.Resp(byte(u))
	tt.Assert(!(sol != nil || unsol == nil), "sol != nil || unsol == nil")
	tt.Equals(u, *unsol)

	// t4 sol
	rs = t4.Sol06_Profiles
	sol, unsol = t4.Resp(byte(rs))
	tt.Assert(!(sol == nil || unsol != nil), "sol == nil || unsol != nil")
	tt.Equals(rs, *sol)
	// t4 unsol
	u = t4.Unsol13_Levels
	sol, unsol = t4.Resp(byte(u))
	tt.Assert(!(sol != nil || unsol == nil), "sol != nil || unsol == nil")
	tt.Equals(u, *unsol)

	dst, _ := to.NewDst(2, to.P1, 7)
	req, err := to.NewCanGet(dst, milk.Get00_Version, nil)
	tt.Ok(err)
	tt.Assert(!(req.Get == nil || req.Set != nil), "get != nil || set == nil")
	req, err = to.NewCanSet(dst, p1.Set04_Config, []byte{1,2,3,4,5,6,7,8})
	tt.Ok(err)
	tt.Assert(!(req.Get != nil || req.Set == nil), "get == nil || set != nil")

	// pm req commands see ..cans-base/p1/p1.go
	pmGets := []byte{ 0,1,2,3,5,6,7,9,11,13,15,17,19 }
	pmSets := []byte{ 4,8,10,12,14,16,18,20 }
	_, err = to.NewDst(16, to.P1, 0)
	tt.Assert(err != nil, "Not rejected net=16")
	dst, err = to.NewDst(4, to.P1, 0)
	tt.Ok(err)
	_, err = to.NewCan(dst, 21, nil)
	tt.Assert(err != nil, "No rejected PM comm=21 undefined")
	for _, exp := range pmGets {
		req, err := to.NewCan(dst, exp, nil)
		tt.Ok(err)
		tt.Equals(exp, byte(*req.Get))
	}
	for _, exp := range pmSets {
		req, err := to.NewCan(dst, exp, nil)
		tt.Ok(err)
		tt.Equals(exp, byte(*req.Set))
	}

	// pm resp commands see ..cans-base/p1/p1.go
	pmSols := []byte{ 0,2,3,4,8,9,10,11,12,13,14,15,18,21,22,23,24,25,27 }
	pmUnsols := []byte{ 1,5,6,7,16,17,20,26 }
	_, err = from.NewSrc(16, from.P1, 0)
	tt.Assert(err != nil, "Not rejected net=16")
	src, err := from.NewSrc(4, from.P1, 0)
	tt.Ok(err)
	_, err = from.NewCan(src, 32, nil)
	tt.Assert(err != nil, "Not rejected PM comm=32 undefined")
	for _, exp := range pmSols {
		resp, err := from.NewCan(src, exp, nil)
		tt.Ok(err)
		tt.Equals(exp, byte(*resp.Sol))
	}
	for _, exp := range pmUnsols {
		resp, err := from.NewCan(src, exp, nil)
		tt.Ok(err)
		tt.Equals(exp, byte(*resp.Unsol))
	}
}

func TestMsgBytes20(t *testing.T) {
	// Added for bug of SL config (last byte missing for 13 data)
	tt := base.Test{t}
	data := []byte{
		1,2,3,4,5,6,7,8,9,10,11,12, // last 12 wasn't added and was read as 0.
	}

	if msgs, err := NewMsgNet0(data); err != nil {
		tt.Ok(err)	
	} else {
		twenty := msgs.Bytes20()[:13]
		tt.Equals(twenty, []byte{
			0,1,2,3,4,5,6,7,8,9,10,11,12,
		})
	}
}




