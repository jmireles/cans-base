package line

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
	"gitlab.com/jmireles/cans-base/milk/to"
)

// The ID#Data was collected from cans-kernel/socket tests
// connecting real T4/PM to raspberryPI-HAT CAN connector

// Take it from cans-server/ports/socks2_test.go
func TestCanRx(t *testing.T) {
	bt := base.Test{t}
	net := byte(1)
	can := Can{}
	var sbytes   []byte
	var resp     bool
	var err      error
	var msg      *Msg
	var serial   bool
	var fromLine from.From
	var line     to.To
	var id       byte
	var comm     byte

	for _, tt := range[]struct{
		in     string
		serial bool
		from   from.From
		id     byte
		unsol  milk.Unsol
		sol    milk.Sol
		data   []byte
	} {
		// T4 unsols
		{
			in:   "0E000021#",
			from: from.T4,
			id:   1,   
			unsol: milk.Unsol01_Restart,
		},
		{
    		in:   "0E000029#0100070000",
			from:  from.T4,
			id:    1,   
			unsol: t4.Unsol09_Stage,
    		data:  []byte{1,0,7},
    	},
    	{
    		in:   "0E000029#0201070000",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol09_Stage,
    		data: []byte{2,1,7},
    	},
    	{
    		in:   "0E000029#0302070000",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol09_Stage,
    		data: []byte{3,2,7},
    	},
    	{
    		in:   "0E000029#0403070000",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol09_Stage,
    		data: []byte{4,3,7},
    	},
    	{
    		in:   "0E00002E#010004040401",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol14_Colors,
    		data: []byte{1,0,4,4,4,1},
    	},
    	{
    		in:   "0E00002E#020104040401",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol14_Colors,
    		data: []byte{2,1,4,4,4,1},
    	},
    	{
    		in:   "0E00002E#030204040401",
			from: from.T4,
			id:   1,   
			unsol: t4.Unsol14_Colors,
    		data: []byte{3,2,4,4,4,1},
    	},
    	{
    		in:  "0E00002E#040304040401",
			from: from.T4,
			id: 1,   
			unsol: t4.Unsol14_Colors,
    		data: []byte{4,3,4,4,4,1},
    	},
    	{
    		in:   "0E000020#1C",
			from: from.T4,
			id:   1,   
			sol:  milk.Sol00_Version,
    		data: []byte{0x1C},
    	},
    	{
    		in:     "0A96201F#000754B10001",
    		serial: true,
			from:   from.T4,
			id:     1,   
			sol:    milk.Sol31_Serial,
    		data:   []byte{0x00,0x07,0x54,0xB1,0x00,0x01},
    	},
    	{
    		in:   "0E000022#08080808",
			from: from.T4,
			id:   1,   
			sol:  milk.Sol02_Status,
    		data: []byte{8,8,8,8},
    	},
    	{
     		in:   "0E000023#0001020304010101",
			from: from.T4,
			id:   1,   
			sol:  t4.Sol03_Stalls,
     		data: []byte{0,1,2,3,4,1,1,1},
    	},
    	{
    		in:   "0E000023#0101050607080101",
			from: from.T4,
			id:   1,   
			sol:  t4.Sol03_Stalls,
    		data: []byte{1,1,5,6,7,8,1,1},
    	},
    	{
    		in:   "0E000023#0201010600000000",
			from: from.T4,
			id:   1,   
			sol:  t4.Sol03_Stalls,
    		data: []byte{2,1,1,6,0,0,0,0},
    	},
    	{
    		in:   "0E000023#0300000000",
			from: from.T4,
			id:   1,   
			sol:  t4.Sol03_Stalls,
    		data: []byte{3,0,0,0,0},
    	},
    	{
    		in:   "02000020#18",
			from: from.P1,
			id:   1,   
			sol:  milk.Sol00_Version,
			data: []byte{0x18},
    	},
    	{
    		in:  "0200001F#000110000001",
    		serial: true,
			from: from.P1,
			id: 1,   
			sol: milk.Sol31_Serial,
			data: []byte{0,1,0x10,0,0,1},
    	},
    	{
    		in:   "02000022#7A0000",
			from: from.P1,
			id:   1,   
			sol:  milk.Sol02_Status,
			data: []byte{0x7A,0,0},
    	},
    	{
    		in:   "02000023#00013C28413C2814",
			from: from.P1,
			id:   1,   
			sol:  p1.Sol03_Config,
			data: []byte{0,1,0x3C,0x28,0x41,0x3C,0x28,0x14},
    	},
    	{
    		in:   "02000023#011403E800130184",
			from: from.P1,
			id:   1,   
			sol:  p1.Sol03_Config,
			data: []byte{1,0x14,3,0xE8,0,0x13,1,0x84},
    	},
    	{
    		in:   "02000023#02011003B803B800",
			from: from.P1,
			id:   1,   
			sol:  p1.Sol03_Config,
			data: []byte{2,1,0x10,3,0xB8,3,0xB8,0},
    	},
    	{
    		in:  "02000023#03AC2803B803B800",
			from: from.P1,
			id:   1,   
			sol:  p1.Sol03_Config,
			data: []byte{3,0xAC,0x28,3,0xB8,3,0xB8,0},
    	},
	} {
    	can.UnmarshalString(tt.in)

		sbytes, resp, err = CanRx(can)
		bt.Ok(err)
		now, _ := base.UTCNow()
		msg, err = NewMsg(net, sbytes, now)
		bt.Assert(resp, "Is no response")
    	serial, fromLine, id, err = msg.ParseResp()
    	bt.Ok(err)
    	bt.Equals(fromLine, tt.from)
    	bt.Equals(serial, tt.serial)
    	bt.Equals(id, tt.id)
    	comm = msg.Comm()
    	if comm == byte(tt.sol) {

    	} else if comm == byte(tt.unsol) {

    	} else {
    		t.Fatalf("got=%d expected=%d or %d", comm, tt.sol, tt.unsol)
    	}
    	data1 := []byte{0,0,0,0,0,0,0,0}
    	data2 := []byte{0,0,0,0,0,0,0,0}
    	copy(data1, msg.Extra())
    	copy(data2, tt.data)
    	bt.Equals(data1, data2)
    	_, err := fromLine.Name()
    	bt.Ok(err)
	}

	for _, tt := range[]struct {
		in     string
		serial bool
		to     to.To
		id     byte
		get    milk.Get
		data   []byte
	} {
    	// T4 req/sol
    	{
    		in:  "00E00000#",
    		to:  to.T4,
    		get: milk.Get00_Version,
    	},
    	{
    		in:  "00E00001#",
    		to:  to.T4,
    		get: milk.Get01_Serial,
    	},
    	{
    		in:  "00E00002#",
    		to:  to.T4,
    		get: milk.Get02_Status,
    	},
    	{
    		in:  "00E00003#",
    		to:  to.T4,
    		get: t4.Get03_Stalls,
    	},
    	// PM req/sol
    	{
    		in:  "00200000#",
    		to:  to.P1,
    		get: milk.Get00_Version,
    	},
    	{
    		in:  "00200001#",
    		to:  to.P1,
    		get: milk.Get01_Serial,
    	},
    	{
    		in:  "00200002#",
    		to:  to.P1,
    		get: milk.Get02_Status,
    	},
    	{
    		in:  "00200003#",
    		to:  to.P1,
    		get: t4.Get03_Stalls,
    	},
    } {
    	can.UnmarshalString(tt.in)

		sbytes, resp, err = CanRx(can)
		bt.Ok(err)
		now, _ := base.UTCNow()
		msg, err = NewMsg(net, sbytes, now)
		bt.Assert(!resp, "Is response")
		serial, line, id, err = msg.ParseReq()
		bt.Assert(!serial, "response serial")
		bt.Equals(line, tt.to)
    	

    	bt.Equals(id, tt.id)
    	bt.Equals(msg.Comm(), byte(tt.get))
    	_, err := line.Name()
    	bt.Ok(err)
    }
}

// Take it from cans-server/ports/socks2_test.go
func TestCanTx(t *testing.T) {
	bt := base.Test{t}
	for _, tt := range[]struct {
		bytes []byte
		ascii string
	} {
		// P1
		{
			bytes: to.P1.ReqGetBytes(0, milk.Get00_Version, nil),
			ascii: "00200000#",
		},
		{
			bytes: to.P1.ReqGetBytes(1, milk.Get01_Serial, nil),
			ascii: "00202001#",	
		},
		{
			bytes: to.P1.ReqGetBytes(2, milk.Get02_Status, nil),
			ascii: "00204002#",	
		},
		{
			bytes: to.P1.ReqGetBytes(3, p1.Get03_Config, nil),
			ascii: "00206003#",	
		},
		{
			bytes: to.P1.ReqSetBytes(4, p1.Set04_Config, []byte{1,2,3,4,5,6,7,8}),
			ascii: "00208004#0102030405060708",	
		},
		{
			bytes: to.P1.ReqGetBytes(5, p1.Get05_Values, []byte{1}),
			ascii: "0020A005#01",	
		},
		{
			bytes: to.P1.ReqGetBytes(6, p1.Get06_Averages, nil),
			ascii: "0020C006#",	
		},
		{
			bytes: to.P1.ReqGetBytes(7, p1.Get07_Calibr, nil),
			ascii: "0020E007#",	
		},
		{
			bytes: to.P1.ReqSetBytes(8, p1.Set08_Calibr, []byte{255}),
			ascii: "00210008#FF",	
		},
		{
			bytes: to.P1.ReqGetBytes(9, p1.Get09_Options, nil),
			ascii: "00212009#",	
		},
		{
			bytes: to.P1.ReqSetBytes(10, p1.Set10_Options, []byte{0,1}),
			ascii: "0021400A#0001",	
		},
		{
			bytes: to.P1.ReqGetBytes(11, p1.Get11_Profiles, nil),
			ascii: "0021600B#",	
		},
		{
			bytes: to.P1.ReqSetBytes(12, p1.Set12_Profiles, []byte{1,2,3}),
			ascii: "0021800C#010203",	
		},
		{
			bytes: to.P1.ReqGetBytes(13, p1.Get13_Display, nil),
			ascii: "0021A00D#",	
		},
		{
			bytes: to.P1.ReqSetBytes(14, p1.Set14_Display, []byte{1,2,3,4}),
			ascii: "0021C00E#01020304",	
		},
		{
			bytes: to.P1.ReqGetBytes(15, p1.Get15_Config, nil),
			ascii: "0021E00F#",	
		},
		{
			bytes: to.P1.ReqSetBytes(16, p1.Set16_Config, []byte{1,2,3,4,5}),
			ascii: "00220010#0102030405",	
		},
		{
			bytes: to.P1.ReqGetBytes(17, p1.Get17_Alarms, nil),
			ascii: "00222011#",	
		},
		{
			bytes: to.P1.ReqSetBytes(18, p1.Set18_Alarms, []byte{1,2,3,4,5,6}),
			ascii: "00224012#010203040506",	
		},
		{
			bytes: to.P1.ReqGetBytes(19, p1.Get19_Stalls, nil),
			ascii: "00226013#",	
		},
		{
			bytes: to.P1.ReqSetBytes(20, p1.Set20_Stalls, []byte{1,2,3,4,5,6,7}),
			ascii: "00228014#01020304050607",	
		},
		// RS gets
		{
			bytes: to.RS.ReqGetBytes(0, milk.Get00_Version, nil),
			ascii: "00C00000#",
		},
		{
			bytes: to.RS.ReqGetBytes(1, milk.Get01_Serial, nil),
			ascii: "00C02001#",	
		},
		{
			bytes: to.RS.ReqGetBytes(2, milk.Get02_Status, nil),
			ascii: "00C04002#",	
		},
		{
			bytes: to.RS.ReqGetBytes(3, rs.Get03_Config, nil),
			ascii: "00C06003#",	
		},
		{
			bytes: to.RS.ReqSetBytes(4, rs.Set04_Config, []byte{1}),
			ascii: "00C08004#01",	
		},

		// T4 gets
		{
			bytes: to.T4.ReqGetBytes(0, milk.Get00_Version, nil),
			ascii: "00E00000#",
		},
		{
			bytes: to.T4.ReqGetBytes(1, milk.Get01_Serial, nil),
			ascii: "00E02001#",	
		},
		{
			bytes: to.T4.ReqGetBytes(2, milk.Get02_Status, nil),
			ascii: "00E04002#",	
		},
		{
			bytes: to.T4.ReqGetBytes(3, t4.Get03_Stalls, nil),
			ascii: "00E06003#",	
		},
		{
			bytes: to.T4.ReqGetBytes(4, t4.Get04_Equipment, nil),
			ascii: "00E08004#",	
		},
		{
			bytes: to.T4.ReqGetBytes(5, t4.Get05_Milking, nil),
			ascii: "00E0A005#",	
		},
		{
			bytes: to.T4.ReqGetBytes(6, t4.Get06_Profiles, nil),
			ascii: "00E0C006#",	
		},
		{
			bytes: to.T4.ReqGetBytes(7, t4.Get07_Colors, nil),
			ascii: "00E0E007#",	
		},
		{
			bytes: to.T4.ReqGetBytes(8, t4.Get08_Palette, nil),
			ascii: "00E10008#",	
		},
		{
			bytes: to.T4.ReqSetBytes(9, t4.Set09_Stalls, []byte{1}),
			ascii: "00E12009#01",	
		},
		{
			bytes: to.T4.ReqSetBytes(10, t4.Set10_Equipment, []byte{1,2}),
			ascii: "00E1400A#0102",	
		},
		{
			bytes: to.T4.ReqSetBytes(11, t4.Set11_Milking, []byte{1,2,3}),
			ascii: "00E1600B#010203",	
		},
		{
			bytes: to.T4.ReqSetBytes(12, t4.Set12_Profiles, []byte{1,2,3,4}),
			ascii: "00E1800C#01020304",	
		},
		{
			bytes: to.T4.ReqSetBytes(13, t4.Set13_Colors, []byte{1,2,3,4,5}),
			ascii: "00E1A00D#0102030405",	
		},
		{
			bytes: to.T4.ReqSetBytes(14, t4.Set14_Pallete, []byte{1,2,3,4,5,6}),
			ascii: "00E1C00E#010203040506",	
		},
		{
			bytes: to.T4.ReqGetBytes(15, t4.Get15_Levels, []byte{1,2,3,4,5,6,7}),
			ascii: "00E1E00F#01020304050607",	
		},
		{
			bytes: to.T4.ReqGetBytes(16, t4.Get16_Rotary, nil),
			ascii: "00E20010#",	
		},
		{
			bytes: to.T4.ReqSetBytes(17, t4.Set17_Rotary, []byte{1,2,3,4,5,6,7,8}),
			ascii: "00E22011#0102030405060708",	
		},
		// I4 gets
		{
			bytes: to.I4.ReqGetBytes(0, milk.Get00_Version, nil),
			ascii: "01000000#",
		},
		{
			bytes: to.I4.ReqGetBytes(1, milk.Get01_Serial, nil),
			ascii: "01002001#",	
		},
		{
			bytes: to.I4.ReqGetBytes(2, milk.Get02_Status, nil),
			ascii: "01004002#",	
		},
		{
			bytes: to.I4.ReqGetBytes(3, i4.Get03_Config, nil),
			ascii: "01006003#",	
		},
		{
			bytes: to.I4.ReqSetBytes(4, i4.Set04_Config, []byte{1}),
			ascii: "01008004#01",	
		},

	} {
		can, err := CanTx(tt.bytes)
		bt.Ok(err)
		bt.Equals(can.String(), tt.ascii)
	}
}
