package line

import (
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

const (
	canIdBits    = 11 // standard
	canExtIdBits = 29 // extended
)

// Can format constants
const (
	canMaxId    = 0x7ff      // standard
	canMaxExtId = 0x1fffffff // extended
)

// Can represents a CAN frame.
//
// A Can fit into 16 bytes on common architectures
// and is therefore amenable to pass-by-value and judicious copying.
type Can struct {
	// ID is the Can ID
	ID uint32
	// Length is the number of bytes of data in the can.
	Length uint8
	// Extra is the can extra data
	Extra base.Extra
	// IsRemote is true for remote cans.
	IsRemote bool
	// IsExtended is true for extended cans (29 bits IDs).
	IsExtended bool
}

// Validates returns anr eror if the Can is not a valid Can can.
func (c *Can) Validate() error {
	// Validate ID
	if c.IsExtended && c.ID > canMaxExtId {
		return fmt.Errorf(
			"invalid extended Can id: %v does not fit in %v bits",
			c.ID,
			canExtIdBits,
		)
	} else if !c.IsExtended && c.ID > canMaxId {
		return fmt.Errorf(
			"invalid standard Can id: %v does not fit in %v bits",
			c.ID,
			canIdBits,
		)
	}
	// Validate Data
	if c.Length > base.ExtraCanDataMaxLength {
		return fmt.Errorf("invalid data length: %v", c.Length)
	}
	return nil
}

// String returns an ASCII representation of the Can can.
//
// Format:
//
//	([0-9A-F]{3}|[0-9A-F]{3})#(R[0-8]?[0-9A-F]{0,16})
//
// The format is compatible with the candump(1) log file format.
func (c Can) String() string {
	var id string
	if c.IsExtended {
		id = fmt.Sprintf("%08X", c.ID)
	} else {
		id = fmt.Sprintf("%03X", c.ID)
	}
	if c.IsRemote && c.Length == 0 {
		return id + "#R"
	} else if c.IsRemote {
		return id + "#R" + strconv.Itoa(int(c.Length))
	}
	return id + "#" + strings.ToUpper(hex.EncodeToString(c.Extra[:c.Length]))
}

// UnmarshalString sets *c using the provided ASCII representation of a Can.
func (c *Can) UnmarshalString(s string) error {
	// split into parts
	parts := strings.Split(s, "#")
	if len(parts) != 2 {
		return fmt.Errorf("invalid can format: %v", s)
	}
	id, data := parts[0], parts[1]
	var can Can
	// Parse: IsExtended
	if len(id) != 3 && len(id) != 8 {
		return fmt.Errorf("invalid ID length: %v", s)
	}
	can.IsExtended = len(id) == 8
	// Parse: ID
	uid, err := strconv.ParseUint(id, 16, 32)
	if err != nil {
		return fmt.Errorf("invalid can ID: %v", s)
	}
	can.ID = uint32(uid)
	if len(data) == 0 {
		*c = can
		return nil
	}
	// Parse: IsRemote
	if data[0] == 'R' {
		can.IsRemote = true
		if len(data) > 2 {
			return fmt.Errorf("invalid remote length: %v", s)
		} else if len(data) == 2 {
			dataLength, err := strconv.Atoi(data[1:2])
			if err != nil {
				return fmt.Errorf("invalid remote length: %v: %w", s, err)
			}
			can.Length = uint8(dataLength)
		}
		*c = can
		return nil
	}
	// Parse: Length
	if len(data) > 16 || len(data)%2 != 0 {
		return fmt.Errorf("invalid data length: %v", s)
	}
	can.Length = uint8(len(data) / 2)
	// Parse: Data
	dataBytes, err := hex.DecodeString(data)
	if err != nil {
		return fmt.Errorf("invalid data: %v: %w", s, err)
	}
	copy(can.Extra[:], dataBytes)
	*c = can
	return nil
}

// Code take it from ../cans-server/ports/socks2.go
// In order ../cans-cfg/sim/vcan can convert from sockets Frames
// TODO, remove there and call this

// CanRx coverts a Can to bytes
//	     0        1        2      3    4      5        6        7        8        9     10 11
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┬──┬──┐
//	│LLLL0000     0        S    comm  xx     xx       xx       xx       xx       xx     xx xx│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┴──┴──┘
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┐
//	│YYYMMMMD DDDDSSSS CCCCCCCC  31   sub 0000LLLL YYYMMMMD DDDDSSSS CCCCCCCC IIIIIIIII│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┘
func CanRx(can Can) (data milk.Data, resp bool, err error) {
	if !can.IsExtended {
		err = errors.New("Not 29 bits")
		return
	} else if invalid := can.Validate(); invalid != nil {
		err = invalid
		return
	}
	data = milk.Data(make([]byte, 12))
	id := can.ID // 29 bits first four bytes
	data[3] = byte(id & 0x1f) // comm  ------------------------xxxxx
	id >>= 5
	data[2] = byte(id & 0xff) // src   ----------------SSSSSSSS
	id >>= 8
	data[1] = byte(id & 0xff) // dst   --------00000000
	id >>= 8
	data[0] = byte(id & 0xff) // lines LLLL0000
	copy(data[4:], can.Extra[:])   // 0 to 8 data bytes
	
	serial := data[3] == 0x1f
	if !serial && data[0] >= 0x10 { // not serial and with valid "from"
		resp = true
	}
	if serial && data[4] == 0 { // serial and sub=0
		resp = true
	}
	return
}

// CanTx coverts bytes into a socket Can
//	     0        1        2      3    4      5        6        7        8        9     10 11
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┬──┬──┐
//	│0000LLLL     D        0    comm  xx     xx       xx       xx       xx       xx     xx xx│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┴──┴──┘
func CanTx(bytes []byte) (*Can, error) {
	data, err := milk.NewData(bytes)
	if err != nil {
		return nil, err
	}
	id := uint32(0)
	id |= uint32(data[3] & 0x1f) // comm  ------------------------xxxxx
	id |= uint32(data[2]) << 5   // src   ----------------00000000
	id |= uint32(data[1]) << 13  // dst   --------DDDDDDDD
	id |= uint32(data[0]) << 21  // lines 0000LLLL
	length := uint8(len(data) - 4)
	if length > 8 {
		return nil, errors.New("data length greater than eight")
	}
	var extra base.Extra
	copy(extra[:], data[4:])
	can := Can{
		ID:         id,
		Length:     length,
		Extra:      extra,
		IsExtended: true,
	}
	if err := can.Validate(); err != nil {
		return nil, err
	}
	return &can, nil
}


