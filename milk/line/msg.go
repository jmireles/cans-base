package line

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/to"
)


type Msgs struct {
	resp chan *Msg
	req  chan *Msg
}

func NewMsgs(resp, req chan *Msg) *Msgs {
	return &Msgs{
		resp: resp,
		req:  req,
	}
}

func (m *Msgs) Resp() <-chan *Msg {
	return m.resp
}

func (m *Msgs) Req(mess *Msg) {
	m.req<- mess
}

// A Msg is a message (request or response) of type CAN
// Has a can identifier and a payload of minimum 4 data
type Msg struct {
	base.Net
	milk.Data
	utc base.UTC
}

func NewMsg(net byte, data []byte, utc base.UTC) (*Msg, error) {
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else if data, err := milk.NewData(data); err != nil {
		return nil, err
	} else {
		return &Msg{
			Net:  net,
			Data: data,
			utc:  utc,
		}, nil
	}
}

// NewMsgNet0 returns a message for net=0 (PC message) with given data and now UTC.
func NewMsgNet0(data []byte) (*Msg, error) {
	if data, err := milk.NewData(data); err != nil {
		return nil, err
	} else if now, err := base.UTCNow(); err != nil {
		return nil, err
	} else {
		return &Msg{
			Net:  base.Net(0),
			Data: data,
			utc:  now,
		}, nil
	}

}

// NewMsg20 creates a message given 20 bytes.
// This method is used to build a response message when the bytes 
// from were received TCP clients.
//  0 1                    12 13         19
// ,-,-----------------------,-------------,
// |n|d d d d d d d d d d d d|u u u u u u u|
// '-'-----------------------'-------------'
func NewMsg20(bytes []byte) (*Msg, error) {
	if size := len(bytes); size != 20 {
		return nil, fmt.Errorf("NewMsgRx: bytes size != 20: %d", size)
	
	} else if net, err := base.NewNet(bytes[0]); err != nil {
		return nil, err
	
	} else if data, err := milk.NewData(bytes[1:13]); err != nil {
		return nil, err
	
	} else {
		return &Msg{
			Net:  net, 
			Data: data,
			utc:  base.UTC(bytes[13:]),
		}, nil
	}
}

// NewMsgRx creates a message given 13 bytes.
// This method is used to build a response message when the bytes
// were received from SPI.
//  0 1                    12
// ,-,-----------------------,
// |n|d d d d d d d d d d d d|
// '-'-----------------------'
func NewMsg13(bytes []byte) (*Msg, error) {
	if size := len(bytes); size != 13 {
		return nil, fmt.Errorf("NewMsgProcess bytes size != 13: %d", size)
	
	} else if now, err := base.UTCNow(); err != nil {
		return nil, err

	} else if net, err := base.NewNet(bytes[0]); err != nil {
		return nil, err
	
	} else if data, err := milk.NewData(bytes[1:13]); err != nil {
		return nil, err
	
	} else {
		return &Msg{
			Net:  net, 
			Data: data,
			utc:  now,
		}, nil
	}
}

// Bytes20 returns a 20-byte array using net and payload.
//  0 1                    12 13         19
// ,-,-----------------------,-------------,
// |n|d d d d d d d d d d d d|u u u u u u u|
// '-'-----------------------'-------------'
func (m *Msg) Bytes20() []byte {
	bytes := make([]byte, 20)
	bytes[0] = byte(m.Net)
	copy(bytes[1:13], m.Data) // bug 13 was 12
	copy(bytes[13:],  m.utc)
	return bytes
}

// String returns a string representation of this message (net and payload).
func (m *Msg) String() string {
	return fmt.Sprintf("net=%d data=%02x utc=%02x", m.Net, m.Data, m.utc)
}

func (m *Msg) Hex() []byte {
	return []byte(fmt.Sprintf("%02x%02x%02x", m.Net, m.Data, m.utc))
}

func (m *Msg) UTC() base.UTC {
	return m.utc
}

func (m *Msg) From() (from.From, error) {
	return from.NewFrom(m.Data.Line())
}

func (m *Msg) To() (to.To, error) {
	return to.NewTo(m.Data.Line())
}

// Resp parses message to return common parameters for a request or error
// return serial, to, id box OR error
func (m *Msg) ParseReq() (serial bool, line to.To, id byte, err error) {
	//serial = base.IsGetSerial(m.Data.Command())
	if m.Data.Comm() == byte(milk.Get31_Serial) {
		serial = true
	} else {
		line, err = to.NewTo(m.Line())
		if err != nil {
			return
		}
		id = m.Data[1]
	}
	return
}

// Resp parses message to return common parameters for a response or error
// return serial, from, id box OR error
func (m *Msg) ParseResp() (serial bool, line from.From, id byte, err error) {
	if m.Data[3] == byte(milk.Sol31_Serial) {
		serial = true
		if len(m.Data) < 10 {
			err = fmt.Errorf("serial resp length < 10")
			return
		}
		if m.Data[4] != 0 { // skip no subcommand=0 (unique response accepted)
			err = fmt.Errorf("serial subcommand != 0")
			return
		}
		//  0  1  2   3   4  5  6  7  8  9
		// s1 s2 s3 com sub li s1 s2 s3 id
		// fe dc ba  1f  00 01 fe dc ba 07 -- --
		//          ============================ as payload
		line, err = from.NewFrom(m.Data[5] << 4)
		if err != nil {
			return
		}
		id = m.Data[9]
		return
	} else {
		//  0   1   2   3  4  5  6  7  8  9 10 11
		// li dst src com d0 d1 d2 d3 d4 d5 d6 d7
		// 10  00  01  04 0e 13 0f 13 0e 13 0e 13
		//            =========================== as payload
		line, err = from.NewFrom(m.Data.Line())
		if err != nil {
			return
		}
		id = m.Data[2]
		return
	}
}
