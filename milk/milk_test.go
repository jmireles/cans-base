package milk

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
)

func TestBaseHttpResp(t *testing.T) {
	tt := &base.Test{t}
	myError := `Missing "resource"`
	myJsonError := `{"error":"Missing \"resource\""}`
	goodJsonResp := `{"net":1,"data":[1,2,3,4],"sent":4}`

	// to json
	// good quoted error
	r := NewHttpRespError(myError)
	tt.Equals(myJsonError, r.Json())
	// good message
	r, err := NewHttpResp(1, []byte{1,2,3,4}, 4)
	tt.Ok(err)
	tt.Equals(goodJsonResp, r.Json())
	// wrongs
	_, err = NewHttpResp(5, nil, 0)
	tt.Assert(err != nil, "wrong net 5")
	_, err = NewHttpResp(1, []byte{1,2,3}, 0)
	tt.Assert(err != nil, "wrong data size < 4")
	_, err = NewHttpResp(1, []byte{1,2,3,4,5,6,7,8,9,10,11,12,13}, 0)
	tt.Assert(err != nil, "wrong data size > 12")

	// from json...
	// good error
	r, err = NewHttpRespJson(myJsonError)
	tt.Ok(err)
	tt.Equals(myError, r.Error)
	// good resp
	r, err = NewHttpRespJson(goodJsonResp)
	tt.Ok(err)
	tt.Assert(r.Error == "", "wrong error empty")
	tt.Equals(base.Net(1), r.Net)
	tt.Equals([]byte{1,2,3,4}, r.Bytes())
	tt.Equals(uint(4), r.Sent)
	// wrong jsons
	for _, wrongJson := range []string {
		`{"error":111}`, // error invalid format
		`{}`, // missing net (0)
		`{"net":1}`, // missing data
		`{"net":"string"}`, // net invalid format
		`{"net":1,"data":"string"}`, // data invalid format
		`{"net":1,"data":[1,2,3]}`, // data size < 4
		`{"net":1,"data":[1,2,3,4,5,6,7,8,9,10,11,12,13]}`, // data size > 12
		`{"net":1,"data":[1,2,3,-1]}`, // data byte out of range (-1)
		`{"net":1,"data":[1,2,3,2000]}`, // data byte out of range (2000)
		`{"net":16,"data":[1,2,3,4]}`, // wrong net
		`{"net":1,"data":[1,2,3,4],"sent":-1}`, // negative sent
		`{"net":1,"data":[1,2,3,4],"sent":"string"}`, // sent invalid format
	} {
		_, err := NewHttpRespJson(wrongJson)
		tt.Assert(err != nil, "Missing error for wrong json: %s", wrongJson)
	}
}

func TestClones(t *testing.T) {
	tt := base.Test{t}

	u1, err := base.NewUTC(22, 10, 19, 12, 34, 56, 78)
	tt.Ok(err)
	u2 := u1.Clone()

	u1, err = base.NewUTC(22, 10, 19, 12, 34, 56, 79)
	tt.Ok(err)
	u3 := u1.Clone()

	tt.Equals(u2, base.UTC{22, 10, 19, 12, 34, 56, 78})
	tt.Equals(u3, base.UTC{22, 10, 19, 12, 34, 56, 79})

	d1, err := NewData([]byte{1, 2, 3, 4})
	tt.Ok(err)
	d2 := d1.Clone()

	d1, err = NewData([]byte{5, 6, 7, 8, 9})
	tt.Ok(err)
	d3 := d1.Clone()

	tt.Equals(d2, Data{1,2,3,4})
	tt.Equals(d3, Data{5,6,7,8,9})
}


