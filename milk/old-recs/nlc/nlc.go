package nlc

import (
	"fmt"

	"gitlab.com/jmireles/cans-base/milk/old-recs"
)

// NLC is a directory with folders of the type NLC
//	- r for requests
//	- s for solicited responses
//	- u for unsolicited responses
type NLC struct {
	recs  *recs.Recs
	files map[recs.NLC]*recs.Files
}

func NewNLC(recs *recs.Recs) *NLC {
	nlc := &NLC {
		recs: recs,
		files: make(map[recs.NLC]*recs.Files),
	}
	return nlc
}

func (nlc *NLC) setFiles(base recs.NLC) {

	if files, ok := nlc.files[base]; ok {
		return files
	} else {
		files := recs.NewFiles(base)
		nlc.files[base] = files
		return files
	}
}

func (rsu *RSU) Append(event recs.Event) {
	if event == nil {
		return
	}
	base := recs.NLC(event.Base())
	files := nlc.setFiles(base)
	files.Append(event.Path(), event.Row())
}
