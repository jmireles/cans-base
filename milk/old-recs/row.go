package recs

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
)

type Row []byte



// A RowN constist of at least four time bytes and 0 or mor data bytes.
//	,----,----,----,----,
//	| hh | mm | ss | cc |
//	'----'----'----'----'
//	,----,----,----,----,----,----,----,----,----,----,----,----,
//	| hh | mm | ss | cc | d0 | d1 | d2 | d3 | d4 | d5 | d6 | d7 |...
//	'----'----'----'----'----'----'----'----'----'----'----'----'
// Contains:
//	- byte 1: hours (0-23)
//	- byte 2: minutes (0-59)
//	- byte 3: second (0-59)
//	- byte 4: seconds/100 (0-99)
//	- optional bytes 5-12: payload
type RowN Row

// NewRowN returns a RowN with extra[:payload] bytes
// Panic for payload < 0 or > len(extra)
// rown[0] = utc[3] hrs
// rown[1] = utc[4] min
// rown[2] = utc[5] sec
// rown[3] = utc[6] sec/100
// rown[4] = extra[0] p=0
// ...
// rown[4+p-1] = extra[p-1]
func NewRowN(utc base.UTC, extra []byte, payload int) RowN {
	if payload < 0 {
		panic("payload lower than zero")	
	}
	if payload > len(extra) {
		panic("payload greater than extra")	
	}
	dst := make([]byte, 4 + payload)
	copy(dst, utc[3:7])
	copy(dst[4:], extra[:payload])
	return RowN(dst)
}

// HMSC return row's hour, minute, second and cent of second.
func (r RowN) HMSC() (byte, byte, byte, byte) {
	return r[0], r[1], r[2], r[3]
}

// Extra returns the row's payload.
func (r RowN) Extra() []byte {
	return r[4:]
}

func (r RowN) String() string {
	h,m,s,c := r.HMSC()
	return fmt.Sprintf("%02d:%02d:%02d.%02d %02x", h,m,s,c, r.Extra())
}

// NewRowsN parses a bytes stream and extract consecutive
// rows. Each row correspond to payload within the group.
// Size of returned rows is len(bytes)/(payload+4).
// An error is returned if file is corrupted in size.
func NewRowsN(f *Files, boxDate BoxDate, payload int) ([]RowN, error) {
	if f.m[boxDate] == nil {
		return nil, fmt.Errorf("Empty box/date %s", boxDate)
	} else {
		return newRowsN(f.m[boxDate], payload)
	}
}

func newRowsN(rows []byte, payload int) ([]RowN, error) {
	if len(rows) == 0 {
		return nil, fmt.Errorf("Empty rows")
	}
	n := payload + 4
	if size, mod := len(rows) / n, len(rows) % n; mod != 0 {
		return nil, fmt.Errorf("rows size mismatch")
	} else {
		rowsN := make([]RowN, size)
		p := 0
		for i := 0; i < size; i++ {
			rowsN[i] = RowN(rows[p : p+n])
			p += n
		}
		return rowsN, nil
	}
}






// A RowC consist of 13 bytes and stores partially a CAN message event.
// The 13 bytes are:
//	+----+----+----+----+----+----+----+----+----+----+----+----+----+
//	| hh | mm | ss | cc | CC | d0 | d1 | d2 | d3 | d4 | d5 | d6 | d7 |
//	+----+----+----+----+----+----+----+----+----+----+----+----+----+
//
//  | Field | Name    | Range of values
//  |-------|---------|----------------
//	| hh    | hour    | 0-23
//	| mm    | minutes | 0-59
//	| ss    | seconds | 0-59
//	| cc    | cents   | 0-99
//	| CC    | command | 0-31
//	| dx    | data    | 0-255
//
// Consecutive rows are saved in files that can be accessed within
// folders. Example of files with consecutive binary rows:
//	~/.cans-recs/s/11001/211216
//	~/.cans.recs/u/11001/211216
//
// The format of the file is:
//	base/resp/NLLII/yymmdd
// base is the name of the folder within home directory normally.
// resp is "s" for solicited and "u" for unsolicited.
// N is net, LL is line and II is source id from message.
// yymmdd are year-month-date from timestamp.
type RowC Row

// newRecord creates a Record given a UTC timestamp and a message.
// From the timestamp only hour, minute, second and cent of second are used.
// From message only command and extra are used.
func NewRowC(utc base.UTC, comm byte, extra []byte) RowC {
	r := make([]byte, 13)
	h, m, s, c := utc.Clock()
	r[0] = h
	r[1] = m
	r[2] = s
	r[3] = c
	r[4] = comm
	copy(r[5:], extra)
	return r
}

// HMSC return row's hour, minute, second and cent of second.
func (r RowC) HMSC() (byte, byte, byte, byte) {
	return r[0], r[1], r[2], r[3]
}

// Comm returns the row's command.
func (r RowC) Comm() byte {
	return r[4]
}

// Extra returns the row's payload.
func (r RowC) Extra() []byte {
	return r[5:]
}

func (r RowC) String() string {
	// for debug
	h,m,s,c := r.HMSC()
	return fmt.Sprintf("%02d:%02d:%02d.%02d %02x %02x", h,m,s,c, r.Comm(), r.Extra())
}

func NewRowsC(f *Files, boxDate BoxDate) ([]RowC, error) {
	if f.m[boxDate] == nil {
		return nil, fmt.Errorf("Empty box/date %s", boxDate)
	} else {
		return newRowsC(f.m[boxDate])
	}
}

// newRowsCC parses a bytes stream and extract consecutive
// rows. Each row correspond to 13-bytes within the group.
// Size of returned rows is len(bytes)/13.
// An error is returned if file is corrupted in size.
func newRowsC(rows []byte) ([]RowC, error) {
	if len(rows) == 0 {
		return nil, fmt.Errorf("Empty rows")
	}
	const n = 13
	if size, mod := len(rows) / n, len(rows) % n; mod != 0 {
		return nil, fmt.Errorf("rows size mismatch")
	} else {
		rowsC := make([]RowC, size)
		p := 0
		for i := 0; i < size; i++ {
			rowsC[i] = RowC(rows[p : p + n])
			p += n
		}
		return rowsC, nil
	}
}



