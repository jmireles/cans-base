# recs

Package recs is used to record CAN requests and responses in binary files
in current filesystem.

## Format RSU

Format RSU stores requests, solicited responses and unsolicited responses in three folders

```
                            ------------ ------- ------------------------------------
 unsol                      base folder  string  req=1 or sol=2 or unsol=3
  ├─ nLLII                  box folder   hex     net/from/box-id
  │   ├─ yymmdd             date file    decimal year/month/date
  │   │   ├─ hmscC01234567  13-bytes-row hex     hr/min/sec/cent/command/8 data bytes
  │   │   ├─ hmscC01234567  13-bytes-row ...
  │   │   ├─ ...
 ...
```

## Formats nLLT


Formst nLLCC store records preventing saving unnecesary extra data bytes

### Format nLLCC/ii/ymdh
```
                        ----------------  ------- ---------------------
 nLLCC                  base folder       hex     net/line/command
  ├─ ii                 box folder        hex     box-id
      ├─ yymmddhh       date file         decimal year/month/date/hour
          ├─ msc[...]   (3-11)-bytes-row  hex     min/sec/cent/0-8 data
          ├─ msc[...]   (3-11)-bytes-row  ...
```

### Format nLLCC/ii/ymd
```
                        ---------------- ------- ------------------------
 nLLCC                  base folder      hex     net/line/command
  ├─ ii                 box folder       hex     box-id
      ├─ yymmdd         date file        decimal year/month/date
          ├─ hmsc[...]  (4-12)-bytes-row hex     hr/min/sec/cent/0-8 data
          ├─ hmsc[...]  (4-12)-bytes-row ...
```
### Examples

T4's Get06_Profiles 0 bytes request
```
 10706               
  ├─ 01                              hex box 1
      ├─ 23040506                    dec 2023/apr/5 6AM
          ├─ 070809                  hex 7min,8sec,9cents first octet
```
T4's Sol06_Profiles 8 data bytes responses
```
 17006                               hex net=1 from=SL command=6
  ├─ 01                              hex box 1
      ├─ 23040506                    dec 2023/apr/5 6AM
          ├─ 070809000278fafac8c8c8  hex 7min,8sec,9cents first octet
          ├─ 07080A010201c800643223  hex 7min,8sec,10cents second octet
```

PM's Unsol17_NoPhaseRear 0 data bytes response
```
 11011                               hex fromPC, command 17
  ├─ 01                              hex box 1
      ├─ 23040506                    dec 2023/apr/5 6AM
          ├─ 070809                  hex 7min,8sec,9cents
```

20-bytes event
```
      0 1                    12 13         19
     ,-,-----------------------,-------------,
     |n|L D S C 0 1 2 3 4 5 6 7|y m d h m s c|
     '-'-----------------------'-------------'

from: NNL000ssCCd0d1d2d3d4d5d6d7yymmddhhmmsscc
  to: NN0Ldd00CCd0d1d2d3d4d5d6d7yymmddhhmmsscc
```

```
nLLT = net/line/type

net = 1,2,3,4
line = 01,06,07,08,10,60,70,80
type = 0 stall-status
     = 1 stall-value
     = 2 box-config
     = 3 zone-value

1100 = net=1, line=from-PM, stall-state


```

| Net | Line    | Type         | ID    | Date     | Row              | size
|-----|---------|--------------|-------|----------|------------------|----
| 1-4 | to-XX   | stall-value  | 0-255 | `yymmdd` | `hmscC`          | 5
| 1-4 | to-I4   | zone-value   | 0-15  | `yymmdd` | `hmscC`          | 5
| 1-4 | to-XX   | box-config   | 0-255 | `yymmdd` | `hmscC`          | 5
| 1-4 | from-XX | box-config   | 1-255 | `yymmdd` | `hmscC12345678`  | 13
| 1-4 | from-PM | stall-status | 1-255 | `yymmdd` | `hmscC12`        | 7
| 1-4 | from-PM | stall-value  | 1-255 | `yymmdd` | `hmscC12345678`  | 13
| 1-4 | from-T4 | stall-status | 1-255 | `yymmdd` | `hmscC12`        | 7
| 1-4 | from-T4 | stall-value  | 1-255 | `yymmdd` | `hmscC12345678`  | 13
| 1-4 | from-RS | stall-value  | 1-255 | `yymmdd` | `hmscC12345678`  | 13
| 1-4 | from-I4 | zone-status  | 1-15  | `yymmdd` | `hmscC12`        | 7

```
 NLLT                                net-line-type folder
  ├─ II                              id folder
      ├─ yymmdd                      date file
          ├─ hmscC(0-8)              row timestamp-command-data
```