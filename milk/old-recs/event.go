package recs

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
)

type Event interface {

	Comm() Comm

	Box()  Box

	Date() Date

	Row()  Row
}

// An EventRSU is a resp/req from/to CAN. Includes unsol type, nllii and date folders and
// 13-byte payload with a hms timestamp. A EventRSU is stored in a file system.
type EventRSU struct {
	comm RSU  // req, sol, unsol
	box  NLI  // NLLII hex five chars
	date YMD  // yymmdd decimal six chars
	row  RowC // 13 bytes: [h][m][s][c][comm][payload0-7]
}

func NewEventRSUReq(msg *line.Msg) (*EventRSU, error) {
	if msg == nil {
		// save "line" computer message
		// use this event to record this recorder started up.
		utc, _ := base.UTCNow()
		return &EventRSU{
			comm: Req,
			box:  NewNLI(0,0,0),
			date: NewYMDUTC(utc),
			row:  NewRowC(utc, 0, []byte{}),
		}, nil
	}
	if serial, line, box, err := msg.ParseReq(); err != nil {
		return nil, err

	} else {
		utc := msg.UTC()
		event := func() (*EventRSU, error) {
			return &EventRSU{
				comm: Req,
				box:  NewNLI(byte(msg.Net), byte(line), box),
				date: NewYMDUTC(utc),
				row:  NewRowC(utc, msg.Comm(), msg.Extra()),
			}, nil
		}
		if serial {
			return event()
		} else if get, set := line.Req(msg.Comm()); get != nil || set != nil {
			return event()
		}
	}
	return nil, fmt.Errorf("Invalid request command")
}

func NewEventRSUResp(resp from.Resp) (*EventRSU, error) {
	if resp == nil {
		return nil, fmt.Errorf("Invalid response")
	} else if command, _, unsol, err := resp.Comm(); err != nil {
		return nil, err
	} else if from, err := resp.From(); err != nil {
		return nil, err
	} else {
		comm := Sol
		if unsol != nil {
			comm = Unsol
		}
		utc := resp.UTC()
		return &EventRSU{
			comm: comm,
			box:  NewNLI(byte(resp.NetId()), byte(from), resp.BoxId()),
			date: NewYMDUTC(utc),
			row:  NewRowC(utc, command, resp.Extra()),
		}, nil
	}
}

// implements Event
func (e *EventRSU) Comm() Comm {
	return Comm(e.comm)
}

// implements Event
func (e *EventRSU) Box() Box {
	return Box(e.box)
}

// implements Event
func (e *EventRSU) Date() Date {
	return Date(e.date)
}

// implements Event
func (e *EventRSU) Row() Row {
	return Row(e.row)
}
