package recs

/*
import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/to"
)

var (
	TOs = []to.To{ to.P1, to.RS, to.T4, to.I4 }
	Gets = []milk.Get {
		milk.Get00_Version,
		milk.Get01_Serial,
		milk.Get02_Status,
	}
	Sets = []milk.Set {

	}
)


type Comm struct {
	id     string
	Gets   []milk.Get
	Sets   []milk.Set
	Sols   []milk.Sol
	Unsols []milk.Unsol
}


func TestDirs(t *testing.T) {

	tt := base.Test{ T:t }

	nli, _ := NewNLIMess(2,0x34,0x56)
	for _, d := range []struct {
		d Dir
		i int
		s string
	} {
		{ Sol,                         2,       "sol"   },
		{ NewNLC(base.Net(1), 12, 13), 0x10c0d, "10c0d" },
		{ NewNLIComputer(),            0,       "00000" },
		{ nli,                         0x23456, "23456" },
		{ NewI(0x78),                  0x78,    "78"    },
	} {
		tt.Equals(d.i, d.d.Int())
		tt.Equals(d.s, d.d.String())
	}
}

func TestBase(t *testing.T) {

	tt := base.Test{ T:t }

	lines := map[to.To]*Comm{
		to.P1: &Comm{
			id: "p1",
			Gets: []milk.Get {
				p1.Get03_Config,
				p1.Get05_Values,
				p1.Get06_Averages,
				p1.Get07_Calibr,
				p1.Get09_Options,
				p1.Get11_Profiles,
				p1.Get13_Display,
				p1.Get15_Config,
				p1.Get17_Alarms,
				p1.Get19_Stalls,
			},
			Sets: []milk.Set {
				p1.Set04_Config,
				p1.Set08_Calibr,
				p1.Set10_Options,
				p1.Set12_Profiles,
				p1.Set14_Display,
				p1.Set16_Config,
				p1.Set18_Alarms,
				p1.Set20_Stalls,
			},
		},
	}
	y, m, d := 2014, time.Month(6), 5
	date := time.Date(y, m, d, 12, 34, 56, 0, time.UTC)
	incrUtc := func() base.UTC {
		date = date.Add(time.Duration(10 * time.Millisecond))
		utc, _ := base.NewUTCTime(date)
		return utc
	}

	net := byte(3)
	payload := []byte{ 0,1,2,3,4,5,6,7 }
	exp := []string{
		"30100/05(0)", "30101/05(0)", "30102/05(0)", // p1 ver/ser/status
		"30600/05(0)", "30601/05(0)", "30602/05(0)", // rs ver/ser/status
		"30700/05(0)", "30701/05(0)", "30702/05(0)", // t4 ver/ser/status
		"30800/05(0)", "30801/05(0)", "30802/05(0)", // i4 ver/ser/status
		// pm gets
		"30103/05(0)", 
		"30105/05(1)", // get-values
		"30106/05(0)", 
		"30107/05(0)", 
		"30109/05(0)", 
		"3010b/05(1)", // get-profile
		"3010d/05(0)", 
		"3010f/05(0)", 
		"30111/05(0)", 
		"30113/05(0)", 
		// pm sets
		"30104/05(8)", 
		"30108/05(8)", 
		"3010a/05(8)", 
		"3010c/05(8)", 
		"3010e/05(8)", 
		"30110/05(8)", 
		"30112/05(8)", 
		"30114/05(8)", 
	}
	e := 0
	add := func(canBytes []byte) {
		mess, err := line.NewMess(net, canBytes, incrUtc())
		tt.Ok(err)
		nlc, ii, payload, err := NewNLCReq(mess)
		tt.Ok(err)
		tt.Equals(exp[e], fmt.Sprintf("%s/%s(%d)", nlc, ii, payload))
		e++
	}
	for _, box := range []byte { 5 } {
		// 1) append records to dirs
		for _, to := range TOs {
			for _, comm := range Gets {
				add(to.ReqGetBytes(box, comm, payload))
			}
			for _, comm := range Sets {
				add(to.ReqSetBytes(box, comm, payload))
			}
		}
		for to, c := range lines {
			for _, comm := range c.Gets {
				add(to.ReqGetBytes(box, comm, payload))
			}
			for _, comm := range c.Sets {
				add(to.ReqSetBytes(box, comm, payload))
			}
		}
	}
}


func TestRow(t *testing.T) {

	tt := base.Test{ T:t }

	utc1, _ := base.NewUTC(9,10,11, 12,13,14,15)  // 2009/Oct/11 12:13:14.15
	utc2, _ := base.NewUTC(10,11,12, 13,14,15,16) // 2010/Nov/12 13:14:15.16
	utc3, _ := base.NewUTC(11,12,13, 14,15,16,17) // 2011/Dec/13 14:15:16.17
	for _, d := range []struct {
		utc     base.UTC
		extra   []byte
		payload int
		row     []byte
	} {
		{ utc1, []byte{18,19,20,21},    3, []byte{12,13,14,15,18,19,20 }},
		{ utc2, []byte{22,23,24,25,26}, 4, []byte{13,14,15,16,22,23,24,25 }},
		{ utc3, []byte{27,28,29,30,31}, 0, []byte{14,15,16,17}},
	} {
		rowN := NewRowN(d.utc, d.extra, d.payload)
		tt.Equals(d.row, []byte(rowN))
	}
}
*/