package recs

import (
	"path/filepath"
	"os"

	"gitlab.com/jmireles/cans-base"
)

type Date string


// A YMD is a string to be used as a dir name to identify a year-month-date event.
// Consist of 6 chas to represent a decimal number.
// Format is yymmdd, where yy is year (0-99), mm is month (1-12) and dd is day (1-31)
type YMD Date

// newYMDFile returns a YMD from file information (a dir name).
// returns "" and false when the file is not a dir and name size is not 6.
func NewYMDFromFile(info os.FileInfo) (YMD, bool) {
	if info.IsDir() { // prevent dates further directories
		return "", false
	}
	if date := info.Name(); len(date) != 6 {
		return "", false
	} else {
		// accept only folders with format: yymmdd
		// TODO check is decimal integer
		return YMD(date), true
	}
}

// NewYMDTime returns a YMD from a given time
func NewYMDUTC(utc base.UTC) YMD {
	return YMD(utc.YMD())
}


// YMDs returns and array of strings validated as YMD.
// The list resprent all the date folders under the base/nli filepath
func YMDs(base string, nli string) ([]YMD, error) {
	ymds := make([]YMD, 0)
	b := filepath.Join(base, nli)
	err := filepath.Walk(b, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if b == path {
			return nil // avoid base included by walk
		}
		if ymd, ok := NewYMDFromFile(info); ok {
			ymds = append(ymds, ymd)
		}
		return nil
	})
	return ymds, err
}

type YMDH Date


