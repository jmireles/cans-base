package recs

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
)



// Files is a map of filepaths where to save a byte array
type Files struct {

	sync.Mutex

	base string
	mode os.FileMode
	m    map[BoxDate][]byte
}

func NewFiles(base string, mode os.FileMode) *Files {
	return &Files{
		base: base,
		mode: mode,
		m:    make(map[BoxDate][]byte),
	}
}

func (f *Files) Clone() *Files {
	f.Lock()
	defer f.Unlock()

	g := NewFiles(f.base, f.mode)
	for k, v := range f.m {
		g.m[k] = v[:]
	}
	return g
}

func (f *Files) clear() {
	for elem, _ := range f.m {
		delete(f.m, elem)
	}
}

func (f *Files) append(e Event) {
	f.Lock()
	defer f.Unlock()

	boxDate := NewBoxDate(e.Box(), e.Date())
	if f.m[boxDate] == nil {
		f.m[boxDate] = make([]byte, 0)
	}
	f.m[boxDate] = append(f.m[boxDate], []byte(e.Row())...)
}

func (f *Files) Save() (int, error) {
	f.Lock()
	defer f.Unlock()
	defer f.clear()
	n := 0
	for boxDate, bytes := range f.m {
		if w, err := f.save(boxDate, bytes); err != nil {
			return n, err
		} else {
			n += w
		}
	}
	return n, nil
}

func (f *Files) save(boxDate BoxDate, bytes []byte) (int, error) {

	filename := filepath.Join(f.base, string(boxDate))
	dir := filepath.Dir(filename)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		// make parent which does not exists
		if err := os.MkdirAll(dir, f.mode); err != nil {
			return 0, err
		}
	}
	const o = os.O_APPEND | os.O_WRONLY | os.O_CREATE
	if file, err := os.OpenFile(filename, o, f.mode); err != nil {
		// file cannot be openned
		return 0, err
	} else if w, err := file.Write(bytes); err != nil {
		// file cannot be written
		file.Close()
		return w, err
	} else if err := file.Close(); err != nil {
		// file cannot be closed
		return w, err
	} else {
		return w, nil
	}
}



// fileYMD returns an array of bytes of the binary file
// The file is exactly the filepath join of base, nli and date strings.
func FileRead(base, nli, date string) ([]byte, error) {
	path := filepath.Join(base, nli, date)
	return ioutil.ReadFile(path)
}





