package rsu

import (
	"context"
	"encoding/hex"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
	"gitlab.com/jmireles/cans-base/milk/old-recs"
	"gitlab.com/jmireles/cans-base/milk/to"
)

const (
	SOL     = false
	UNSOL   = true
	VALID   = true
	INVALID = false
)

// go clean -testcache; go test ./milk/recs/. -run TestAppend -v
func TestAppend(t *testing.T) {
	tt := base.Test{t}
	y, m, d := byte(14), byte(6), byte(5) // 2014 / Jun / 5
	utcs := []base.UTC {
		[]byte{ y, m, d, 12, 34, 56, 20 }, // 200ms
		[]byte{ y, m, d, 12, 34, 57, 40 }, // 400ms
		[]byte{ y, m, d, 12, 34, 58, 60 }, // 600ms
		[]byte{ y, m, d, 12, 34, 59, 80 }, // 800ms
	}
	rsu := NewRSU("", "", 0)
	net := byte(1) // valid nets: 1,2,3...15
	serial := []byte{1, 2, 3}
	toLine := to.P1 // yes, resp serial has line "to" not "from"!
	box := byte(7)
	canBytes := from.SolSimSerialGet(serial, toLine, box)
	for _, utc := range utcs {
		resp, err := from.NewResp(net, canBytes, utc)
		tt.Ok(err)
		event, err := recs.NewEventRSUResp(resp)
		tt.Ok(err)
		rsu.Append(event)
	}
	// NLLII/yymmdd key to be used as dirs
	boxDate := recs.NFromIYMD(net, from.P1, box, utcs[0])
	// command expected
	serialComm := byte(milk.Sol31_Serial)
	// payload expected 0=subcommand, 1,2,3=serial, 7=box
	extra := []byte{ 0,1,1,2,3,7,0,0 }
	sol := rsu.Get(recs.Comm(recs.Sol))
	rows, err := recs.NewRowsC(sol, boxDate)
	tt.Ok(err)
	tt.Equals(4, len(rows))

	h, m, s, c := rows[0].HMSC()
	tt.Assert(!(h != 12 || m != 34 || s != 56 || c != 20), "Not 12:34:56.30")
	h, m, s, c = rows[1].HMSC()
	tt.Assert(!(h != 12 || m != 34 || s != 57 || c != 40), "Not 12.34.57.40")
	tt.Equals(serialComm, rows[2].Comm())
	tt.Equals(extra, rows[3].Extra())
	t.Logf("NLLII/yymmdd: %s%s%s", base.Green, boxDate, base.Reset)
	for pos, record := range rows {
		t.Logf("Rec#%d %s%s%s", pos, base.Green, record, base.Reset)
	}

	ms := 0
	getUTC := func() base.UTC {
		ms += int(10 * time.Millisecond) // 1/100 of second
		return []byte { y, m, d, 12, 34, 56, byte(ms) }
	}
	rsu = NewRSU("", "", 0)
	request, err := line.NewMess(1, []byte{1, 1, 0, 3}, getUTC())
	tt.Ok(err)
	event, err := recs.NewEventRSUReq(request)
	tt.Ok(err)
	t.Logf("event %s%v%s", base.Green, event, base.Reset)
	rsu.Append(event)
	pmConfigs := [][]byte{
		[]byte{ 16,0,1,3,0,  1,60, 40, 65,60, 40, 20 },
		[]byte{ 16,0,1,3,1, 20, 3,232,  0,19,  1,132 },
		[]byte{ 16,0,1,3,2,  1,16,  3,184, 3,184,  0 },
		[]byte{ 16,0,1,3,3,172,40,  3,184, 3,184,  0 },
	}
	resps := make([]from.Resp, 0)
	for _, pmConfig := range pmConfigs {
		resp, err := from.NewResp(1, pmConfig, getUTC())
		tt.Ok(err)
		resps = append(resps, resp)
	}
	for _, resp := range resps {
		event, err := recs.NewEventRSUResp(resp)
		tt.Ok(err)
		t.Logf("event %s%v%s", base.Green, event, base.Reset)
		rsu.Append(event)
	}

	rsu = NewRSU("", "", 0)
	for _, s := range []struct {
		net   byte
		hex   string
		valid bool
		base  recs.RSU
		box   recs.NLI
		comm  byte
	} {
		// comm=04 line=10 src-id=01 :
		{ 1, "100001040e130f130e130e13", VALID,   recs.Sol,   "11001", 4  }, // PM-1 values resp
		// comm=1f subcomm=00, (serial fedcba) line=01 id=07:
		{ 1, "fedcba1f0001fedcba07",     VALID,   recs.Sol,   "11007", 31 }, // PM-7 serial resp
		// comm=05 line=80 src-id=01
		{ 4, "80000105031b000000000000", VALID,   recs.Unsol, "48001", 5  }, // I4-1 unsol
		// comm=05 line=80 src-id=02
		{ 4, "800002050207000000000000", VALID,   recs.Unsol, "48002", 5  }, // I4-2 unsol
		// invalid
		{ 1, "90",                       INVALID, recs.Sol,        "", 0  },
		{ 1, "xx",                       INVALID, recs.Sol,        "", 0  },
	} {
		canBytes, _ := hex.DecodeString(s.hex)
		now, _ := base.UTCNow()
		resp0, err := from.NewResp(s.net, canBytes, now)
		tt.Assert(!(err != nil && s.valid), "%v", err)
		resp, err := recs.NewEventRSUResp(resp0)
		if s.valid {
			tt.Assert(resp != nil, "should be valid")
			tt.Ok(err)
			row := []byte(resp.Row())
			tt.Equals(recs.Comm(s.base), resp.Comm())
			tt.Equals(recs.Box(s.box), resp.Box())
			tt.Equals(s.comm, row[4])
			t.Logf("%s -> %s %s%s %02x%s", s.hex, base.Green, s.box, base.Blue, row, base.Reset)
			rsu.Append(resp)
		} else {
			tt.Assert(resp == nil, "should be invalid")
		}
	}
}

func TestComms(t *testing.T) {
	tt := base.Test{t}

	rsu := NewRSU("", "", 0)
	net := byte(4)
	box := byte(7)
	payload := []byte{1, 2, 3, 4, 5, 6, 7, 8} // whatever/irrelavant

	type comm struct {
		id     string
		gets   []milk.Get
		sets   []milk.Set
		sols   []milk.Sol
		unsols []milk.Unsol
	}

	// from
	froms := []from.From{ from.P1, from.RS, from.T4, from.I4 }
	sols := []milk.Sol { // 5
		milk.Sol00_Version,
		milk.Sol02_Status,
		milk.Sol(3),
		milk.Sol30_IapReady,
		milk.Sol31_Serial,
	}
	unsols := []milk.Unsol { // 1
		milk.Unsol01_Restart,
	}

	p1Sols, p1Unsols := p1.Resps()
	rsSols, rsUnsols := rs.Resps()
	t4Sols, t4Unsols := t4.Resps()
	i4Sols, i4Unsols := i4.Resps()
	comms := map[from.From]*comm{
		from.P1: &comm{ id: "p1", sols: p1Sols, unsols: p1Unsols },
		from.RS: &comm{ id: "rs", sols: rsSols, unsols: rsUnsols },
		from.T4: &comm{ id: "t4", sols: t4Sols,	unsols: t4Unsols },
		from.I4: &comm{	id: "i4", sols: i4Sols, unsols: i4Unsols },
	}

	y, m, d := 2014, time.Month(6), 5
	date := time.Date(y, m, d, 12, 34, 56, 0, time.UTC)
	utc, _ := base.NewUTCTime(date)

	incrUtc := func() base.UTC {
		date = date.Add(time.Duration(10 * time.Millisecond))
		utc, _ := base.NewUTCTime(date)
		return utc
	}


	// 1) append records to dirs
	append := func(canBytes []byte) {
		time := incrUtc()
		resp, err := from.NewResp(net, canBytes, time)
		tt.Ok(err)
		event, err := recs.NewEventRSUResp(resp)
		tt.Ok(err)
		rsu.Append(event)
		//t.Logf("% 02x %s", canBytes, time)
	}
	for _, fromLine := range froms {
		for _, comm := range sols {
			if comm == milk.Sol31_Serial {
				to := byte(fromLine >> 4)
				append([]byte{1, 2, 3, byte(comm), 0, to, 1, 2, 3, box, 0, 0})
			} else {
				append(fromLine.RespSolBytes(box, comm, payload))
			}
		}
		for _, comm := range unsols {
			append(fromLine.RespUnsolBytes(box, comm, payload))
		}
	}
	for from, v := range comms {
		for _, comm := range v.sols {
			append(from.RespSolBytes(box, comm, payload))
		}
		for _, comm := range v.unsols {
			append(from.RespUnsolBytes(box, comm, payload))
		}
	}
	// 2) check dirs
	for _, from := range froms {
		boxDate := recs.NFromIYMD(net, from, box, utc)
		ok := func(g string, rows []recs.RowC, line string) string {
			return fmt.Sprintf("%s %s: %s/%v recs=%d%s", g, line, base.Green, boxDate, len(rows), base.Reset)
		}
		// sols
		sol := rsu.Get(recs.Comm(recs.Sol))
		rows, err := recs.NewRowsC(sol, boxDate)
		tt.Ok(err)
		if comm := comms[from]; comm != nil {
			tt.Equals(len(sols) + len(comm.sols), len(rows))
			t.Logf(ok("  sols", rows, comm.id))
		}
		// unsols
		unsol := rsu.Get(recs.Comm(recs.Unsol))
		rows, err = recs.NewRowsC(unsol, boxDate)
		tt.Ok(err)
		if comm := comms[from]; comm != nil {
			tt.Equals(len(unsols) + len(comm.unsols), len(rows))
			t.Logf(ok("unsols", rows, comm.id))
		}
	}

	// to
	tos := []to.To{ to.P1, to.RS, to.T4, to.I4 }
	gets := []milk.Get {
		milk.Get00_Version,
		milk.Get01_Serial,
		milk.Get02_Status,
	}
	sets := []milk.Set {
	}
	commsTo := map[to.To]*comm{
		to.P1: &comm{
			id: "p1",
			gets: []milk.Get {
				p1.Get03_Config,
				p1.Get05_Values,
				p1.Get06_Averages,
				p1.Get07_Calibr,
				p1.Get09_Options,
				p1.Get11_Profiles,
				p1.Get13_Display,
				p1.Get15_Config,
				p1.Get17_Alarms,
				p1.Get19_Stalls,
			},
			sets: []milk.Set {
				p1.Set04_Config,
				p1.Set08_Calibr,
				p1.Set10_Options,
				p1.Set12_Profiles,
				p1.Set14_Display,
				p1.Set16_Config,
				p1.Set18_Alarms,
				p1.Set20_Stalls,
			},
		},
	}
	y, m, d = 2014, time.Month(6), 5
	date = time.Date(y, m, d, 12, 34, 56, 0, time.UTC)
	utc, _ = base.NewUTCTime(date)

	incrUtc = func() base.UTC {
		date = date.Add(time.Duration(10 * time.Millisecond))
		utc, _ := base.NewUTCTime(date)
		return utc
	}

	// 1) append records to dirs
	append = func(canBytes []byte) {
		mess, err := line.NewMess(net, canBytes, incrUtc())
		tt.Ok(err)
		event, err := recs.NewEventRSUReq(mess)
		tt.Ok(err)
		rsu.Append(event)
	}
	for _, toLine := range tos {
		for _, comm := range gets {
			append(toLine.ReqGetBytes(box, comm, payload))
		}
		for _, comm := range sets {
			append(toLine.ReqSetBytes(box, comm, payload))
		}
	}
	for toLine, v := range commsTo {
		for _, comm := range v.gets {
			append(toLine.ReqGetBytes(box, comm, payload))
		}
		for _, comm := range v.sets {
			append(toLine.ReqSetBytes(box, comm, payload))
		}
	}
	// 2) check folders
	for _, toLine := range tos {
		boxDate := recs.NToIYMD(net, toLine, box, utc)
		ok := func(g string, rows []recs.RowC, line string) string {
			return fmt.Sprintf("%s %s: %s/%v recs=%d%s", g, line, base.Green, boxDate, len(rows), base.Reset)
		}
		// rows
		req := rsu.Get(recs.Comm(recs.Req))
		rows, err := recs.NewRowsC(req, boxDate)
		tt.Ok(err)
		if comm := commsTo[toLine]; comm != nil {
			tt.Equals(len(gets) + len(sets) + len(comm.gets) + len(comm.sets), len(rows))
			t.Logf(ok("  rows", rows, comm.id))
		}
	}
}

// go clean -testcache; go test ./milk/recs/rsu/. -run TestStartup -v
func TestStartup(t *testing.T) {
	tt := base.Test{t}
	temp := t.TempDir()
	t.Logf("%s%s%s", base.Yellow, temp, base.Reset)
	recorder, err := NewRecorderTest(temp)
	tt.Ok(err)

	t.Run("1", func(t *testing.T) {
		now, _ := base.UTCNow()
		mess, _ := line.NewMess(1, []byte{ 1,2,3,4 }, now)
		startupReq, err := recs.NewEventRSUReq(mess)
		tt.Ok(err)
		recorder.rsu.Append(startupReq) // Manual append
		m, err := recorder.rsu.Save()
		tt.Ok(err)
		t.Logf("saved m=%v", m)
	})

	t.Run("2", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		go func() {
			recorder.Freq(1 * time.Second)
			err := recorder.Serve(ctx, func(f string, args ...any) {
				t.Logf("debug: " + f, args...)
			})
			tt.Ok(err)
			var sb strings.Builder
			recorder.Json(&sb)
			t.Logf("%s%s%s", base.Green, sb.String(), base.Reset)
		}()
		time.Sleep(2 * time.Second)

	})

	filepath.Walk(temp, func(name string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			t.Logf("%s%v %v%s", base.Blue, name, info.Size(), base.Reset)
		}
		return nil
	})
}

func TestRecorder(t *testing.T) {
	tt := base.Test{t}
	temp := t.TempDir()
	t.Logf("%s%s%s", base.Yellow, temp, base.Reset)
	recorder, err := NewRecorderTest(temp)
	tt.Ok(err)
	
	// print every 1.5 sec the statistics to test Json concurrency
	ticker := time.NewTicker(1500 * time.Millisecond)
	defer ticker.Stop()
	go func() {
		for {
			select {
			case <-ticker.C:
				var sb strings.Builder
				recorder.Json(&sb)
				t.Logf("%sJSON %s%s", base.Green, sb.String(), base.Reset)
			}
		}
	}()

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		recorder.Freq(2 * time.Second)
		err := recorder.Serve(ctx, func(f string, args ...any) {
			t.Logf("debug: " + f, args...)
		})
		tt.Ok(err)
	}()

	t.Logf("Wait 1 sec...")
	time.Sleep(1 * time.Second)
	
	can, toPM, fromPM, box := byte(1), to.P1, from.P1, byte(7)
	getBytes := toPM.ReqGetBytes(box, p1.Get05_Values, []byte{1})
	readBytes := fromPM.RespSolBytes(box, p1.Sol04_ValCurves, []byte{1, 2, 3, 4, 5, 6, 7, 8})

	for i := 0; i < 10; i++ {
		now, _ := base.UTCNow()
		mess, err := line.NewMess(can, getBytes, now)
		tt.Ok(err)
		recorder.Request(mess)
		t.Logf("req #%d %v", i, mess)
		for j := 0; j < 30; j++ { // 30 * 32ms is about 1 second
			time.Sleep(32 * time.Millisecond)
			now, _ := base.UTCNow()
			resp, err := from.NewResp(can, readBytes, now)
			tt.Ok(err)
			recorder.Response(resp)
		}
	}
	t.Logf("Wait 1 sec...")
	time.Sleep(1 * time.Second)
	
	t.Logf("Cancel")
	cancel() // suspend saving
	
	time.Sleep(1 * time.Second)
	// access counts without mutex since everything is stopped
	e := recorder.counts.E
	tt.Assert(e == 0, "Unexpected number of errors got:%d", e)
	// (1 startup + 10 reqs) times 13 bytes/record
	tt.Equals(143, recorder.counts.R)
	// 10 reqs times 30 resps times 13 bytes/record
	tt.Equals(3900, recorder.counts.S)
	tt.Equals(0, recorder.counts.U)

	filepath.Walk(temp, func(name string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			t.Logf("%s%v %v%s", base.Blue, name, info.Size(), base.Reset)
		}
		return nil
	})

}


/*
func TestAll(t *testing.T) {
	tt := base.Test{ T:t }
	temp := t.TempDir()
	t.Logf("%s%s%s", base.Yellow, temp, base.Reset)
	recorder, err := NewRecorderTest(temp)
	tt.Ok(err)
	rsu := recorder.rsu
	// 1) save copies of what we're going to save
	uF := rsu.u.Clone()
	sF := rsu.s.Clone()
	// check saved
	t.Logf("Data to be saved unsol: %v", uF)
	t.Logf("Data to be saved sol: %v", sF)

	// 2) store dirs before save (could even does'nt exists yet).
	before := func(group Group, box recs.Files) recs.Files {
		files := recs.NewFiles(box.Base)
		for k, _ := range box {
			if path := strings.Split(string(k), "/"); len(path) != 2 { // get box/ymd
				t.Fatalf("%sbefore save path error %v%s", base.Red, path, base.Reset)
			} else if ymd, err := rsu.ReadDate(group, path[0], path[1]); err != nil {
				// missing file is ok before saving, don't throw Fatal
			} else {
				files[k] = ymd
			}
		}
		return files
	}
	sA := before(GroupSol, sF)
	uA := before(GroupUnsol, uF)
	t.Logf("Files before save sol: %s%v%s", base.Green, sA, base.Reset)
	t.Logf("Files before save unsol: %s%v%s", base.Green, uA, base.Reset)

	// 3) Do real saved	
	if r, s, u, err := rsu.Save(); err != nil {
		t.Logf("Save error %s%v%s", base.Yellow, err, base.Reset)
		t.Logf("To go beyond RUN test with:")
		t.Logf("%ssudo GOFLAGS=\"-count=1\" $(which go) test ./recs -run TestFolders -v%s", base.Yellow, base.Reset)
		return
	} else {
		t.Logf("%sSaved bytes: r=%d s=%d u=%d %s", base.Green, r, s, u, base.Reset)
	}

	// 4) store dirs after save (must exist everything)
	after := func(group Group, box recs.Files) recs.Files {
		files := recs.NewFiles(box.Base)
		for k, _ := range box {
			if path := strings.Split(string(k), "/"); len(path) != 2 { // get box/ymd
				t.Fatalf("%safter save path error %v%s", base.Red, path, base.Reset)
			} else if ymd, err := rsu.ReadDate(group, path[0], path[1]); err != nil {
				t.Fatalf("%safter save error %v%s", base.Red, err, base.Reset)
			} else {
				files[k] = ymd
			}
		}
		return files
	}
	sB := after(GroupSol, sF)
	uB := after(GroupUnsol, uF)
	t.Logf("Files after save sol: %s%v%s", base.Green, sB, base.Reset)
	t.Logf("Files after save unsol: %s%v%s", base.Green, uB, base.Reset)

	// 5) compare before/after
	for k, before := range sA {
		after, ok := sB[k]
		if !ok {
			t.Logf("%ssol k=%s before=%d unexpected missing after%s", base.Red, k, len(before), base.Reset)
		}
		if len(after)-len(before) != 13 {
			t.Logf("%ssol k=%s before=%d after=%d unexpected diff != 13%s", base.Red, k, len(before), len(after), base.Reset)
		}
		diff := after[len(before):]
		t.Logf("sol k=%s before=%d after=%d %s%02x%s", k, len(before), len(after), base.Blue, diff, base.Reset)
	}
}
*/






/*
// sudo $(which go) test ./recs -run=BenchmarkSave -bench=.
// $ sudo tree -h ~/.cans-recs-benchmark
func BenchmarkSave(b *testing.B) {
	// To inspect
	// $ sudo su
	// $ cd ~/.cans-recs-benchmark/s/11001
	// $ xxd -c 13 {yymmdd} | more
	/ *
		Ubuntu16:
		========
		goos: linux
		goarch: 386
		cpu: Intel(R) Core(TM) i5-4460  CPU @ 3.20GHz
		BenchmarkSave-4   	   71542	     16549 ns/op

		SOM:
		===
		goos: linux
		goarch: arm
		BenchmarkSave-4   	   16504	     72772 ns/op
	* /
	path := "/.cans-recs-benchmark"
	recs, err := NewRecorder(true, path)
	if err != nil {
		b.Fatalf("unexpected newRecs error %v", err)
	}
	for n := 0; n < b.N; n++ {
		rsu := NewRSU(recs.perm, recs.root, recs.path)
		for _, s := range results {
			if s.valid {
				canBytes, _ := hex.DecodeString(s.hex)
				now, _ := base.UTCNow()
				if resp, err := from.NewResp(s.net, canBytes, now); err != nil {
					b.Fatalf("%s%v%s", base.Red, err, base.Reset)
				} else if event, err := NewEventResp(resp); err == nil {
					//b.Fatalf("unexpected new resp error %v", err)
				} else {
					rsu.buffer.Append(event)
				}
			}
		}
		if r, s, u, err := rsu.Save(); err != nil {
			b.Fatal(err)
		} else {
			b.Logf("r=%d s=%d u=%d", r, s, u)
		}
	}
}*/
