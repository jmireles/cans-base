package rsu

import (
	"os"

	"gitlab.com/jmireles/cans-base/milk/old-recs"
)

type RSU struct {
	*recs.Recs
}

func NewRSU(root, folder string, mode os.FileMode) *RSU {
	return &RSU {
		Recs: recs.NewRecs(root, folder, mode),
	}
}

/*
// ReadBoxes returns an array of src strings in hex format: NLLII (net, line, id)
// Every string is a dir where to find date folders which in turn
// have binary files with responses with timestamps hour-minute-second
func (r *RSU) ReadBoxes(base recs.RSU) ([]recs.NLI, error) {
	switch base {
	
	case recs.Req:
		return recs.NLIFiles(r.r.Base)
	
	case recs.Sol:
		return recs.NLIFiles(r.s.Base)
	
	case recs.Unsol:
		return recs.NLIFiles(r.u.Base)
	
	default:
		return nil, fmt.Errorf("invalid base")
	}
}

// RespDates returns an array of FolderYMD where to look for binary files.
func (r *RSU) ReadDates(base recs.RSU, box string) ([]recs.YMD, error) {
	switch base {
	
	case recs.Req:
		return recs.YMDs(r.r.Base, box)
	
	case recs.Sol:
		return recs.YMDs(r.s.Base, box)
	
	case recs.Unsol:
		return recs.YMDs(r.u.Base, box)
	
	default:
		return nil, fmt.Errorf("invalid base")
	}
}

// RespDate returns a binary file with  with responses with timestamps hour-minute-second
func (r *RSU) ReadDate(base recs.RSU, box string, date string) ([]byte, error) {
	switch base {
	
	case recs.Req:
		return recs.FileRead(r.r.Base, box, date)
	
	case recs.Sol:
		return recs.FileRead(r.s.Base, box, date)
	
	case recs.Unsol:
		return recs.FileRead(r.u.Base, box, date)
	
	default:
		return nil, fmt.Errorf("invalid base")
	}
}
*/





