package rsu

import (
	"context"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/old-recs"
)

type Recorder struct {
	rsu    *RSU
	freq   time.Duration
	event  chan recs.Event
	counts *Counts
}

func NewRecorderTest(folder string) (*Recorder, error) {
	return newRecorder("", folder, 0777), nil
}

func NewRecorder(fromHome bool, folder string) (*Recorder, error) {
	root := "."
	if fromHome {
		if home, err := os.UserHomeDir(); err != nil {
			return nil, err
		} else {
			root = home
		}
	}
	return newRecorder(root,folder,0644), nil
}

func newRecorder(root, folder string, mode os.FileMode) *Recorder {
	return &Recorder{
		rsu:    NewRSU(root, folder, mode),
		freq:   10 * time.Second,
		event:  make(chan recs.Event),
		counts: &Counts{},
	}
}

func (r *Recorder) Freq(freq time.Duration) {
	r.freq = freq
}

// Response saves the given valid response to records folders with
// the timestamp supplied
// implements api.Reader
func (r *Recorder) Response(response from.Resp) error {
	if event, err := recs.NewEventRSUResp(response); err != nil {
		return err
	} else {
		r.event <- event
		return nil
	}
}

// Request appends the given resquest to records folders to be saved
func (r *Recorder) Request(message *line.Mess) error {
	if event, err := recs.NewEventRSUReq(message); err != nil {
		return err
	} else {
		r.event <- event
		return nil
	}
}

// Json appends to given buffer json path and counts
// implements api.Reader
func (r *Recorder) Json(sb *strings.Builder) {
	sb.WriteString(`{`)
	sb.WriteString(`"folder":"` + r.rsu.Folder() + `"`)
	sb.WriteString(`,"counts":`)
	r.counts.Json(sb)
	sb.WriteString(`}`)
}

func (r *Recorder) Serve(ctx context.Context, log base.Log) error {

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	nothing := 0

	save := func() error {
		if m, err := r.rsu.Save(); err != nil {
			if log != nil {
				log("save error %v", err)
			}
			r.counts.Error()
			return err
		
		} else if m != nil {
			req, sol, unsol := 0, 0, 0
			if c, ok := m[string(recs.Req)]; ok {
				req = c
			}
			if c, ok := m[string(recs.Sol)]; ok {
				sol = c
			}
			if c, ok := m[string(recs.Unsol)]; ok {
				unsol = c
			}
			if log != nil {
				log("saved req=%d sol=%d unsol=%d nothing=%d", req, sol, unsol, nothing)
			}
			r.counts.Incr(req, sol, unsol)
		
		} else {
			if log != nil {
				nothing++
			}
		}
		return nil
	}
	// 1. Create startup recorder event whit now time
	now, _ := base.UTCNow()
	mess, _ := line.NewMess(15, []byte{0,0,0,0}, now)
	event, err := recs.NewEventRSUReq(mess)
	if err != nil {
		// notify startp error and abort goroutine
		if log != nil {
			log("startup error")
		}
		return err
	}
	// 2. try to save immediatelly the startup
	r.rsu.Append(event)
	if err := save(); err != nil {
		// notify startup save error and abort goroutine
		if log != nil {
			log("startup save error")
		}
		return err
	}

	done := make(chan error, 1)
	ticker := time.NewTicker(r.freq)
	defer ticker.Stop()
	go func() {
		for {
			select {
			case <-ticker.C:
				if err := save(); err != nil {
					done<- err
					return
				}

			case event := <-r.event:
				r.rsu.Append(event)

			case <-ctx.Done():
				save()
				if log != nil {
					log("stopped")
				}
				done<- nil
				return
			}
		}
	}()
	time.Sleep(1 * time.Second)
	return <-done
}

// Counts is used to count operations on dirs of three groups or error.
type Counts struct {
	E  int
	R  int
	S  int
	U  int
	mu sync.Mutex
}

func (c *Counts) Error() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.E += 1
}

func (c *Counts) Incr(r, s, u int) {
	c.mu.Lock()
	defer c.mu.Unlock()
	if r > 0 {
		c.R += r
	}
	if s > 0 {
		c.S += s
	}
	if u > 0 {
		c.U += u
	}
}

func (c *Counts) Json(sb *strings.Builder) {
	c.mu.Lock()
	defer c.mu.Unlock()
	sb.WriteString(`{`)
	sb.WriteString(`"err":`  + strconv.Itoa(c.E))
	sb.WriteString(`,"req":` + strconv.Itoa(c.R))
	sb.WriteString(`,"sol":` + strconv.Itoa(c.S))
	sb.WriteString(`,"uns":` + strconv.Itoa(c.U))
	sb.WriteString(`}`)
}

