package recs

import (
	"fmt"
	"path/filepath"
	"os"
	"strconv"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/to"
)


var (
	invalidDir  = fmt.Errorf("Invalid directory")
	invalidMess = fmt.Errorf("Invalid message")
	invalidNet  = fmt.Errorf("Invalid net")

	hex = []byte{
		'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f',
	}
)

type Dir string

type Comm Dir

type RSU Comm

const (
	Req   RSU = "req"
	Sol   RSU = "sol"
	Unsol RSU = "uns"
)


// NLC is a dir of the string form nLLCC where:
//	- n is a net valid from 0-0x0F
//	- LL is a line valid from 0 - 0xFF
//	- CC is a command valid from 0 -0x1F

type NLC Comm

func NewNLC(net base.Net, line, comm byte) NLC {
	return NLC(string([]byte{
		hex[net & 0x0F],
		hex[line >> 4],
		hex[line & 0x0F],
		hex[comm >> 4],
		hex[comm & 0x0F],
	}))
}

func NewNLCReq(mess *line.Msg) (nlc NLC, i I, payload int, e error) {
	if mess == nil {
		e = invalidMess

	} else if _, line, box, err := mess.ParseReq(); err != nil {
		e = err

	} else {
		nlc = NewNLC(mess.Net, byte(line), mess.Comm())
		i = NewI(box)
		payload, e = line.Payload(mess.Comm())
	}
	return
}

func NewNLCResp(resp from.Resp) (nlc NLC, i I, payload int, e error) {
	if resp == nil {
		e = invalidMess

	} else if comm, _, _, err := resp.Comm(); err != nil {
		e = err

	} else if line, err := resp.From(); err != nil {
		e = err

	} else {
		nlc = NewNLC(resp.NetId(), byte(line), comm)
		i = NewI(resp.BoxId())
		payload, e = line.Payload(comm)
	}
	return
}

func NewNLCFile(file os.FileInfo) (nlc NLC, err error) {
	if !file.IsDir() {
		err = invalidDir

	} else if name := file.Name(); len(name) != 5 {
		err = invalidDir

	} else if i, err := strconv.ParseInt(name, 16, 64); err != nil {
		err = invalidDir

	} else if i < 0 || i > 0xFFFFF {
		err = invalidNet

	} else if net, err := base.NewNet(byte(i >> 16)); err != nil {
		err = invalidNet
	
	} else {
		nlc = NewNLC(
			net,
			byte(i >> 8),  // line
			byte(i),       // command
		)
	}
	return
}

type Box Dir

// A NLI is a string to be used as a dir name to store box's Net, Line and Id event.
// Consist of 5 chars to represent an hexadecimal number.
// Format is NLI, where N is a net (0-15), LL a line (0-255) and II an id (0-255).
type NLI Box


func NewNLI(net, line, id byte) NLI {
	return NLI(string([]byte{
		hex[net & 0x0F],
		hex[line >> 4],
		hex[line & 0x0F],
		hex[id >> 4],
		hex[id & 0x0F],
	}))
}

// NewNLIfile returns a Src from file information (a dir name).
// returns "" and false when the file is not a dir and name size is not 5.
func NewNLIFile(file os.FileInfo) (nli NLI, err error) {
	if !file.IsDir() {
		err = invalidDir

	} else if name := file.Name(); len(name) != 5 {
		err = invalidDir

	} else if i, err := strconv.ParseInt(name, 16, 64); err != nil {
		err = invalidDir

	} else if i < 0 || i > 0xFFFFF {
		err = invalidNet

	} else {
		nli = NewNLI(
			byte(i >> 16), // net
			byte(i >> 8),  // line
			byte(i),       // id
		)
	}
	return
}


func NLIFiles(base string) ([]NLI, error) {
	nlis := make([]NLI, 0)
	err := filepath.Walk(base, func(path string, file os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if path == base {
			return nil // avoid base included by walk
		}
		if nli, err := NewNLIFile(file); err != nil {
			return err

		} else {
			nlis = append(nlis, nli)
		}
		return nil
	})
	return nlis, err
}



type I Box

func NewI(id byte) I {
	return I(string([]byte{
		hex[int(id) >> 4],
		hex[int(id) & 0x0F],
	}))
}




type BoxDate string

func NewBoxDate(box Box, date Date) BoxDate {
	return BoxDate(filepath.Join(string(box), string(date)))
}

func NFromIYMD(net byte, from from.From, id byte, utc base.UTC) BoxDate {
	return nliYmd(net, byte(from), id, utc)
}

func NToIYMD(net byte, to to.To, id byte, utc base.UTC) BoxDate {
	return nliYmd(net, byte(to), id, utc)
}

func nliYmd(net, line, id byte, utc base.UTC) BoxDate {
	//key := fmt.Sprintf("%x%02x%02x/%s", net, from, id, utc.YMD())
	return BoxDate(string([]byte{
		hex[net & 0x0F],
		hex[line >> 4],
		hex[line & 0x0F],
		hex[id >> 4],
		hex[id & 0x0F],
		os.PathSeparator, // '/'
		hex[utc[0] / 10], // year-decade
		hex[utc[0] % 10], // year-unit
		hex[utc[1] / 10], // month-decade
		hex[utc[1] % 10], // month-unit
		hex[utc[2] / 10], // day-decade
		hex[utc[2] % 10], // day-unit
	}))
}

