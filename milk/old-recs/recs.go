package recs

import (
	"path/filepath"
	"os"
)

type Recs struct {
	root   string
	folder string
	mode   os.FileMode
	comms  map[string]*Files
}

func NewRecs(root, folder string, mode os.FileMode) *Recs {
	return &Recs{
		root:   root,
		folder: folder,
		mode:   mode,		
		comms:  make(map[string]*Files),
	}
}

// Folder returns the folder for JSON recorder health.
// Root is kept secret for publication.
func (r *Recs) Folder() string {
	return r.folder
}

// Gets returns the Files associated with given comm path
// Creates a new map element when files does not exist.
func (r *Recs) Get(comm Comm) *Files {
	if f, ok := r.comms[string(comm)]; ok {
		return f
	} else {
		f = r.newComms(comm)
		r.comms[string(comm)] = f
		return r.comms[string(comm)]
	}
}

func (r *Recs) newComms(comm Comm) *Files {
	if root := r.root; root != "" {
		base := filepath.Join(root, r.folder, string(comm))
		return NewFiles(base, r.mode)

	} else {
		base := filepath.Join(r.folder, string(comm))
		return NewFiles(base, r.mode)

	}
}

// Append event to files map.
func (r *Recs) Append(event Event) {
	if event != nil {
		f := r.Get(event.Comm())
		f.append(event)
	}
}

// Save saves to filesystem all events already appended.
// Once success, files are cleared not the key comm but the
// event's content.
func (r *Recs) Save() (map[string]int, error) {
	var o map[string]int
	for s, f := range r.comms {
		if n, err := f.Save(); err != nil {
			return o, err
		} else if n > 0 {
			if o == nil {
				o = make(map[string]int, 0)
			}
			o[s] = n
		}
	}
	return o, nil
}


