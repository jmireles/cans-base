package i4

import (
	"gitlab.com/jmireles/cans-base/milk"
)

// requests
const (
	Get03_Config milk.Get = 3  // cfg 1
	Set04_Config milk.Set = 4  // cfg 1
)

// responses
const (
	Sol03_Config   milk.Sol   = 3 // cfg 1

	Unsol05_Change milk.Unsol = 5
)

func Req(comm byte) (*milk.Get, *milk.Set) {
	// get requests
	get := milk.Get(comm)
	switch get {
	case milk.Get00_Version,
		milk.Get01_Serial,
		milk.Get02_Status,
		Get03_Config:
		return &get, nil
	}
	// set requests
	set := milk.Set(comm)
	switch set {
	case Set04_Config:
		return nil, &set
	}
	return nil, nil
}

func Resp(comm byte) (*milk.Sol, *milk.Unsol) {
	for _, unsol := range unsols {
		if comm == byte(unsol) {
			return nil, &unsol
		}
	}
	for _, sol := range sols {
		if comm == byte(sol) {
			return &sol, nil
		}
	}
	return nil, nil
}

func Resps() ([]milk.Sol, []milk.Unsol) {
	return sols, unsols
}

var sols = []milk.Sol {
	milk.Sol00_Version,
	milk.Sol02_Status,
	Sol03_Config,
}

var unsols = []milk.Unsol {
	milk.Unsol01_Restart,
	Unsol05_Change,
}

