package recs

import (
	"encoding/hex"
	"fmt"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/p1"
)

func TestRecs1(t *testing.T) {
	tmp := t.TempDir()
	tt := base.Test{ T:t }
	testRecs(tmp, tt, NLRI_YMD)
}

func TestRecs2(t *testing.T) {
	tmp := t.TempDir()
	tt := base.Test{ T:t }
	recs := testRecs(tmp, tt, YMD_NLRI)
	// zip
	remove := true
	logFS := func(name string, size int64) {
		base.LogBlue("%v %v\n", name, size)
	}
	// zip date by date print filesystem reduced
	tt.Ok(recs.Zip("091011", remove, tt.Log))
	tt.Ok(recs.Files(logFS))
	tt.Ok(recs.Zip("101112", remove, tt.Log))
	tt.Ok(recs.Files(logFS))
	tt.Ok(recs.Zip("111213", remove, tt.Log))
	tt.Ok(recs.Files(logFS))
}

func TestRecsPmStates1(t *testing.T) {
	tt := base.Test{ T:t }
	temp := t.TempDir()
	recs := NewRecs("", temp, 0777, NLRI_YMD)
	testRecsPmStates(tt, recs, tt.Log)
}

func TestRecsPmStates2(t *testing.T) {
	tt := base.Test{ T:t }
	temp := t.TempDir()
	recs := NewRecs("", temp, 0777, YMD_NLRI)
	testRecsPmStates(tt, recs, tt.Log)
	// zip
	remove := true
	logFS := func(name string, size int64) {
		tt.Log("%s%v %v%s\n", base.Blue, name, size, base.Reset)
	}
	tt.Ok(recs.Zip("140605", remove, tt.Log))
	tt.Ok(recs.Files(logFS))
}














func testRecs(temp string, tt base.Test, fs RecFS) *Recs {
	recs := NewRecs("", temp, 0777, fs)
	testRecordAppend(tt, recs, func(f string, args ...any) {
		tt.Log(f, args...)
	})
	testRecordSave(tt, recs)
	err := recs.Files(func(name string, size int64) {
		tt.Log("%s%v %v%s\n", base.Blue, name, size, base.Reset)
	})
	tt.Ok(err)
	return recs
}


func testRecordAppend(tt base.Test, recs *Recs, log base.Log) {
	t1, _ := base.NewUTC(9,10,11, 12,13,14,15)  // 2009/Oct/11 12:13:14.15
	t2, _ := base.NewUTC(10,11,12, 13,14,15,16) // 2010/Nov/12 13:14:15.16
	t3, _ := base.NewUTC(11,12,13, 14,15,59,99) // 2011/Dec/13 14:15:59.99
	for _, test := range []struct{
		net byte     // n
		can string   // lliissddcceeeeeeeeeeeeeeee
		utc base.UTC // 
		rec string   // nllx/ii/yymmdd hh:mm:ss.cc ------
	} {
		// PM to
		{ 1, "01550000",                 t1, "101b55/091011 12:13:14.15 000000000000000000" }, // get version
		{ 1, "01550001",                 t1, "101b55/091011 12:13:14.15 010000000000000000" }, // get serial
		{ 1, "01550002",                 t1, "101b55/091011 12:13:14.15 020000000000000000" }, // get status
		{ 1, "01550003",                 t1, "101b55/091011 12:13:14.15 030000000000000000" }, // get old-cfg
		{ 1, "01550004fedcba9876543210", t2, "101b55/101112 13:14:15.16 04fedcba9876543210" }, // set old-cfg
		{ 1, "0155000501",               t2, "101b55/101112 13:14:15.16 050100000000000000" }, // get values
		{ 1, "01550006",                 t2, "101b55/101112 13:14:15.16 060000000000000000" }, // get avgs
		{ 1, "01550007",                 t2, "101b55/101112 13:14:15.16 070000000000000000" }, // get calibr 
		{ 1, "01550008fedcba9876543210", t3, "101b55/111213 14:15:59.99 08fedcba9876543210" }, // set calibr
		{ 1, "01550009",                 t3, "101b55/111213 14:15:59.99 090000000000000000" }, // get options 
		{ 1, "0155000afedcba9876543210", t3, "101b55/111213 14:15:59.99 0afedcba9876543210" }, // set options
		{ 1, "0155000b01",               t3, "101b55/111213 14:15:59.99 0b0100000000000000" }, // get profiles

		{ 1, "0154000cfedcba9876543210", t1, "101b54/091011 12:13:14.15 0cfedcba9876543210" }, // set profiles
		{ 1, "0154000d",                 t1, "101b54/091011 12:13:14.15 0d0000000000000000" }, // get display
		{ 1, "0154000efedcba9876543210", t1, "101b54/091011 12:13:14.15 0efedcba9876543210" }, // set display
		{ 1, "0154000f",                 t1, "101b54/091011 12:13:14.15 0f0000000000000000" }, // get config
		{ 1, "01540010fedcba9876543210", t2, "101b54/101112 13:14:15.16 10fedcba9876543210" }, // set config
		{ 1, "01540011",                 t2, "101b54/101112 13:14:15.16 110000000000000000" }, // get alarms
		{ 1, "01540012fedcba9876543210", t2, "101b54/101112 13:14:15.16 12fedcba9876543210" }, // set alarms
		{ 1, "01540013",                 t2, "101b54/101112 13:14:15.16 130000000000000000" }, // get stalls
		{ 1, "01540014fedcba9876543210", t3, "101b54/111213 14:15:59.99 14fedcba9876543210" }, // set stalls
		
		{ 1, "01400015",                 t1, "",                                             }, // 21
		{ 1, "013f0016",                 t1, "",                                             }, // 22
		{ 1, "013e0017",                 t1, "",                                             }, // 23
		{ 1, "013d0018",                 t1, "",                                             }, // 24
		{ 1, "013c0019",                 t1, "",                                             }, // 25
		{ 1, "013b001a",                 t1, "",                                             }, // 26
		{ 1, "013a001b",                 t1, "",                                             }, // 27
		{ 1, "0139001c",                 t1, "",                                             }, // 28
		{ 1, "0138001d",                 t1, "",                                             }, // 29
		{ 1, "0137001e",                 t1, "",                                             }, // 30
		{ 1, "0136001f",                 t1, "",                                             }, // 31

		// RS to
		{ 1, "06550000",                 t1, "106b55/091011 12:13:14.15 000000000000000000" }, // get version
		{ 1, "06550001",                 t1, "106b55/091011 12:13:14.15 010000000000000000" }, // get serial
		{ 1, "06550002",                 t1, "106b55/091011 12:13:14.15 020000000000000000" }, // get status
		{ 1, "0655000301",               t1, "106b55/091011 12:13:14.15 030100000000000000" }, // get cfg
		{ 1, "065500040123456789abcdef", t1, "106b55/091011 12:13:14.15 040123456789abcdef" }, // set cfg

		// T4 to
		{ 2, "07400000",                 t1, "207b40/091011 12:13:14.15 000000000000000000" }, // get version
		{ 2, "07400001",                 t1, "207b40/091011 12:13:14.15 010000000000000000" }, // get serial
		{ 2, "07400002",                 t1, "207b40/091011 12:13:14.15 020000000000000000" }, // get status
		{ 2, "07400003",                 t1, "207b40/091011 12:13:14.15 030000000000000000" }, // get stalls
		{ 2, "07400004",                 t1, "207b40/091011 12:13:14.15 040000000000000000" }, // get equipment
		{ 2, "07400005",                 t1, "207b40/091011 12:13:14.15 050000000000000000" }, // get milking
		{ 2, "0740000601",               t1, "207b40/091011 12:13:14.15 060100000000000000" }, // get profiles
		{ 2, "07400007",                 t1, "207b40/091011 12:13:14.15 070000000000000000" }, // get colors
		{ 1, "073f0008",                 t1, "107b3f/091011 12:13:14.15 080000000000000000" }, // get pallete
		{ 1, "073f00090123456789abcdef", t1, "107b3f/091011 12:13:14.15 090123456789abcdef" }, // set stalls
		{ 1, "073f000a0123456789abcdef", t1, "107b3f/091011 12:13:14.15 0a0123456789abcdef" }, // set equipment
		{ 1, "073f000b0123456789abcdef", t1, "107b3f/091011 12:13:14.15 0b0123456789abcdef" }, // set milking
		{ 1, "073f000c0123456789abcdef", t1, "107b3f/091011 12:13:14.15 0c0123456789abcdef" }, // set profiles
		{ 1, "073f000d0123456789abcdef", t1, "107b3f/091011 12:13:14.15 0d0123456789abcdef" }, // set colors
		{ 1, "073f000e0123456789abcdef", t1, "107b3f/091011 12:13:14.15 0e0123456789abcdef" }, // set pallete
		{ 1, "073f000f",                 t1, "107b3f/091011 12:13:14.15 0f0000000000000000" }, // get levels
		{ 1, "073f0010",                 t1, "107b3f/091011 12:13:14.15 100000000000000000" }, // get rotary
		{ 1, "073f00110123456789abcdef", t1, "107b3f/091011 12:13:14.15 110123456789abcdef" }, // set rotary
		{ 3, "07220012",                 t1, "",                                             }, // 18
		{ 4, "07210013",                 t1, "",                                             }, // 19
		{ 1, "07200014",                 t1, "",                                             }, // 20
		{ 2, "071f0015",                 t1, "",                                             }, // 21
		{ 3, "071e0016",                 t1, "",                                             }, // 22
		{ 4, "071d0017",                 t1, "",                                             }, // 23
		{ 1, "071c0018",                 t1, "",                                             }, // 24
		{ 2, "071b0019",                 t1, "",                                             }, // 25
		{ 3, "071a001a",                 t1, "",                                             }, // 26
		{ 4, "0709001b",                 t1, "",                                             }, // 27
		{ 1, "0708001c",                 t1, "",                                             }, // 28
		{ 2, "0707001d",                 t1, "",                                             }, // 29
		{ 3, "0706001e",                 t1, "",                                             }, // 30
		{ 4, "0705001f",                 t1, "",                                             }, // 31
		
		// I4 to
		{ 1, "08550000",                 t1, "108b55/091011 12:13:14.15 000000000000000000" }, // get version
		{ 2, "08540001",                 t1, "208b54/091011 12:13:14.15 010000000000000000" }, // get serial
		{ 3, "08530002",                 t1, "308b53/091011 12:13:14.15 020000000000000000" }, // get status
		{ 4, "0852000301",               t1, "408b52/091011 12:13:14.15 030100000000000000" }, // get cfg
		{ 4, "085200040123456789abcdef", t1, "408b52/091011 12:13:14.15 040123456789abcdef" }, // set cfg

		// PM from
		{ 1, "1000990055",               t1, "110b99/091011 12:13:14.15 005500000000000000" }, // sol version
		{ 1, "10009901",                 t1, "110s99/091011 12:13:14.15 010000"             }, // unsol restart
		{ 1, "10009902",                 t1, "110b99/091011 12:13:14.15 020000000000000000" }, // sol status
		{ 1, "10009903ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 03ffeeddccbbaa9988" }, // sol cfg
		{ 1, "10009904aa55aa55aa55aa55", t1, "110v99/091011 12:13:14.15 04aa55aa55aa55aa55" }, // resp curves
		{ 1, "10009905",                 t1, "110s99/091011 12:13:14.15 050000"             }, // unsol idle 
		{ 1, "10009906",                 t1, "110s99/091011 12:13:14.15 060000"             }, // unsol working
		{ 1, "1000990755aa",             t1, "110s99/091011 12:13:14.15 0755aa"             }, // unsol alarms
		{ 1, "100099081122334455667788", t1, "110v99/091011 12:13:14.15 081122334455667788" }, // sol avg
		{ 1, "100099092233445566778899", t1, "110v99/091011 12:13:14.15 092233445566778899" }, // sol avg
		{ 1, "1000990a33445566778899aa", t1, "110v99/091011 12:13:14.15 0a33445566778899aa" }, // sol avg
		{ 1, "1000990b445566778899aabb", t1, "110v99/091011 12:13:14.15 0b445566778899aabb" }, // sol avg
		{ 1, "1000990c1122334455667788", t1, "110v99/091011 12:13:14.15 0c1122334455667788" }, // sol val
		{ 1, "1000990d2233445566778899", t1, "110v99/091011 12:13:14.15 0d2233445566778899" }, // sol val
		{ 1, "1000990e33445566778899aa", t1, "110v99/091011 12:13:14.15 0e33445566778899aa" }, // sol val
		{ 1, "1000990f445566778899aabb", t1, "110v99/091011 12:13:14.15 0f445566778899aabb" }, // sol val
		{ 1, "10009910",                 t1, "110s99/091011 12:13:14.15 100000"             }, // unsol alarm front
		{ 1, "10009911",                 t1, "110s99/091011 12:13:14.15 110000"             }, // unsol alarm rear
		{ 1, "10009912ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 12ffeeddccbbaa9988" }, // sol cfg 18
		{ 1, "10009913",                 t1, "",                                             }, // 19
		{ 1, "10009914",                 t1, "110s99/091011 12:13:14.15 140000"             }, // unsol stimulation
		{ 1, "10009915ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 15ffeeddccbbaa9988" }, // sol cfg 21
		{ 1, "10009916ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 16ffeeddccbbaa9988" }, // sol cfg 22
		{ 1, "10009917ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 17ffeeddccbbaa9988" }, // sol cfg 23
		{ 1, "10009918ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 18ffeeddccbbaa9988" }, // sol cfg 24
		{ 1, "10009919ffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 19ffeeddccbbaa9988" }, // sol cfg 25
		{ 1, "1000991a0011223344556677", t1, "110v99/091011 12:13:14.15 1a0011223344556677" }, // unsol notification
		{ 1, "1000991bffeeddccbbaa9988", t1, "110b99/091011 12:13:14.15 1bffeeddccbbaa9988" }, // sol cfg 27
		{ 1, "1000991c",                 t1, "",                                             }, // 28
		{ 1, "1000991d",                 t1, "",                                             }, // 29
		{ 1, "1000991e",                 t1, "",                                             }, // 30
		{ 1, "1000991f",                 t1, "",                                             }, // 31

		// RS from 1-4
		{ 1, "6000770055",               t1, "160b77/091011 12:13:14.15 005500000000000000" }, // sol version
		{ 1, "60007701",                 t1, "160b77/091011 12:13:14.15 010000000000000000" }, // unsol restart
		{ 1, "60007702",                 t1, "160b77/091011 12:13:14.15 020000000000000000" }, // sol status
		{ 1, "60007703ffeeddccbbaa9988", t1, "160b77/091011 12:13:14.15 03ffeeddccbbaa9988" }, // sol cfg
		{ 1, "60007704",                 t1, "",                                             }, // 4
		{ 1, "60007705",                 t1, "",                                             }, // 5
		// RS from 6
		{ 1, "60007706",                 t1, "",                                             }, // invalid-extra
		{ 1, "6000770600",               t1, "",                                             }, // invalid-extra
		{ 1, "600077060102",             t1, "160s01/091011 12:13:14.15 060277"             }, // stall=01, reader=02 box=95
		{ 1, "600077060f10",             t1, "160s0f/091011 12:13:14.15 061077"             }, // stall=0f, reader=10 box=94
		{ 1, "600077065556",             t1, "160s55/091011 12:13:14.15 065677"             }, // stall=55, reader=56 box=93
		{ 1, "600077065556",             t1, "160s55/091011 12:13:14.15 065677"             }, // stall=55, reader=56 box=93
		// RS from 7,8,...
		{ 1, "60007707",                 t1, "",                                             }, // 4
		{ 1, "60007708",                 t1, "",                                             }, // 5

		// T4 from 1-8
		{ 1, "7000220055",               t1, "170b22/091011 12:13:14.15 005500000000000000" }, // sol version
		{ 1, "70002201",                 t1, "170b22/091011 12:13:14.15 010000000000000000" }, // unsol restart
		{ 1, "700022020123456789abcdef", t1, "170b22/091011 12:13:14.15 020123456789abcdef" }, // sol status
		{ 1, "70002203ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 03ffeeddccbbaa9988" }, // sol stalls
		{ 1, "70002204ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 04ffeeddccbbaa9988" }, // sol equipment
		{ 1, "70002205ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 05ffeeddccbbaa9988" }, // sol milking
		{ 1, "70002206ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 06ffeeddccbbaa9988" }, // sol profiles
		{ 1, "70002207ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 07ffeeddccbbaa9988" }, // sol colors
		{ 1, "70002208ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 08ffeeddccbbaa9988" }, // sol pallete
		// T4 from 9
		{ 1, "7000220901fedcba98765432", t1, "170v01/091011 12:13:14.15 09fedcba9876543222" }, // sol stage stall=01
		{ 1, "700022091fedcba987654321", t1, "170v1f/091011 12:13:14.15 09edcba98765432122" }, // sol stage stall=1f
		{ 1, "7000220955dcba9876543210", t1, "170v55/091011 12:13:14.15 09dcba987654321022" }, // sol stage stall=55
		{ 1, "70002209aacba9876543210f", t1, "170vaa/091011 12:13:14.15 09cba9876543210f22" }, // sol stage stall=aa
		{ 1, "70002209",                 t1, "",                                             }, // sol stage invalid-extra
		{ 1, "7000220900",               t1, "",                                             }, // sol stage invalid-extra
		// T4 from 10
		{ 1, "7000220a0101",             t1, "170v01/091011 12:13:14.15 0a0100000000000022" }, // sol mode stall=01
		{ 1, "7000220a1f02",             t1, "170v1f/091011 12:13:14.15 0a0200000000000022" }, // sol mode stall=1f
		{ 1, "7000220a5503",             t1, "170v55/091011 12:13:14.15 0a0300000000000022" }, // sol mode stall=55
		{ 1, "7000220aaa04",             t1, "170vaa/091011 12:13:14.15 0a0400000000000022" }, // sol mode stall=aa
		{ 1, "7000220a",                 t1, "",                                             }, // sol mode invalid-extra
		{ 1, "7000220a00",               t1, "",                                             }, // sol mode invalid-extra
		// T4 from 11,12
		{ 1, "7000220b",                 t1, "",                                             }, // 11
		{ 1, "7000220c0123456789abcdef", t1, "170b22/091011 12:13:14.15 0c0123456789abcdef" }, // sol levels
		// T4 from 13
		{ 1, "7000220d010123456789abcd", t1, "170v01/091011 12:13:14.15 0d0123456789abcd22" }, // sol level stall=01
		{ 1, "7000220d1f123456789abcde", t1, "170v1f/091011 12:13:14.15 0d123456789abcde22" }, // sol level stall=1f
		{ 1, "7000220d5523456789abcdef", t1, "170v55/091011 12:13:14.15 0d23456789abcdef22" }, // sol level stall=55
		{ 1, "7000220daa3456789abcdef0", t1, "170vaa/091011 12:13:14.15 0d3456789abcdef022" }, // sol level stall=aa
		{ 1, "7000220d",                 t1, "",                                             }, // sol level invalid-extra
		{ 1, "7000220d00",               t1, "",                                             }, // sol level invalid-extra
		// T4 from 14-31
		{ 1, "7000220e",                 t1, "",                                             }, // 14 don't save leds
		{ 1, "7000220f",                 t1, "",                                             }, // 15
		{ 1, "70002210",                 t1, "",                                             }, // 16
		{ 1, "70002211",                 t1, "",                                             }, // 17
		{ 1, "70002212ffeeddccbbaa9988", t1, "170b22/091011 12:13:14.15 12ffeeddccbbaa9988" }, // sol rotary
		{ 1, "70002213",                 t1, "",                                             }, // 19
		{ 1, "70002214",                 t1, "",                                             }, // 20
		{ 1, "70002215",                 t1, "",                                             }, // 21
		{ 1, "70002216",                 t1, "",                                             }, // 22
		{ 1, "70002217",                 t1, "",                                             }, // 23
		{ 1, "70002218",                 t1, "",                                             }, // 24
		{ 1, "70002219",                 t1, "",                                             }, // 25
		{ 1, "7000221a",                 t1, "",                                             }, // 26
		{ 1, "7000221b",                 t1, "",                                             }, // 27
		{ 1, "7000221c",                 t1, "",                                             }, // 28
		{ 1, "7000221d",                 t1, "",                                             }, // 29
		{ 1, "7000221e",                 t1, "",                                             }, // 30
		{ 1, "7000221f",                 t1, "",                                             }, // 31

		// I4 from
		{ 1, "8000990055",               t1, "180b99/091011 12:13:14.15 005500000000000000" }, // sol version
		{ 1, "80009901",                 t1, "180b99/091011 12:13:14.15 010000000000000000" }, // unsol restart
		{ 1, "80009902",                 t1, "180b99/091011 12:13:14.15 020000000000000000" }, // sol status
		{ 1, "80009903ffeeddccbbaa9988", t1, "180b99/091011 12:13:14.15 03ffeeddccbbaa9988" }, // sol cfg
		{ 1, "80009904",                 t1, "",                                             }, // 4
		// I4 from 5
		{ 1, "80009905",                 t1, "",                                             }, // zone invalid extra
		{ 1, "8000990501",               t1, "",                                             }, // zone invalid extra
		{ 1, "800099050000000000000000", t1, "180z00/091011 12:13:14.15 0000000099"         }, // zone=0 in=00 f=0
		{ 1, "800099050010",             t1, "180z01/091011 12:13:14.15 0010000099"         }, // zone=1 in=00 f=0
		{ 1, "800099050017",             t1, "180z01/091011 12:13:14.15 0017000099"         }, // zone=1 in=00 f=7
		{ 1, "80009905001f",             t1, "180z01/091011 12:13:14.15 001f000099"         }, // zone=1 in=00 f=f
		{ 1, "80009905002f",             t1, "180z02/091011 12:13:14.15 002f000099"         }, // zone=2 in=00 f=f
		{ 1, "80009905007f",             t1, "180z07/091011 12:13:14.15 007f000099"         }, // zone=7 in=00 f=f
		{ 1, "80009905017f",             t1, "180z07/091011 12:13:14.15 017f000099"         }, // zone=7 in=01 f=f
		{ 1, "800099050f7f",             t1, "180z07/091011 12:13:14.15 0f7f000099"         }, // zone=7 in=0f f=f
		{ 1, "800099050f7f1234",         t1, "180z07/091011 12:13:14.15 0f7f123499"         }, // zone=7 in=0f f=f 1234
		// I4 from 6,7...
		{ 1, "80009906",                 t1, "",                                             }, // 6
		{ 1, "80009907",                 t1, "",                                             }, // 7
	} {
		bytes, _ := hex.DecodeString(test.can)
		if r, err := NewRecord(test.net, bytes, test.utc); err == nil {
			tt.Equals(test.rec, fmt.Sprintf("%s/%s %s", r.NLRI(), r.Date(), r.RowDebug()))
			//t.Logf("rec %s", test.rec)
			recs.Append(r)
		} else {
			tt.Equals(test.rec, "")
		}
	}

	// RecordBox8 used for PC messages
	for _, test := range []struct {
		box   byte
		comm  byte
		extra string
		utc   base.UTC
		rec   string
	} {
		{ 0, 0, "00",                    t1, "000b00/091011 12:13:14.15 000000000000000000" },
		{ 1, 2, "0123456789abcdef",      t2, "000b01/101112 13:14:15.16 020123456789abcdef" },
		{ 2, 3, "0123456789abcdef01234", t3, "000b02/111213 14:15:59.99 030123456789abcdef" },
	} {
		extra, _ := hex.DecodeString(test.extra)
		r := NewRecordBox8(test.box, test.comm, extra, test.utc)
		tt.Equals(test.rec, fmt.Sprintf("%s/%s %s", r.NLRI(), r.Date(), r.RowDebug()))
		recs.Append(r)
	}
	// Tests all recs appends before save clears the maps.
	for nlri, ymds := range map[string]map[string]int {
		// PM-to boxes, all rows size=12
		"101b55": map[string]int {
			"091011": 4*12, // commands 00,01,02,03
			"101112": 4*12, // commands 04,05,06,07
			"111213": 4*12, // commands 08,09,0a,0b
		},
		"101b54": map[string]int {
			"091011": 4*12, // commands 0c,0d,0e,0f
			"101112": 4*12, // commands 10,11,12,13
			"111213": 1*12, // command 14
		},
		
		// RS-to boxes, all rows size=12
		"106b55": map[string]int { "091011":  5*12 }, // commands 00,01,02,03,04
		
		// T4-to boxes, all rows size=12
		"207b40": map[string]int { "091011":  8*12 }, // commands 00,01,02,03,04,05,06,07
		"107b3f": map[string]int { "091011": 10*12 }, // commands 08,09,0a,0b,0c,0d,0e,0f,10,11
		"108b55": map[string]int { "091011":  1*12 }, // command 00
		
		// I4-to boxes, all rows size=12
		"208b54": map[string]int { "091011":  1*12 }, // command 01
		"308b53": map[string]int { "091011":  1*12 }, // command 02
		"408b52": map[string]int { "091011":  2*12 }, // commands 03,04
		
		// PM-from, box&values rows size=12, state rows size=6
		"110b99": map[string]int { "091011": 10*12 }, // box 99:  10 commands
		"110s99": map[string]int { "091011":  7* 6 }, // stall 99 states:  7 commands
		"110v99": map[string]int { "091011": 10*12 }, // stall 99 values: 10 commands
		
		// RS-from, boxes row size=12, state row size=6
		"160b77": map[string]int { "091011":  4*12 }, // box 77: 4 commands
		"160s01": map[string]int { "091011":  1* 6 }, // stall 01 state: once
		"160s0f": map[string]int { "091011":  1* 6 }, // stall 0f state: once
		"160s55": map[string]int { "091011":  2* 6 }, // stall 55 state: twice
		
		// T4-from, all rows size=12
		"170b22": map[string]int { "091011": 11*12 }, // box 22: 11 commands
		"170v01": map[string]int { "091011":  3*12 }, // stall 01 states: stage,mode,level
		"170v1f": map[string]int { "091011":  3*12 }, // stall 1f states: stage,mode,level
		"170v55": map[string]int { "091011":  3*12 }, // stall 55 states: stage,mode,level
		"170vaa": map[string]int { "091011":  3*12 }, // stall aa states: stage,mode,level
		
		// I4-from, boxes row size=12, zone row size=8
		"180b99": map[string]int { "091011":  4*12 }, // box 99: 4 commands
		"180z00": map[string]int { "091011":  1* 8 }, // zone 0: once
		"180z01": map[string]int { "091011":  3* 8 }, // zone 1: three times
		"180z02": map[string]int { "091011":  1* 8 }, // zone 2: once
		"180z07": map[string]int { "091011":  4* 8 }, // zone 7: four times

		// PC
		"000b00": map[string]int { "091011":  1*12 }, 
		"000b01": map[string]int { "101112":  1*12 },
		"000b02": map[string]int { "111213":  1*12 },

	} {
		file, ok := recs.nlris[nlri]
		tt.Assert(ok, "%s nlri=%s missing%s", base.Red, nlri, base.Reset)
		
		for ymd, exp := range ymds {
			//t.Logf("ymd=%s exp=%v\n", ymd, exp)
			got, ok := file.rows[ymd]
			tt.Assert(ok, "%s nlri=%s ymd=%s missing%s", base.Red, nlri, ymd, base.Reset)
			g := len(got)
			tt.Assert(exp == g, "%s nlri=%s ymd=%s exp=%d got=%d%s", base.Red, nlri, ymd, exp, g, base.Reset)
		}
		log("%s%s %v %s\n", base.Green, nlri, ymds, base.Reset)
	}
}

func testRecordSave(tt base.Test, recs *Recs) {
	saved, err := recs.Save()
	tt.Ok(err)
	tt.Equals(saved, map[string]int{
		"000b00":12,
		"000b01":12,
		"000b02":12,
		"101b54":108,
		"101b55":144,
		"106b55":60,
		"107b3f":120,
		"108b55":12,
		"110b99":120,
		"110s99":42,
		"110v99":120,
		"160b77":48,
		"160s01":6,
		"160s0f":6,
		"160s55":12,
		"170b22":132,
		"170v01":36,
		"170v1f":36,
		"170v55":36,
		"170vaa":36,
		"180b99":48,
		"180z00":8,
		"180z01":24,
		"180z02":8,
		"180z07":32,
		"207b40":96,
		"208b54":12,
		"308b53":12,
		"408b52":24,
	})
}


func testRecsPmStates(tt base.Test, recs *Recs, log base.Log) {
	net := byte(1)
	line := byte(from.P1)
	date := time.Date(2014, time.Month(6), 5, 12, 34, 56, 0, time.UTC)
	nextDate := func() base.UTC {
		date = date.Add(time.Duration(170 * time.Millisecond))
		utc, _ := base.NewUTCTime(date)
		return utc
	}
	src := byte(7)
	comms := []byte {
		byte(milk.Unsol01_Restart),
		byte(p1.Unsol05_Idle),
		byte(p1.Unsol06_Working),
		byte(p1.Unsol07_Alarmed),
		byte(p1.Unsol16_NoPhaseFront),
		byte(p1.Unsol17_NoPhaseRear),
		byte(p1.Unsol20_Stimulation),
	}
	bits := [][]byte {
		[]byte{ 0x80, 0x00 },
		[]byte{ 0x40, 0x00 },
		[]byte{ 0x20, 0x00 },
		[]byte{ 0x10, 0x00 },
		[]byte{ 0x08, 0x00 },
		[]byte{ 0x04, 0x00 },
		[]byte{ 0x02, 0x00 },
		[]byte{ 0x01, 0x00 },
		[]byte{ 0x00, 0x80 },
		[]byte{ 0x00, 0x40 },
		[]byte{ 0x00, 0x20 },
		[]byte{ 0x00, 0x10 },
		[]byte{ 0x00, 0x08 },
		[]byte{ 0x00, 0x04 },
		[]byte{ 0x00, 0x02 },
		[]byte{ 0x00, 0x01 },
	} 
	for i := 0; i < 2; i++ {
		for _, comm := range comms {
			if comm != 7 {
				r := newRecord(net, line, nextDate())
				r.fromP1(src, comm, nil)
				recs.Append(r)
			} else {
				for _, bit := range bits {
					r := newRecord(net, line, nextDate())
					r.fromP1(src, comm, bit)
					recs.Append(r)
				}
			}
			recs.Save()
			err := recs.Files(func(name string, size int64) {
				tt.Log("%s%v %v%s\n", base.Blue, name, size, base.Reset)
			})
			tt.Ok(err)
		}
	}

	var file []byte
	var err error

	switch recs.fs {
	case NLRI_YMD:
		file, err = recs.ReadFile("110s07", "140605")
		tt.Ok(err)

	case YMD_NLRI:
		file, err = recs.ReadFile("140605", "110s07")
		tt.Ok(err)
	}
	f := 0
	nextRow := func(size int, expComm byte, expExtra []byte) {
		exp := nextDate()
		row := file[ f : f+size ]
		eh, em, es, ec := exp.Clock() // expected clock (when append)
		gh, gm, gs, gc := RowTime(row)
		if eh != gh || em != gm || es != gs || ec != gc {
			tt.Assert(true, "%sexp:%02d:%02d:%02d.%02d got:%02d:%02d:%02d.%02d%s", 
				base.Red, eh, em, es, ec, gh, gm, gs, gc, base.Reset)
		}
		if gotComm := RowComm(row); expComm != gotComm {
			tt.Assert(true, "%scomm exp:%02x got:%02x%s", base.Red, expComm, gotComm, base.Reset)
		}
		tt.Equals(expExtra, RowExtra(row))
		log("%s%02d:%02d:%02d.%02d %02x %02x%s\n", 
			base.Green, eh, em, es, ec, expComm, expExtra, base.Reset)
		f += size
	}

	// reset date before call nextRow to check hmsc matches
	date = time.Date(2014, time.Month(6), 5, 12, 34, 56, 0, time.UTC)
	for i := 0; i < 2; i++ {
		for _, comm := range comms {
			if comm != 7 {
				// check row commands 1,5,6,16,17,20 matches hmsc, comm, and empty extra
				nextRow(6, comm, []byte{ 0,0 })
			} else {
				for _, bit := range bits {
					// check row command 7 matches hmsc, comm, and bit extra
					nextRow(6, comm, bit)
				}
			}
		}
	}
}


