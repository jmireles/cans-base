package recs

import (
	"archive/zip"
	"fmt"
	"path/filepath"
	"os"
	"io/ioutil"
	"strings"
	"sync"

	"gitlab.com/jmireles/cans-base"
)

type RecsSaveFunc func(date string, bytes []byte) (int, error)


type RecFS int

const (
	NLRI_YMD RecFS = iota
	YMD_NLRI
)

type Recs struct {

	sync.Mutex

	base   string
	folder string
	mode   os.FileMode
	fs     RecFS
	nlris  map[string]*Files
}

func NewRecs(root, folder string, mode os.FileMode, fs RecFS) *Recs {
	base := ""
	if root != "" {
		base = filepath.Join(root, folder)
	} else {
		base = folder
	}
	return &Recs{
		base:   base,
		folder: folder,
		mode:   mode,		
		fs:     fs,
		nlris:  make(map[string]*Files),
	}
}

// Append appends a record to the buffer
func (r *Recs) Append(record *Record) {
	if record == nil {
		return
	}
	r.Lock()
	defer r.Unlock()
	files := r.get(record)
	files.append(record)
}

// Save saves to filesystem all events appended to the buffers
// Once success, buffers are cleared
func (r *Recs) Save() (map[string]int, error) {
	r.Lock()
	defer r.Unlock()
	var o map[string]int
	for nlri, files := range r.nlris {
		
		n, err := files.save(func(date string, rows []byte) (int, error) {
			switch r.fs {
			case NLRI_YMD:
				return r.appendFile(filepath.Join(r.base, nlri, date), rows)

			case YMD_NLRI:
				return r.appendFile(filepath.Join(r.base, date, nlri), rows)

			default:
				return 0, fmt.Errorf("Invalid RecFS")
			}
		})
		if err != nil {
			return o, err
		} else if n > 0 {
			if o == nil {
				o = make(map[string]int, 0)
			}
			o[nlri] = n
		}
	}
	return o, nil
}

// fileYMD returns an array of bytes of the binary file
// The file is exactly the filepath join of base, nli and date strings.
func (r *Recs) ReadFile(a, b string) ([]byte, error) {
	path := filepath.Join(r.base, a, b)
	return ioutil.ReadFile(path)
}

func (r *Recs) Files(file func(name string, size int64)) error {
	return filepath.Walk(r.base, func(name string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			name = strings.TrimPrefix(name, r.base)
			if file != nil {
				file(name, info.Size())
			}
		}
		return err
	})
}

func (r *Recs) Zip(dir string, remove bool, log base.Log) error {

	folder := filepath.Join(r.base, dir)
	files, err := ioutil.ReadDir(folder)
	if err != nil {
		return err
	}
	if len(files) == 0 {
		return fmt.Errorf("folder without files")
	}

	out, err := os.Create(filepath.Join(r.base, dir + ".zip"))
	if err != nil {
		return err
	}
	defer out.Close()
	w := zip.NewWriter(out)
	added := make(map[string]int, 0)
	m := 0
	ze := 0
	for _, file := range files {
		if !file.IsDir() {
			bytes, err := ioutil.ReadFile(filepath.Join(folder, file.Name()))
			if err != nil {
				ze++
				if log != nil {
					log("%sZIP read file err:%v%s", base.Red, err, base.Reset)
				}
				continue
			}
			f, err := w.Create(file.Name())
			if err != nil {
				ze++
				if log != nil {
					log("%sZIP create file err:%v%s", base.Red, err, base.Reset)
				}
				continue
			}
			n, err := f.Write(bytes)
			if err != nil {
				ze++
				if log != nil {
					log("%sZIP create file err:%v%s", base.Red, err, base.Reset)
				}
				continue
			}
			added[file.Name()] = n
			m += n
		}
	}
	// TODO add to zip metadata file
	if err := w.Close(); err != nil {
		return err
	}
	if log != nil {
		log("%sZIP file:%s.zip files:%d bytes:%d added:%v%s", base.Blue, dir, len(files), m, added, base.Reset)
	}
	if remove {
		if err := os.RemoveAll(folder); err != nil {
			return err
		}
		if log != nil {
			log("%sZIP folder:%s removed%s", base.Blue, dir, base.Reset)
		}
	}

	return nil

}

// Folder returns the folder for JSON recorder health.
// Root is kept secret for publication.
func (r *Recs) Folder() string {
	return r.folder
}

// gets returns the Files associated with given nlri path
// Creates a new map element when files does not exist.
func (r *Recs) get(record *Record) *Files {
	nlri := record.NLRI()
	if f, ok := r.nlris[nlri]; ok {
		return f
	} else {
		f = newFiles()
		r.nlris[nlri] = f
		return r.nlris[nlri]
	}
}

func (r *Recs) appendFile(filename string, rows []byte) (int, error) {
	dir := filepath.Dir(filename)
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		// make parent which does not exists
		if err := os.MkdirAll(dir, r.mode); err != nil {
			return 0, err
		}
	}
	const o = os.O_APPEND | os.O_WRONLY | os.O_CREATE
	if file, err := os.OpenFile(filename, o, r.mode); err != nil {
		// file cannot be openned
		return 0, err
	} else if w, err := file.Write(rows); err != nil {
		// file cannot be written
		file.Close()
		return w, err
	} else if err := file.Close(); err != nil {
		// file cannot be closed
		return w, err
	} else {
		return w, nil
	}
}


// Files is a map of filepaths where to save a byte array
type Files struct {
	rows map[string][]byte
}

func newFiles() *Files {
	return &Files{
		rows: make(map[string][]byte),
	}
}

func (f *Files) clear() {
	for elem, _ := range f.rows {
		delete(f.rows, elem)
	}
}

func (f *Files) append(r *Record) {
	date := r.Date()
	if f.rows[date] == nil {
		f.rows[date] = make([]byte, 0)
	}
	f.rows[date] = append(f.rows[date], r.row...)
}

func (f *Files) save(save RecsSaveFunc) (int, error) {
	defer f.clear()
	n := 0
	for date, rows := range f.rows {
		if w, err := save(date, rows); err != nil {
			return n, err
		} else {
			n += w
		}
	}
	return n, nil
}



