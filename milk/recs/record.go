package recs

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/line"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
	"gitlab.com/jmireles/cans-base/milk/to"
)

var (
	InvalidLine  = fmt.Errorf("Invalid line")
	InvalidComm  = fmt.Errorf("Invalid command")
	InvalidExtra = fmt.Errorf("Invalid extra")
)

type Rec byte

const (
	Box   Rec = 'b' // id is a box
	State Rec = 's' // id is a stall
	Value Rec = 'v' // id is a stall
	Zone  Rec = 'z' // id is a box
)

var hex_ = []byte{
	'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f',
}

type Record struct {
	nlri []byte
	date []byte
	row  []byte
}

func (r *Record) NLRI() string {
	return string(r.nlri)
}

func (r *Record) Date() string {
	return string(r.date)	
}

func (r *Record) Row() []byte {
	return r.row
}

func (r *Record) RowTime() (h, m, s, c byte) {
	return RowTime(r.row)
}

func (r *Record) RowDebug() string {
	h, m, s, c := r.RowTime()
	return fmt.Sprintf("%02d:%02d:%02d.%02d %02x", h, m, s, c, r.row[3:])
}

func RowTime(row []byte) (h, m, s, c byte) {
	// CCCCCCCCDDDDDDDDEEEEEEEE
	// hhhhhmmmmmmssssssccccccc
	// 10111                    = 23 hrs
	//      111011              = 59 min
	//            111011        = 59 sec
	//                  1100011 = 99 sec/100
	hmsc := uint(row[0])<<16 + uint(row[1])<<8 + uint(row[2])
	h = byte(hmsc >> 19) & 0x1F
	m = byte(hmsc >> 13) & 0x3F
	s = byte(hmsc >> 7)  & 0x3F
	c = byte(hmsc)       & 0x7F
	return
}

func RowComm(row []byte) byte {
	return row[3]
}

func RowExtra(row []byte) []byte {
	return row[4:]
}

// AAAAAAAABBBBBBBB
// yyyyyyymmmmddddd
// 1100011          = 99
//        1100      = 12
//            11111 = 31

func NewRecordBox8(box, comm byte, extra []byte, utc base.UTC) *Record {
	r := newRecord(0, 0, utc)
	r.box8(box, comm , extra)
	return r
}

func NewRecord(net byte, bytes []byte, utc base.UTC) (*Record, error) {
	if msg, err := line.NewMsg(net, bytes, utc); err != nil {
		return nil, err
	} else {
		comm  := msg.Data.Comm()
		extra := msg.Data.Extra()
		if line, err := msg.From(); err == nil {
			return NewRecordFrom(net, line, msg.Data.Src(), comm, extra, utc)
		}
		if line, err := msg.To(); err == nil {
			return NewRecordTo(net, line, msg.Data.Dst(), comm, extra, utc)
		}
		return nil, InvalidLine
	}
}

func NewRecordMsg(msg *line.Msg) (*Record, error) {
	net := byte(msg.Net)
	data := msg.Data
	if line, err := msg.From(); err == nil {
		return NewRecordFrom(net, line, data.Src(), data.Comm(), data.Extra(), msg.UTC())
	}
	if line, err := msg.To(); err == nil {
		return NewRecordTo(net, line, data.Dst(), data.Comm(), data.Extra(), msg.UTC())
	}
	return nil, InvalidLine
}

func NewRecordResp(r from.Resp) (*Record, error) {
	if from, err := r.From(); err != nil {
		return nil, err
	} else if comm, _, _, err := r.Comm(); err != nil {
		return nil, err 
	} else {
		return NewRecordFrom(byte(r.NetId()), from, r.BoxId(), comm, r.Extra(), r.UTC())
	}
}

func NewRecordFrom(net byte, line from.From, src, comm byte, extra []byte, utc base.UTC) (r *Record, e error) {
	r = newRecord(net, byte(line), utc)
	switch line {
	
	case from.P1:
		e = r.fromP1(src, comm, extra)
	
	case from.RS:
		e = r.fromRS(src, comm, extra)
	
	case from.T4:
		e = r.fromT4(src, comm, extra)
	
	case from.I4:
		e = r.fromI4(src, comm, extra)

	default:
		e = InvalidLine
	}
	return
}

func NewRecordTo(net byte, line to.To, dst, comm byte, extra []byte, utc base.UTC) (r *Record, e error) {
	r = newRecord(net, byte(line), utc)
	switch line {

	case to.P1:
		e = r.toP1(dst, comm, extra)

	case to.RS:
		e = r.toRS(dst, comm, extra)

	case to.T4:
		e = r.toT4(dst, comm, extra)

	case to.I4:
		e = r.toI4(dst, comm, extra)

	default:
		e = InvalidLine
	}
	return
}

func newRecord(net, line byte, utc base.UTC) *Record {
	hmsc := uint(utc[3] % 24) << 19 // 0-23 hrs
	hmsc += uint(utc[4] % 60) << 13 // 0-59 minutes
	hmsc += uint(utc[5] % 60) << 7  // 0-59 seconds
	hmsc += uint(utc[6] % 100)      // 0-99 seconds/100

	return &Record{
		nlri: []byte{
			// First four chars: nllrii
			hex_[net & 0x0F],  // net
			hex_[line >> 4],   // line*16
			hex_[line & 0x0F], // line units
			0,                 // will update with set(rec)
			0,                 // will update with set(id>>4)
			0,                 // will update with set(id&0xF)
		},
		date: []byte{
			// Last seven chars: /yymmdd
			hex_[utc[0] / 10], // year-decade
			hex_[utc[0] % 10], // year-unit
			hex_[utc[1] / 10], // month-decade
			hex_[utc[1] % 10], // month-unit
			hex_[utc[2] / 10], // day-decade
			hex_[utc[2] % 10], // day-unit
		},
		row: append([]byte{
			byte(hmsc >> 16),
			byte(hmsc >> 8),
			byte(hmsc),
			0,                // will update with set(comm)
		}),
	}
}

func (r *Record) set(rec Rec, id byte, comm byte, extra []byte) {
	r.nlri[3] = byte(rec)
	r.nlri[4] =	hex_[id >> 4]   // id*16
	r.nlri[5] = hex_[id & 0x0F] // id-unit
	r.row[3] = comm
	r.row = append(r.row, extra...)
}

func (r *Record) box8(box, comm byte, extra []byte) error {
	e := make([]byte, 8)
	copy(e, extra)
	r.set(Box, box, comm, e)
	return nil
}

func (r *Record) state2(stall, comm byte, extra []byte) error {
	e := make([]byte, 2)
	copy(e, extra)
	r.set(State, stall, comm, e)
	return nil
}

func (r *Record) value8_T4(src, comm byte, extra []byte) error {
	if len(extra) < 2 {
		// Don't have e[0] which is the stall nor e[1] the minimum payload
		return InvalidExtra
	}
	e := make([]byte, 9)
	copy(e, extra)
	e[8] = src // set box in last e
	r.set(Value, e[0], comm, e[1:])
	return nil
}

func (r *Record) value8(stall, comm byte, extra []byte) error {
	e := make([]byte, 8)
	copy(e, extra)
	r.set(Value, stall, comm, e)
	return nil
}

func (r *Record) zone4(src byte, extra []byte) error {
	if len(extra) < 2 {
		// missed extra[0]=input and extra[1] where zone is: zzzzffff
		return InvalidExtra
	}
	input := byte(extra[0])
	zone := byte(extra[1] >> 4)
	e := make([]byte, 4)
	copy(e, extra[1:])
	e[3] = src // set box in last 
	r.set(Zone, zone, input, e)
	return nil
}

func (r *Record) state2_RS(src, comm byte, extra []byte) error {
	if len(extra) < 2 {
		// missed extra[0]=stall, extra[1]=reader
		return InvalidExtra
	}
	r.set(State, extra[0], comm, []byte{ extra[1], src })
	return nil
}

func (r *Record) fromP1(src, comm byte, extra []byte) error {
	switch comm {
	// responses as stall State2:
	case byte(milk.Unsol01_Restart), // in PM restart is for stall state
		byte(p1.Unsol05_Idle),
		byte(p1.Unsol06_Working),
		byte(p1.Unsol07_Alarmed),
		byte(p1.Unsol16_NoPhaseFront),
		byte(p1.Unsol17_NoPhaseRear),
		byte(p1.Unsol20_Stimulation):
		return r.state2(src, comm, extra)

	// responses as stall Value8:
	case byte(p1.Sol04_ValCurves),
		byte(p1.Sol08_AvgPhaseFront),  
		byte(p1.Sol09_AvgPhaseRear),
		byte(p1.Sol10_AvgLevelFront),  
		byte(p1.Sol11_AvgLevelRear),
		byte(p1.Sol12_ValPhasesFront), 
		byte(p1.Sol13_ValPhasesRear),
		byte(p1.Sol14_ValLevelsFront), 
		byte(p1.Sol15_ValLevelsRear),
		byte(p1.Unsol26_Notify):
		return r.value8(src, comm, extra)
	
	// responses as Box8:
	case byte(milk.Sol00_Version),
		byte(milk.Sol02_Status),
		byte(p1.Sol03_Config),
		byte(p1.Sol18_Calibr),
		byte(p1.Sol21_Options),
		byte(p1.Sol22_Profiles),
		byte(p1.Sol23_Display),
		byte(p1.Sol24_Config),
		byte(p1.Sol25_Alarms),
		byte(p1.Sol27_Stalls):
		return r.box8(src, comm, extra)

	default:
		return InvalidComm
	}
}

func (r *Record) fromRS(src, comm byte, extra []byte) error {
	switch comm {

	// responses as stall State2
	case byte(rs.Unsol06_Stall):
		return r.state2_RS(src, comm, extra)

	// responses as Box8:
	case byte(milk.Sol00_Version),
		byte(milk.Unsol01_Restart), // in RS restart is for box
		byte(milk.Sol02_Status),
		byte(rs.Sol03_Config):
		return r.box8(src, comm, extra)

	default:
		return InvalidComm
	}
}

func (r *Record) fromT4(src, comm byte, extra []byte) error {
	switch comm {
	// responses as stall State8
	case byte(t4.Unsol09_Stage),
		byte(t4.Unsol10_Mode),
 		byte(t4.Unsol13_Levels),
 		byte(t4.Unsol14_Colors):
 		return r.value8_T4(src, comm, extra)

	// responses as Box8:
	case byte(milk.Sol00_Version),
		byte(milk.Unsol01_Restart), // in T4 restart is for box
		byte(milk.Sol02_Status),
		byte(t4.Sol12_Levels),
		byte(t4.Sol03_Stalls),
		byte(t4.Sol04_Equipment),
		byte(t4.Sol05_Milking),
		byte(t4.Sol06_Profiles),
		byte(t4.Sol07_Colors),
		byte(t4.Sol08_Palette),
		byte(t4.Sol18_Rotary):
		r.box8(src, comm, extra)
		return nil

	default:
		return InvalidComm
	}
}

func (r *Record) fromI4(src, comm byte, extra []byte) error {
	switch comm {

	// responses as Zone8:
	case byte(i4.Unsol05_Change):
		// src and comm (the box and the 5 are discarded from rows)
		return r.zone4(src, extra)

	// responses as Box8: 
	case byte(milk.Sol00_Version),
		byte(milk.Unsol01_Restart), // In I4 restart is for box
		byte(milk.Sol02_Status),
		byte(i4.Sol03_Config):
		return r.box8(src, comm, extra)

	default:
		return InvalidComm
	}
}

func (r *Record) toP1(dst, comm byte, extra []byte) error {
	switch comm {

	// requests as Box8:
	case byte(milk.Get00_Version),
		byte(milk.Get01_Serial),
		byte(milk.Get02_Status),
		byte(p1.Get03_Config),
		byte(p1.Set04_Config),
		byte(p1.Get05_Values),
		byte(p1.Get06_Averages),
		byte(p1.Get07_Calibr),
		byte(p1.Set08_Calibr),
		byte(p1.Get09_Options),
		byte(p1.Set10_Options),
		byte(p1.Get11_Profiles),
		byte(p1.Set12_Profiles),
		byte(p1.Get13_Display),
		byte(p1.Set14_Display),
		byte(p1.Get15_Config),
		byte(p1.Set16_Config),
		byte(p1.Get17_Alarms),
		byte(p1.Set18_Alarms),
		byte(p1.Get19_Stalls),
		byte(p1.Set20_Stalls):
		return r.box8(dst, comm, extra)

	default:
		return InvalidComm
	}
}

func (r *Record) toRS(dst, comm byte, extra []byte) error {
	switch comm {

	// requests as Box8:
	case byte(milk.Get00_Version),
		byte(milk.Get01_Serial),
		byte(milk.Get02_Status),
		byte(rs.Get03_Config),
		byte(rs.Set04_Config):
		return r.box8(dst, comm, extra)
	
	default:
		return InvalidComm
	}
}

func (r *Record) toT4(dst, comm byte, extra []byte) error {
	switch comm {

	// requests as Box8:
	case byte(milk.Get00_Version),
		byte(milk.Get01_Serial),
		byte(milk.Get02_Status),
		byte(t4.Get03_Stalls),
		byte(t4.Get04_Equipment),
		byte(t4.Get05_Milking),
		byte(t4.Get06_Profiles),
		byte(t4.Get07_Colors),
		byte(t4.Get08_Palette),
		byte(t4.Set09_Stalls),
		byte(t4.Set10_Equipment),
		byte(t4.Set11_Milking),
		byte(t4.Set12_Profiles),
		byte(t4.Set13_Colors),
		byte(t4.Set14_Pallete),
		byte(t4.Get15_Levels),
		byte(t4.Get16_Rotary),
		byte(t4.Set17_Rotary):
		return r.box8(dst, comm, extra)
	
	default:
		return InvalidComm
	}
}

func (r *Record) toI4(dst, comm byte, extra []byte) error {
	switch comm {

	// requests as Box8:
	case byte(milk.Get00_Version),
		byte(milk.Get01_Serial),
		byte(milk.Get02_Status),
		byte(i4.Get03_Config),
		byte(i4.Set04_Config):
		return r.box8(dst, comm, extra)	
	
	default:
		return InvalidComm
	}
}



