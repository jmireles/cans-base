package from

import (
	"fmt"

	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
)

type From byte

const (
	PC From = 0x00
	P1 From = 0x10
	TO From = 0x20
	MM From = 0x30
	RF From = 0x40
	SO From = 0x50
	RS From = 0x60
	T4 From = 0x70
	I4 From = 0x80
)

func New(s string) (From, error) {
	switch s {
	case milk.PC:
		return PC, nil
	case milk.P1:
		return P1, nil
	case milk.RS:
		return RS, nil
	case milk.T4:
		return T4, nil
	case milk.I4:
		return I4, nil
	default:
		return From(0xF0), fmt.Errorf("Invalid string:%s", s)
	}
}

func NewFrom(b byte) (From, error) {
	switch b {
	case 0x00:
		return PC, nil
	case 0x10:
		return P1, nil
	case 0x60:
		return RS, nil
	case 0x70:
		return T4, nil
	case 0x80:
		return I4, nil
	default:
		return From(0xF0), fmt.Errorf("From: Invalid byte:%d", b)
	}
}

func (from From) RespBytes(src byte, comm byte, extra []byte) []byte {
	data := make([]byte, 4+len(extra))
	data[0] = byte(from) // LLLL0000
	data[1] = byte(0)    // destination always 0 (this computer)
	data[2] = src        // source (0,1,2...) 0=broadcast
	data[3] = comm
	copy(data[4:], extra) // command + data
	return data
}

func (from From) RespSolBytes(src byte, comm milk.Sol, extra []byte) []byte {
	return from.RespBytes(src, byte(comm), extra)
}

func (from From) RespUnsolBytes(src byte, comm milk.Unsol, extra []byte) []byte {
	return from.RespBytes(src, byte(comm), extra)
}

func (from From) Name() (name string, err error) {
	switch from {
	case PC:
		name = milk.PC
	case P1:
		name = milk.P1
	case TO:
		name = milk.TO
	case MM:
		name = milk.MM
	case RF:
		name = milk.RF
	case SO:
		name = milk.SO
	case RS:
		name = milk.RS
	case T4:
		name = milk.T4
	case I4:
		name = milk.I4
	default:
		err = fmt.Errorf("Invalid name [%d]", from)
	}
	return 
}

func (from From) Resp(comm byte) (*milk.Sol, *milk.Unsol) {
	switch from {
	case PC:
		sol := milk.Sol(comm)
		return &sol, nil
	case P1:
		return p1.Resp(comm)
	case RS:
		return rs.Resp(comm)
	case T4:
		return t4.Resp(comm)
	case I4:
		return i4.Resp(comm)
	default:
		return nil, nil
	}
}

func (from From) Payload(comm byte) (int, error) {
	switch from {

	case P1:
		return p1.PayloadFrom(comm)
	//case RS:
	//	return rs.PayloadFrom(comm)
	//case T4:
	//	return t4.PayloadFrom(comm)
	//case I4:
	//	return i4.PayloadFrom(comm)
	default:
		return 0, fmt.Errorf("Invalid line:%d", from)
	}
}

func (from From) SolSim(src byte, sol milk.Sol, extra []byte) []byte {
	pre := []byte{
		byte(from), // [0] fromline
		byte(0),    // dst always zero (this computer)
		src,        // box source id
		byte(sol),  // version command
	}
	if len(extra) == 0 {
		return pre
	} else {
		return append(pre, extra...)
	}
}

// RespSolBoxIdSim returns the bytes a real device needs to
// so this method is used by simulators:
//      0         1         2       3      4       5         6         7         8         9
// ┌─────────┬─────────┬─────────┬────┬───────┬─────────┬─────────┬─────────┬─────────┬──────────┐
// │YYYM-MMMD DDDD-SSSS CCCC-CCCC  31  subcomm 0000-LLLL YYYM-MMMD DDDD-SSSS CCCC-CCCC IIII-IIIII│
// └─────────┴─────────┴─────────┴────┴───────┴─────────┴─────────┴─────────┴─────────┴──────────┘
func (from From) SolSimSerial(src byte, s []byte) []byte {
	return []byte{
		s[0], s[1], s[2],        // [0][1][2] serial number
		byte(milk.Sol31_Serial), // [3]       command = 31
		byte(milk.Serial00_Get), // [4]       subcommand = 0
		byte(from >> 4),         // [5]       0000LLLL
		s[0], s[1], s[2],        // [6][7][8] serial number (again)
		src, // [9]
	}
}





