package from

import (
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/to"
)

type SolsFunc func(*to.Can) (*Sols, error)

type Sols struct {
	Comm  byte
	Req   *to.Can
	resps map[milk.Sol][][]byte
}

func NewSols(req *to.Can) (*Sols, error) {
	if comm, err := req.Comm(); err != nil {
		return nil, err
	} else {
		return &Sols{
			Comm:  comm,
			Req:   req,
			resps: make(map[milk.Sol][][]byte, 0),
		}, nil
	}
}

func (s *Sols) Add(k milk.Sol, v [][]byte) {
	s.resps[k] = v
}

// Data is the set of responses messages expected as
// solicited responses
func (s *Sols) Data() ([]milk.Data, error) {
	line, err := NewFrom(byte(s.Req.Line) << 4)
	if err != nil {
		return nil, err
	}
	box := s.Req.Box
	resps := make([]milk.Data, 0)
	for sol, extras := range s.resps {
		for _, extra := range extras {
			d := line.RespSolBytes(box, sol, extra)
			if data, err := milk.NewData(d); err != nil {
				return nil, err
			} else {
				resps = append(resps, data)
			}
		}
	}
	return resps, nil
}

