package from

import (
	"errors"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

// A Serial is a response where command is serial type.
// implements Resp.NetId interface
type Serial struct {
	base.Net
	milk.Data
	utc base.UTC
}

//      0         1         2       3      4         5         6         7         8         9
// ┌─────────┬─────────┬─────────┬────┬─────────┬─────────┬─────────┬─────────┬─────────┬──────────┐
// │YYYM-MMMD DDDD-SSSS CCCC-CCCC  31  0000-0000 0000-LLLL YYYM-MMMD DDDD-SSSS CCCC-CCCC IIII-IIIII│
// └─────────┴─────────┴─────────┴────┴─────────┴─────────┴─────────┴─────────┴─────────┴──────────┘
func NewSerial(net base.Net, data milk.Data, utc base.UTC) (*Serial, error) {
	if len(data) < 5 {
		return nil, errors.New("data length < 5")
	}
	switch data[4] { // subcommand

	case 0:
		if len(data) < 10 {
			return nil, errors.New("serial sub=0 resp length < 10")
		}
		if data[0] != data[6] || data[1] != data[7] || data[2] != data[8] {
			return nil, errors.New("serial sub=0 serial mismatch")
		}
		return &Serial{
			Net:  net,
			Data: data,
			utc:  utc,
		}, nil

	default:
		return nil, errors.New("subcommand != 0")
	}
}

func (s Serial) Clone() Serial {
	return Serial {
		Net:  s.Net,
		Data: s.Data.Clone(),
		utc:  s.utc.Clone(),
	}
}

// From implements Resp interface
func (s Serial) From() (From, error) {
	// convert to-line to from-line
	// this adjustment is needed since serial response
	// receives 0000LLLL line instead LLLL0000
	return NewFrom(s.Data[5] << 4)
}

// BoxId implements Resp interface
func (s Serial) BoxId() byte {
	return s.Data[9]
}

// Comm implements Resp interface
func (s Serial) Comm() (byte, *milk.Sol, *milk.Unsol, error) {
	b := byte(milk.Sol31_Serial) // only accepted at constructor
	sol := milk.Sol(b)
	return b, &sol, nil, nil
}

// Extra implements Resp interface
func (s *Serial) Extra() []byte {
	return s.Data[4:]
}

// UTC implements Resp interface
func (s *Serial) UTC() base.UTC {
	return s.utc
}

// Serial returns the three bytes which identify a box among other boxes
// without checking line ids.
func (s Serial) Serial() []byte {
	return s.Data[0:3]
}



