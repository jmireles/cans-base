package from

import (
	"context"
	"testing"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

func TestTcpC(t *testing.T) {
	tt := base.Test{t}
	// All pms responses
	pmsAll := pm7_123
	pmsAll = append(pmsAll, pm7_3s...) 
	pmsAll = append(pmsAll, pm7_4s...)

	pms3masks := []milk.Data {
		P1.RespBytes(7, 3, []byte{ 1 }),
		P1.RespBytes(7, 3, []byte{ 2 }),
		P1.RespBytes(7, 3, []byte{ 3 }),
		P1.RespBytes(7, 3, []byte{ 4 }),
	}

	t.Run("FourResps3s", func(t *testing.T) {
		// four masks cmd=3, ext={1,2,3,4} ...
		c, _ := NewTcpC(pms3masks)
		// ... will be PRESENT in pmsAll responses
		for _, data := range pmsAll {
			c.response(data)
		}
		// verify the four masks were consumed
		tt.Equals(0, len(c.Masks))
		// verify all four resps arrived
		tt.Equals(len(pm7_3s), len(c.Resps))
		for pos, exp := range pm7_3s {
			tt.Equals(exp, c.Resps[pos])
		}
	})

	t.Run("TwoResps3s", func(t *testing.T) {
		// two masks cmd=3, ext={1,2} ...
		c, _ := NewTcpC(pms3masks[:2])
		// ... will be PRESENT in pmsAll responses
		for _, data := range pmsAll {
			c.response(data)
		}
		// verify the two masks were consumed
		tt.Equals(0, len(c.Masks))
		// verify only 2 responses arrived
		tt.Equals(2, len(c.Resps))
		for pos, exp := range pm7_3s[:2] {
			tt.Equals(exp, c.Resps[pos])
		}
	})

	t.Run("NoResps3s", func(t *testing.T) {
		// three masks ext={1,2,3} ...
		masks := pms3masks[:3]
		c, _ := NewTcpC(masks)
		// ... will be NOT present in partial responses
		partial := pm7_123
		partial = append(partial, pm7_4s...)
		for _, data := range partial {
			c.response(data)
		}
		// verify no mask was consumed
		tt.Equals(len(masks), len(c.Masks))
		for pos, exp := range masks {
			tt.Equals(exp, c.Masks[pos])
		}
		// verify no response arrive
		tt.Equals(0, len(c.Resps))
	})
}

func TestTcpSrv(t *testing.T) {
	testTcpSrv(t, nil) // without log
	testTcpSrv(t, base.LogF("tcps", base.Blue)) // with log
}

func testTcpSrv(t *testing.T, logF base.Log) {
	tt := &base.Test{t}
	tcps, err := NewTcpSrv(":0", logF)
	tt.Ok(err)

	net := byte(3)

	// client 1
	c1, _ := NewTcpC([]milk.Data {
		// masks for collect four possible responses comm=3
		P1.RespBytes(7, 3, []byte{ 1 }),
		P1.RespBytes(7, 3, []byte{ 2 }),
		P1.RespBytes(7, 3, []byte{ 3 }),
		P1.RespBytes(7, 3, []byte{ 4 }),
	})
	conn1, r1, err := c1.Read(tcps.listener.Addr().String(), net)
	tt.Ok(err)
	defer conn1.Close()

	// The request expects responses (like get cfg).
	// Do the post and return http-response with cans-data or timeout.
	ctx, cancel := context.WithTimeout(context.Background(), 2 * time.Second)
	defer cancel()
	// Collect responses
	go func() {
		for {	
			select {
			case result := <-r1:
				// whole mask list matched, return before timeout
				t.Logf("%v", result)
			case <-ctx.Done():
				// timeout elapsed
				t.Log("Timeout")
				return
			}
		}
	}()

	resps := pm7_123
	resps = append(resps, pm7_3s...)
	resps = append(resps, pm7_4s...)
	for _, data := range resps {
		if err := tcps.Write(net, data); err != nil {
			//return err
		}
		time.Sleep(33 * time.Millisecond)
	}
}

var pm7_123 = []milk.Data {
	milk.Data(P1.RespBytes(7, 0, []byte{ 44 })),
	milk.Data(P1.RespBytes(7, 1, []byte{ 0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18 })),
	milk.Data(P1.RespBytes(7, 2, []byte{ 0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28 })),
}
var pm7_3s = []milk.Data {
	milk.Data(P1.RespBytes(7, 3, []byte{ 1,11,12,13,14,15,16,17 })),
	milk.Data(P1.RespBytes(7, 3, []byte{ 2,21,22,23,24,25,26,27 })),
	milk.Data(P1.RespBytes(7, 3, []byte{ 3,31,32,33,34,35,36,37 })),
	milk.Data(P1.RespBytes(7, 3, []byte{ 4,41,42,43,44,45,46,47 })),
}
var pm7_4s = []milk.Data {
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
	milk.Data(P1.RespBytes(7, 4, []byte{ 1,2,3,4,5,6,7,8 })),
}

