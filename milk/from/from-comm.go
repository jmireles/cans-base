package from

import(
	"errors"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/rs"
	"gitlab.com/jmireles/cans-base/milk/t4"
)

// A Comm is a response where command is not serial type.
type Comm struct {
	base.Net
	milk.Data
	from From
	utc base.UTC
}

//      0         1         2       3    4    5    6    7    8    9   10   11
// ┌─────────┬─────────┬─────────┬────┬────┬────┬────┬────┬────┬────┬────┬────┐
// │LLLL 0000     0         S     comm                                        │
// └─────────┴─────────┴─────────┴────┴────┴────┴────┴────┴────┴────┴────┴────┘
func NewComm(net base.Net, data milk.Data, utc base.UTC) (*Comm, error) {
	// TODO check data[0] is valid FROM
	if from, err := NewFrom(data[0]); err != nil {
		return nil, err
	} else if dst := data[1]; dst != 0 { // destination (should be zero)
		return nil, errors.New("command dst is not zero")
	} else {
		return &Comm{
			Net:  net,
			Data: data,
			from: from,
			utc:  utc,
		}, nil
	}
}

func (r Comm) Clone() Comm {
	return Comm{
		Net:  r.Net,
		Data: r.Data.Clone(),
		from: r.from,
		utc:  r.utc.Clone(),
	}
}

// From implements from.Resp interface
func (r Comm) From() (From, error) {
	return r.from, nil
	//return NewFrom(r.Data[0])
}

// BoxId implements from.Resp interface
func (r Comm) BoxId() byte {
	return r.Data[2]
}

// Comm return the command, pointers to sol/unsol type or error.
// Implements from.Resp interface
func (r Comm) Comm() (comm byte, sol *milk.Sol, unsol *milk.Unsol, err error) {
	comm = r.Data[3]
	switch r.from {
	case P1:
		sol, unsol = p1.Resp(comm)
	case RS:
		sol, unsol = rs.Resp(comm)
	case T4:
		sol, unsol = t4.Resp(comm)
	case I4:
		sol, unsol = i4.Resp(comm)
	default:
		err = errors.New("Invalid from")		
	}
	return
}

// Extra Implements from.Resp interface
func (c *Comm) Extra() []byte {
	return c.Data[4:]
}

// UTC implements from.Resp interface
func (c *Comm) UTC() base.UTC {
	return c.utc
}
