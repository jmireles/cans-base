package from

import (
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

// Src is a source with net, line from and box used for responses
type Src struct {
	Net  base.Net
	Line From
	Box  byte
}

// NewSrc creates a Src with given net, line and box
func NewSrc(net byte, line From, box byte) (*Src, error) {
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else {
		return &Src{Net:net, Line:line, Box:box}, nil
	}
}

func (s *Src) Key() string {
	return base.BoxKey(s.Net, byte(s.Line), s.Box)
}

type Can struct {
	*Src
	Sol   *milk.Sol
	Unsol *milk.Unsol
	Extra []byte
}

func NewCan(src *Src, comm byte, extra []byte) (*Can, error) {
	if err := base.ExtraValid(extra); err != nil {
		return nil, err
	} else if sol, unsol := src.Line.Resp(comm); sol == nil && unsol == nil {
		return nil, fmt.Errorf("Invalid line:%d, comm:%d", src.Line, comm)
	} else {
		return &Can{
			Src:   src,
			Sol:   sol,
			Unsol: unsol,
			Extra: extra,
		}, nil
	}
}

func NewCanSol(src *Src, sol milk.Sol, extra []byte) (*Can, error) {
	return NewCan(src, byte(sol), extra)
}

func NewCanUnsol(src *Src, unsol milk.Unsol, extra []byte) (*Can, error) {
	return NewCan(src, byte(unsol), extra)
}


