package from

import (
	"fmt"
	"net"
	"strings"
	"sync"
	"time"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

// A TcpSrv serves TCP connections (clients) which receive
// Can responses bytes. Is the equivalent used by Cans microservice
// and this one is used to simulate Cans responses from a Cans simulator.
type TcpSrv struct {
	listener net.Listener
	conns    []net.Conn
	logF     base.Log
	mu       sync.Mutex
	tx       int
}

// NewTcpSrv creates a TCP server with given listener TCP port and optional logger.
// port examples: ":0", ":8082"
func NewTcpSrv(port string, logF base.Log) (*TcpSrv, error) {
	if listener, err := net.Listen("tcp", port); err != nil {
		return nil, err
	} else {
		s := &TcpSrv{
			listener: listener,
			conns:    make([]net.Conn, 0),
			logF:     logF,
		}
		go s.accept()
		return s, nil
	}
}

// Response implements from.Reader
// Sends the response net and byte as exactly 13 bytes to all current TCP clients.
func (s *TcpSrv) Response(resp Resp) error {
	if data, err := milk.NewData(resp.Bytes()); err != nil {
		return err
	} else {
		s.mu.Lock()
		defer s.mu.Unlock()
		return s.write(byte(resp.NetId()), data)
	}
}

// Write sends the message as exactly 13 bytes to all current TCP clients.
func (s *TcpSrv) Write(net byte, data milk.Data) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.write(net, data)
}

func (s *TcpSrv) Close(by string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.listener != nil {
		if err := s.listener.Close(); err != nil {
			s.log("closed by %s error %v", by, err)
		} else {
			s.log("closed by %s", by)
		}
	}
}

// Move it into TcpSrv
func (s *TcpSrv) Json(sb *strings.Builder) {
	s.mu.Lock()
	defer s.mu.Unlock()
	sb.WriteString(`{`)
	sb.WriteString(`"addr":"` + s.listener.Addr().String() + `"`)
	sb.WriteString(`,"conns":[`)
	for pos, conn := range s.conns {
		if pos > 0 {
			sb.WriteString(`,`)
		}
		if conn != nil {
			sb.WriteString("true")
		} else {
			sb.WriteString("false")
		}
	}
	sb.WriteString(`]`)
	sb.WriteString(`}`)
}

func (s *TcpSrv) log(debug string, args ...interface{}) {
	if s.logF != nil {
		s.logF(debug, args...)
	}
}

func (s *TcpSrv) write(net byte, data milk.Data) error {
	resp := make([]byte, 13)
	resp[0] = net
	copy(resp[1:], []byte(data))
	for _, conn := range s.conns {
		if conn != nil {
			if n, err := conn.Write(resp); err != nil {
				s.log("write error %v", err)
				return err
			} else {
				s.tx++
				s.log("write n=%d %02x tx=%d", n, resp, s.tx)
			}
		}
	}
	return nil
}

// accept new connection from reals/demo apps
func (s *TcpSrv) accept() {
	s.log("accepting at %s", s.listener.Addr())
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			s.log("accept error=%v", err)
			break
		}
		s.mu.Lock()
		s.conns = append(s.conns, conn)
		name := conn.RemoteAddr()
		s.log("accept client %v clients=%d", name, len(s.conns))
		s.mu.Unlock()
		go s.supervise(conn)
	}
	s.Close("accept")
}

func (s *TcpSrv) supervise(conn net.Conn) {
	b := make([]byte, 13)
	for {
		// use read to detect client TCP close
		_, err := conn.Read(b)
		if err != nil {
			break
		}
	}
	s.mu.Lock()
	name := conn.RemoteAddr()
	conn.Close()
	for i, c := range s.conns {
		if c == conn {
			s.conns = append(s.conns[:i], s.conns[i+1:]...)
		}
	}
	s.log("closed client %v clients=%d", name, len(s.conns))
	s.mu.Unlock()
}




type TcpClient struct {
	conn net.Conn
}

func NewTcpClient(addr string) (*TcpClient, error) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	return &TcpClient{ conn:conn }, nil
}

func (c *TcpClient) SetReadTimeout(timeout time.Duration) {
	c.conn.SetReadDeadline(time.Now().Add(timeout))
}

// connect to server's tcp service
func (c *TcpClient) Read(lastRx func([]byte) bool) chan error {
	done := make(chan error, 1)
	go func() {
		defer c.conn.Close()
		for {
			rx := make([]byte, 32)
			n, err := c.conn.Read(rx)
			if err != nil {
				// conn error or read timeout
				done<- err
				return
			}
			// send to listener tcp reception bytes
			if lastRx(rx[:n]) {
				// listener says all receptions were complete and ok
				done<- nil
				return
			}
		}
	}()
	return done
}















type TcpC struct {
	Masks []milk.Data
	Resps []milk.Data
	Error error
}

func NewTcpC(masks []milk.Data) (*TcpC, error) {
	_masks := make([]milk.Data, len(masks))
	for pos, mask := range masks {
		if len(mask) > 5 {
			return nil, fmt.Errorf("Mask length greater than 5")
		} else {
			_masks[pos] = mask
		}
	}
	return &TcpC {
		Masks: _masks,
		Resps: make([]milk.Data, 0),
	}, nil
}

func (r *TcpC) Read(addr string, net_ byte) (net.Conn, chan *TcpC, error) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, nil, err
	}
	// read TCP server responses
	// make room for responses received matching with masks

	done := make(chan *TcpC, 1)
	go func() {
		rx := make([]byte, 13)
		for {
			n, err := conn.Read(rx)
			if err != nil {
				r.Error = err
				done<- r
				return
			}
			// TODO check n==13???
			parts := rx[:n]
			if net_ != parts[0] {
				continue 
			}
			data := make([]byte, 12)
			copy(data, parts[1:])
			r.response(data)
			if len(r.Masks) == 0 {
				done<- r
				return
			}
		}
	}()
	return conn, done, nil
}

func (t *TcpC) response(data []byte) {
	for i, mask := range t.Masks {
		if line := mask[0]; line != data[0] {
			// mask's lines does not match data's
			continue
		}
		if dst := mask[1]; dst != 0 && dst != data[1] {
			// mask's dst is not broadcast (0) AND doest not match data's
			continue
		}
		if src := mask[2]; src != 0 && src != data[2] {
			// mask's src is not broadcast (0) AND doest not match data's
			continue
		}
		if comm := mask[3]; comm != data[3] {
			// mask's comm does not match data's
			continue
		}
		if len(mask) > 4 && mask[4] != data[4] {
			// mask's has an extra byte and does not match data's
			continue
		}
		// here line && dst && src && comm && extra == true
		
		// append response matched including extra bytes
		t.Resps = append(t.Resps, data)
		// delete from masks list matched complete (consume)
		t.Masks = append(t.Masks[:i], t.Masks[i+1:]...)
	}
}


// From
// https://forum.golangbridge.org/t/correct-shutdown-of-net-listener/8705/3
// https://go.dev/play/p/OqiSY2peFBN

type Tcp2 struct {
	listener net.Listener
	shutdown chan bool
	exited   chan bool
	Write    chan []byte
}

func NewTcp2(listener net.Listener) *Tcp2 {
	t := &Tcp2{
		listener: listener,
		shutdown: make(chan bool),
		exited:   make(chan bool),
		Write:    make(chan []byte),
	}
	go t.serve()
	return t
}

func (t *Tcp2) serve() {
	// one go-routine that Accept()s and gets a connection object. 
	// Just accept and pass them down to a channel.
	var handlers sync.WaitGroup
	readers := make([]chan []byte, 0)
	for {
		select {
		case write := <-t.Write:
			for _, reader := range readers {
				if reader != nil {
					reader<- write
				}
			}

		case <-t.shutdown:
			fmt.Println("Tcp2: Shutting down...")
			for _, reader := range readers {
				if reader != nil {
					close(reader)
				}
			}
			t.listener.Close()
			handlers.Wait()
			close(t.exited)
			return
		
		default:
			//fmt.Println("Listening for clients")
			//t.listener.SetDeadline(time.Now().Add(1e9))
			conn, err := t.listener.Accept()
			if err != nil {
				if opErr, ok := err.(*net.OpError); ok && opErr.Timeout() {
					continue
				}
				fmt.Println("Tcp2: Failed to accept connection:", err.Error())
			}
			reader := make(chan []byte, 1)
			readers = append(readers, reader)
			handlers.Add(1)
			go func() {
				fmt.Println("Tcp2: Accepted conne from", conn.RemoteAddr())
				for {
					if _, err := conn.Write(<-reader); err != nil {
						break
					}					
				}
				close(reader)
				conn.Close()
				handlers.Done()
			}()
		}
	}
}

func (t *Tcp2) Shutdown() {
	fmt.Println("Tcp2: Shutdown requested")
	// XXX: You cannot use the same channel in two directions.
	//      The order of operations on the channel is undefined.
	close(t.shutdown)
	<-t.exited
	fmt.Println("Tcp2: Shutdown successfully")
}

