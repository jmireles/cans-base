package from

import (
	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

type Reader interface {
	// Response receives CAN responses from equipment
	Response(Resp) error
}

type Rx interface {
	// Device receives a heartbeat
	RxHeartbeat()
	// Device receives a error like CRC
	RxError(err error)
	// Device receives a response
	RxResp(resp Resp) error
}

// A Resp is used to receive messages from CAN responses.
type Resp interface {
	
	// NetId returns the CAN net where this response arrived
	NetId() base.Net
	
	// Bytes returns the array to be send to realtime clients and to be saved.
	Bytes() []byte
	
	// From returns the from line of this response or error for invalid lines.
	From() (From, error)
	
	// BoxId returns the response source identifier box.
	BoxId() byte
	
	// Comm returns command and two pointers for solicited/unsolicited types
	Comm() (byte, *milk.Sol, *milk.Unsol, error)
	
	// Extra returns the data bytes removing lines, dst, src and command
	// can be null or a maximum of eight bytes in this CAN protocol
	Extra() []byte

	// UTC returns the time when response arrived in this computer.
	UTC() base.UTC
}

func NewResp(net byte, bytes []byte, utc base.UTC) (Resp, error) {
	if net, err := base.NewNet(net); err != nil {
		return nil, err
	} else if data, err := milk.NewData(bytes); err != nil {
		return nil, err
	} else {
		command := data[3]
		if command == byte(milk.Sol31_Serial) {
			return NewSerial(net, data, utc)
		} else {
			return NewComm(net, data, utc)
		}
	}
}

func SolSimSerialSet(s []byte, to, id byte, idNew byte) []byte {
	return nil
}

func RespParse(net byte, data []byte, utc base.UTC) (serial *Serial, comm *Comm, err error) {
	if net, err1 := base.NewNet(net); err1 != nil {
		err = err1
	} else if data, err1 := milk.NewData(data); err1 != nil {
		err = err1
	} else if command := data[3]; command == byte(milk.Sol31_Serial) {
		// serial 31
		serial, err = NewSerial(net, data, utc)
	} else {
		// command not 31
		comm, err = NewComm(net, data, utc)
	}
	return
}

