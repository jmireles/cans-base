package from

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
)

func TestClones(t *testing.T) {
	tt := base.Test{t}

	// clone comms
	nA, _ := base.NewNet(1)
	dA := milk.Data{0x10,0,7,1}
	uA := base.UTC{1,2,3,4,5,6}
	c, err := NewComm(nA, dA, uA)
	tt.Ok(err)
	cA := c.Clone() // first clone
	nB, _ := base.NewNet(1)
	dB := milk.Data{0x80,0,1,1,1}
	uB := base.UTC{7,8,9,10,11,12}
	c, err = NewComm(nB, dB, uB)
	tt.Ok(err)
	cB := c.Clone() // second clone
	c = nil
	tt.Assert(c==nil, "not nil")

	// clone boxes
	s1, s2, s3 := byte(0xab), byte(0xcd), byte(0xef)
	dBA := milk.Data{s1,s2,s3,31,0,1,s1,s2,s3,1}
	b, err := NewSerial(nA, dBA, uA)
	tt.Ok(err)
	bA := b.Clone() // first clone

	s4, s5, s6 := byte(0x12), byte(0x34), byte(0x56)
	dBB := milk.Data{s4,s5,s6,31,0,8,s4,s5,s6,2}
	b, err = NewSerial(nB, dBB, uB)
	tt.Ok(err)
	bB := b.Clone() // last clone
	b = nil
	tt.Assert(b==nil, "not nil")

	// comms
	tt.Equals(cA.Net, nA)
	tt.Equals(cA.Data, dA)
	tt.Equals(cA.utc, uA)
	fromA, err := cA.From()
	tt.Ok(err)
	tt.Equals(fromA, From(0x10))
	tt.Equals(cB.Net, nB)
	tt.Equals(cB.Data, dB)
	tt.Equals(cB.utc, uB)
	fromB, err := cB.From()
	tt.Ok(err)
	tt.Equals(fromB, From(0x80))
	// boxes
	tt.Equals(bA.Net, nA)
	tt.Equals(bA.Data, dBA)
	tt.Equals(bA.utc, uA)
	tt.Equals(bB.Net, nB)
	tt.Equals(bB.Data, dBB)
	tt.Equals(bB.utc, uB)
	
	//t.Log(bB)
}