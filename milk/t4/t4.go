package t4

import (
	"gitlab.com/jmireles/cans-base/milk"
)

// requests
const (
	Get03_Stalls    milk.Get = 3  // cfg 1
	Get04_Equipment milk.Get = 4  // cfg 2
	Get05_Milking   milk.Get = 5  // cfg 3
	Get06_Profiles  milk.Get = 6  // cfg 4
	Get07_Colors    milk.Get = 7  // cfg 5
	Get08_Palette   milk.Get = 8  // cfg 6
	Set09_Stalls    milk.Set = 9  // cfg 1
	Set10_Equipment milk.Set = 10 // cfg 2
	Set11_Milking   milk.Set = 11 // cfg 3
	Set12_Profiles  milk.Set = 12 // cfg 4
	Set13_Colors    milk.Set = 13 // cfg 5
	Set14_Pallete   milk.Set = 14 // cfg 6

	Get15_Levels    milk.Get = 15

	Get16_Rotary    milk.Get = 16 // cfg 7
	Set17_Rotary    milk.Set = 17 // cfg 7
)

// responses
const (
	Sol03_Stalls    milk.Sol   = 3  // cfg 1
	Sol04_Equipment milk.Sol   = 4  // cfg 2
	Sol05_Milking   milk.Sol   = 5  // cfg 3
	Sol06_Profiles  milk.Sol   = 6  // cfg 4
	Sol07_Colors    milk.Sol   = 7  // cfg 5
	Sol08_Palette   milk.Sol   = 8  // cfg 6
	
	Unsol09_Stage   milk.Unsol = 9
	Unsol10_Mode    milk.Unsol = 10
	Unsol11_LetDown milk.Unsol = 11
	Sol12_Levels    milk.Sol   = 12
	Unsol13_Levels  milk.Unsol = 13
	Unsol14_Colors  milk.Unsol = 14
	
	Sol18_Rotary    milk.Sol   = 18  // cfg 7
	
	Unsol19_Flow    milk.Unsol = 19
)

func Req(comm byte) (*milk.Get, *milk.Set) {
	// get
	get := milk.Get(comm)
	switch get {
	case milk.Get00_Version,
		milk.Get01_Serial,
		milk.Get02_Status,
		Get03_Stalls,
		Get04_Equipment,
		Get05_Milking,
		Get06_Profiles,
		Get07_Colors,
		Get08_Palette,
		Get15_Levels,
		Get16_Rotary:
		return &get, nil
	}
	// set
	set := milk.Set(comm)
	switch set {
	case Set09_Stalls,
		Set10_Equipment,
		Set11_Milking,
		Set12_Profiles,
		Set13_Colors,
		Set14_Pallete,
		Set17_Rotary:
		return nil, &set
	}
	return nil, nil
}

func Resp(comm byte) (*milk.Sol, *milk.Unsol) {
	for _, unsol := range unsols {
		if comm == byte(unsol) {
			return nil, &unsol
		}
	}
	for _, sol := range sols {
		if comm == byte(sol) {
			return &sol, nil
		}
	}
	return nil, nil
}

func Resps() ([]milk.Sol, []milk.Unsol) {
	return sols, unsols
}

var sols = []milk.Sol {
	milk.Sol00_Version,
	milk.Sol02_Status,
	Sol03_Stalls,
	Sol04_Equipment,
	Sol05_Milking,
	Sol06_Profiles,
	Sol07_Colors,
	Sol08_Palette,
	Sol12_Levels,
	Sol18_Rotary,
}

var unsols = []milk.Unsol {
	milk.Unsol01_Restart,
	Unsol09_Stage,
	Unsol10_Mode,
	Unsol11_LetDown,
	Unsol13_Levels,
	Unsol14_Colors,
	Unsol19_Flow,
}

// from cans-server/lines/t4/state.go
type Stage int
const (
	StageReady      Stage = 0x00
	StagePrep       Stage = 0x10
	StagePrepEnd    Stage = 0x20
	StageAttach     Stage = 0x30
	StageLetDown    Stage = 0x40
	StageMilking    Stage = 0x50
	StageDetach     Stage = 0x60
	StagePreWash    Stage = 0x70
	StageWashing    Stage = 0x80
	StageWashDetach Stage = 0x90
)

type State struct {
	//Stage   Stage    `json:"stage"`
	Ready   *Ready   `json:"ready,omitempy"`	
	LetDown *LetDown `json:"letdown,omitempy"`
}

func UnsolStallState(comm byte, extra []byte) (stall byte, state *State) {

	if comm != byte(Unsol09_Stage) {
		return
	}
	if len(extra) > 0 {
		stall = extra[0]
	}
	if len(extra) > 1 {
		stage := int(extra[1] & 0xF0)
		switch stage {

		case int(StageReady):
			state = &State{
				// TODO set ready fields from extra
				Ready:&Ready{},
			}
			return
		
		case int(StageLetDown):
			state = &State{
				// TODO set letdown fields from extra
				LetDown: &LetDown{},
			}
			return
		}
	}
	return
}

type Ready struct {

}

type LetDown struct {

}

