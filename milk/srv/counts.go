package srv

import (
	"strings"
	"sync"

	"gitlab.com/jmireles/cans-base/milk/from"
)

// Counts dispatches CAN responses to a group of listeners.
// Has statistics for the responses and a mutex
type Counts struct {
	r []chan from.Resp
	c *Counter
	m sync.Mutex
}

// NewRx creates a Counts
func NewCounts() *Counts {
	return &Counts{
		c: &Counter{},
		r: make([]chan from.Resp, 0),
	}
}

// AddListener appends a listener to receive CAN responses.
func (d *Counts) AddListener(resp chan from.Resp) {
	d.m.Lock()
	defer d.m.Unlock()
	d.r = append(d.r, resp)
}

// implements from.Rx
func (d *Counts) RxError(err error) {
	d.m.Lock()
	defer d.m.Unlock()
	d.c.Error()
}

// implements from.Rx
func (d *Counts) RxHeartbeat() {
	d.m.Lock()
	defer d.m.Unlock()
	d.c.Heartbeat()
}

// implements from.Rx
func (d *Counts) RxResp(resp from.Resp) error {
	d.m.Lock()
	defer d.m.Unlock()
	if err := d.c.Resp(resp); err != nil {
		return err
	}
	for _, r := range d.r {
		go func(r1 chan from.Resp) {
			r1 <- resp
		}(r)
	}
	return nil
}

func (d *Counts) TxReq() {
	d.m.Lock()
	defer d.m.Unlock()
	d.c.Req()
}

func (d *Counts) Json(sb *strings.Builder) {
	d.m.Lock()
	defer d.m.Unlock()
	d.c.Json(sb)
}

func (d *Counts) String() string {
	d.m.Lock()
	defer d.m.Unlock()
	return d.c.String()	
}
