# SocketCAN


## Raspberry Pi 3 with Waveshare RS485/CAN hat

Edit
```
$ sudo nano /boot/config.txt
```
Be sure next lines are active. Oscillator value should matches hat's crystal.
```
dtparam=spi=on
dtoverlay=mcp2515-can0,oscillator=12000000,interrupt=25,spimaxfrequency=2000000
dtoverlay=spi0-hw-cs
```
Enable SPI communications
```
$ sudo raspi-config
```
Click `3 Interface Options`
Click `P4 SPI Enable/disable automatic loading of SPI kernel module`
Click `Yes`
Click `Finish`

Verify detection of hat:
```
$ ip addr | grep "can"
3: can0: <NOARP,ECHO> mtu 16 qdisc noop state DOWN group default qlen 10
    link/can 
```
Load SocketCAN kernel modules

```
$ sudo modprobe can
$ sudo modprobe can_raw
$ lsmod | grep "can"
can_raw                20480  0
can                    28672  1 can_raw
can_dev                28672  1 mcp251x
```

Configure and bring up the SocketCAN network interface.
For boumatic equipment, bitRate must be 100Khz:

```
$ sudo ip link set can0 type can bitrate 100000 restart-ms 100
$ sudo ip link set up can0
$ ip -details link show can0
4: can0: <NOARP,UP,LOWER_UP,ECHO> mtu 16 qdisc pfifo_fast state UP mode DEFAULT group default qlen 10
    link/can  promiscuity 0 minmtu 0 maxmtu 0 
    can state ERROR-ACTIVE restart-ms 100 
    bitrate 100000 sample-point 0.866 
    tq 666 prop-seg 6 phase-seg1 6 phase-seg2 2 sjw 1
    mcp251x: tseg1 3..16 tseg2 2..8 sjw 1..4 brp 1..64 brp-inc 1
    clock 6000000numtxqueues 1 numrxqueues 1 gso_max_size 65536 gso_max_segs 65535 
```

Download Test apps
https://github.com/linux-can/can-utils
```
$ sudo apt install can-utils
```

Read a connected boumatic I4 and change states
```
$ candump -tz can0
 (000.000000)  can0  10000025   [8]  01 22 00 00 00 00 00 00
 (000.018944)  can0  10000025   [8]  02 06 00 00 00 00 00 00
 (000.020396)  can0  10E00020   [8]  04 00 00 00 00 00 00 00
 (000.987789)  can0  10000025   [8]  03 1A 00 00 00 00 00 00
 (000.989210)  can0  10E00020   [8]  01 01 00 00 00 00 00 00
 (001.120693)  can0  10000025   [8]  AD 6C 74 ED 48 B9 FD F2
 (003.913298)  can0  10E00020   [8]  01 01 74 ED 48 B9 FD F2
 (005.566037)  can0  10000025   [8]  AD 6D 74 ED 48 B9 FD F2
 (006.344934)  can0  10000025   [8]  03 1B 00 00 00 00 00 00
 (006.346363)  can0  10E00020   [8]  02 01 00 00 00 00 00 00
 (006.952826)  can0  10000025   [8]  02 07 00 00 00 00 00 00
 (006.954262)  can0  10E00020   [8]  03 00 00 00 00 00 00 00
 (007.731760)  can0  10000025   [8]  01 23 00 00 00 00 00 00
 (250.244616)  can0  10000021   [0] 
 (250.246029)  can0  10000025   [8]  01 23 00 00 00 00 00 00
 (250.247473)  can0  10000025   [8]  03 1B 00 00 00 00 00 00
 (250.248932)  can0  10000025   [8]  02 07 00 00 00 00 00 00
 (250.250396)  can0  10E00020   [8]  03 00 00 00 00 00 00 00
```

Details:
https://www.kernel.org/doc/Documentation/networking/can.txt

## T4 requests and responses

Connect a T4 CAN cable to Raspberry CAN Hat

At console 1, expect CAN messages with utility `candump`
```bash
dev@raspberrypi:~ $ candump can0
```

At console 2, request messages to T4 with utility `cansend`

1. Get00_Version: `$ cansend can0 00E00000#`
Result in console 1:
```bash
  can0  00E00000   [0] 
  can0  0E000020   [1]  1C
```

2. Get01_Serial: `$ cansend can0 00E00001#`
Result in console 1:
```bash
  can0  00E00001   [0] 
  can0  0A96201F   [6]  00 07 54 B1 00 01
```

3. Get02_Status: `$ cansend can0 00E00002#`
Result in console 1:
```bash
  can0  00E00002   [0] 
  can0  0E000022   [4]  08 08 08 08
```
4. Get03_Config: `$ cansend can0 00E00003#`
Result in console 1:
```bash  
  can0  00E00003   [0] 
  can0  0E000023   [8]  00 01 02 03 04 01 01 01
  can0  0E000023   [8]  01 01 05 06 07 08 01 01
  can0  0E000023   [8]  02 01 01 06 00 00 00 00
  can0  0E000023   [5]  03 00 00 00 00
```

29 Bits Addresses for T4 Gets:

| From<br/>line | To<br/>Line | Destination | Source | Command | Details | Data<br>bytes
|--------|--------|-------------|------------|---------|---------|------
| `0000` | `0111` | `00000000`  | `00000000` | `00000` | Version | 0
| `0000` | `0111` | `00000000`  | `00000000` | `00001` | Serial  | 0 
| `0000` | `0111` | `00000000`  | `00000000` | `00010` | Status  | 0
| `0000` | `0111` | `00000000`  | `00000000` | `00011` | Config  | 0

29 Bits Addresses for T4 Responses:

| From<br/>line | To<br/>Line | Destination | Source | Command | Details | Data<br>bytes
|--------|--------|------------|------------|---------|---------|------
| `0000` | `0111` | `00000000` | `00000001` | `00000` | Version | 1
| `xxxx` | `xxxx` | `xxxxxxxx` | `xxxxxxxx` | `11111` | Serial  | 6
| `0000` | `0111` | `00000000` | `00000001` | `00010` | Status  | 4
| `0000` | `0111` | `00000000` | `00000001` | `00011` | Config  | 8,5
