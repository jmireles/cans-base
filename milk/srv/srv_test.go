package srv

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/p1"
)

func TestSocketCan(t *testing.T) {
	tt := base.Test{t}
	// NewSrc creates a Src with given net, line and box
	net := byte(1)
	box := byte(7)
	if src, err := from.NewSrc(net, from.P1, box); err != nil {
		tt.Ok(err)
	} else if can, err := from.NewCan(src, byte(p1.Sol04_ValCurves), []byte{0,1,2,3,4,5,6,7}); err != nil {
		tt.Ok(err)
	} else if socket, err := NewSocketCanFrom(can); err != nil {
		tt.Ok(err)
	} else {
		tt.Equals("020000E4#0001020304050607", socket.String())
	}
}