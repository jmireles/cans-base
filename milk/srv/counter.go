package srv

import (
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
)

// A Counter counts Net responses statistics:
// heartbeats, errors (like CRC), comms: common responses and boxes: serial responses.
type Counter struct {
	Hbs    int64
	Errs   int64
	Reqs   int64
	Boxes  int64 // number of serial responses
	Sols   int64 // number of solicited responses
	Unsols int64 // number of unsolicited responses
}

// Heartbeat increases by one the heartbeats counter.
func (c *Counter) Heartbeat() {
	c.Hbs++
}

// Error increases by one the errors counter.
func (c *Counter) Error() {
	c.Errs++
}

// Req increases by one the reqs counter
func (c *Counter) Req() {
	c.Reqs++
}

// RxResp receives a response. Calls Command's resp method to update statistics.
// Increments boxes/sols/unsols/error count.
func (c *Counter) Resp(resp from.Resp) error {
	if comm, sol, unsol, err := resp.Comm(); err != nil {
		c.Errs++
		return err
	} else {
		if comm == byte(milk.Sol31_Serial) {
			c.Boxes++
		} else if sol != nil {
			c.Sols++
		} else if unsol != nil {
			c.Unsols++
		}
		return nil
	}
}

// Json writes in buffer a JSON representation.
func (c *Counter) Json(sb *strings.Builder) {
	// manually done to preserve fields order and respond fast
	sb.WriteString(`{`)
	sb.WriteString(`"hbs":` + strconv.FormatInt(c.Hbs, 10))
	sb.WriteString(`,"errs":` + strconv.FormatInt(c.Errs, 10))
	sb.WriteString(`,"reqs":` + strconv.FormatInt(c.Reqs, 10))
	sb.WriteString(`,"boxes":` + strconv.FormatInt(c.Boxes, 10))
	sb.WriteString(`,"sols":` + strconv.FormatInt(c.Sols, 10))
	sb.WriteString(`,"unsols":` + strconv.FormatInt(c.Unsols, 10))
	sb.WriteString(`}`)
}

// String returns a string representation
func (c *Counter) String() string {
	var sb strings.Builder
	sb.WriteString(`h:` + strconv.FormatInt(c.Hbs, 10))
	sb.WriteString(` e:` + strconv.FormatInt(c.Errs, 10))
	sb.WriteString(` r:` + strconv.FormatInt(c.Reqs, 10))
	sb.WriteString(` b:` + strconv.FormatInt(c.Boxes, 10))
	sb.WriteString(` s:` + strconv.FormatInt(c.Sols, 10))
	sb.WriteString(` u:` + strconv.FormatInt(c.Unsols, 10))
	return sb.String()
}
