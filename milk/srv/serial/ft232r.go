package serial

import (
	"fmt"

	"github.com/tarm/serial"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/to"
)

const (
	head byte = 0x5a
)

const (
	classCan    byte = 0x05
	classStatus byte = 0x06
	funcWrite   byte = 0x03
	funcUnsol   byte = 0x10
)

const (
	stateHead = iota
	stateCount
	stateData
	stateCRC
)

type FT232R struct {
	*USB
	state int
	count byte
	crc   byte
	pos   byte
}

func NewFT232R(rx from.Rx) *FT232R {
	dongle := &FT232R{}
	dongle.state = stateHead
	dongle.USB = NewUSB(dongle, rx)
	return dongle
}

func (d *FT232R) GetUSB() *USB {
	return d.USB
}

// OpenPort open serial port with path and bauds given
// A response listener is given also to receive port receptions
// receive dongle heartbeats at console
// $sudo socat /dev/ttyUSB0,b115200,raw,echo=0,crnl -
func (d *FT232R) OpenPort(path string, bauds int, net byte) (chan bool, error) {
	config := &serial.Config{Name: path, Baud: bauds}
	port, err := serial.OpenPort(config)
	if err != nil {
		return nil, err
	}
	done := d.Serve(path, net, port)
	closer := make(chan bool, 1)
	go func() {
		select {
		case <-closer:
			port.Close()
		case <-done:
			port.Close()
		}
	}()
	return closer, nil
}

func (d *FT232R) Req(bytes to.Bytes) ([]byte, error) {
	if d.rw == nil {
		return nil, fmt.Errorf("%s %s not started", Serial_FT232R, d.name)
	}
	// Example reqVersion: line=1, src=0, dst=2, command=0
	// Result { head=90, count=7, class=5, func=3, 1, 0, 2, 0, crc=5 }
	out := make([]byte, 5+len(bytes))
	out[0] = head
	out[1] = byte(3 + len(bytes))
	out[2] = classCan
	out[3] = funcWrite
	copy(out[4:], bytes)
	crc := byte(0)
	for i := 2; i < len(out); i++ {
		crc ^= out[i]
	}
	out[len(out)-1] = crc
	return out, nil
}

func (d *FT232R) Resp(bytes []byte) {
	// byte-by-byte analysis is needed since crc needs to be calculated
	for _, b := range bytes {
		switch d.state {
		case stateHead:
			if head == b {
				d.state = stateCount
			}
		case stateCount:
			d.count = b
			d.crc = 0
			d.pos = 0
			d.state = stateData
		case stateData:
			d.data[d.pos] = b
			d.pos++
			d.crc ^= b
			if d.pos >= d.count-1 {
				d.state = stateCRC
			}
		case stateCRC:
			if d.crc != b {
				d.rx.RxError(fmt.Errorf("serial resp CRC error"))
			} else if d.data[0] == classStatus { // 0x06
				if d.data[1] == funcUnsol { // 0x10
					// heartbeat example:
					// head=5a count=04 class=06 func=10 data=00 crc=16
					d.rx.RxHeartbeat()
				}
			} else if d.data[0] == classCan { // 0x05
				if d.data[1] == funcUnsol { // 0x10
					//log.Printf("data=% 02x crc=%02x", d.data[:d.pos], d.crc)
					if d.count >= 7 {
						//  0  1  2  3  4  5  6  7  8  9 10 11
						// cl fn -------- cm sc to -------  id
						// 05 10 10 00 00 1f 00 01 10 00 00 01 : pm serial
						// 05 10 54 b1 63 1f 00 08 54 b1 63 01 : i4 serial

						//  0  1  2  3  4  5  6  7  8  9 10 11 12 13
						// cl fu fr ds sr co -----------------------
						//       10 00 01 01 00 00 00 00 00 00 00 00 : pm-1 startup
						//       10 00 01 10 00 00 00 00 00 00 00 00 : pm-1 unsol-no-phase-front
						//       10 00 01 11 00 00 00 00 00 00 00 00 : pm-1 unsol-no-phase-rear
						// 05 10 10 00 01 00 18                      : pm version
						// 05 10 80 00 01 00 06                      : i4 version
						// 05 10 10 00 01 02 7a 00 00                : pm status
						// 05 10 80 00 01 02 01 23 02 06 03 1b 00 0d : i4 status
						// 05 10 10 00 01 03 00 01 28 28 21 29 28 14 : pm cfg
						// 05 10 10 00 01 03 01 14 03 e8 03 e8 03 e8 : pm cfg
						// 05 10 10 00 01 03 02 03 e8 03 b8 03 b8 00 : pm cfg
						// 05 10 10 00 01 03 03 8c 84 03 b8 03 b8 00 : pm cfg

						//fmt.Printf("FT232R.resp % 02x\n", d.data[2:d.pos])

						if now, err := base.UTCNow(); err != nil {
							d.rx.RxError(err)
						} else if resp, err := from.NewResp(d.net, d.data[2:d.pos], now); err != nil {
							d.rx.RxError(err)
						} else {
							d.rx.RxResp(resp)
						}
					}
				}
			}
			d.state = stateHead
		}
	}
}


/* Debug boumatic-dongle:
$ od -t x1 < /dev/ttyUSB0
0000000 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000020 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000040 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000060 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000100 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000120 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000140 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000160 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000200 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000220 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000240 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000260 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000300 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000320 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000340 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000360 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000400 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000420 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000440 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000460 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000500 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000520 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000540 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000560 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000600 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0000620 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0000640 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0000660 5a 07 05 10 10 00 01 01 05 5a 04 06 10 00 16 5a
0000700 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00
0000720 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 07 05
0000740 10 10 00 01 14 10 5a 04 06 10 00 16 5a 04 06 10
0000760 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0001000 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0001020 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0001040 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
0001060 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0001100 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 07 05 10
0001120 10 00 01 10 14 5a 07 05 10 10 00 01 11 15 5a 04
0001140 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16
0001160 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04 06 10
0001200 00 16 5a 04 06 10 00 16 5a 04 06 10 00 16 5a 04
*/