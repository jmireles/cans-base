# CH340

## Dongle with CH340

![title](img/dongle.jpeg)

## Windows

Below is shown the USBCAN V8.00 anaylizer https://github.com/SeeedDocument/USB-CAN-Analyzer connected to a single Smartlite.

![title](img/ch340.png)

Select first the USB speed USB below COM configure. Every time the dongle is powered up will blink according these speeds:

| Blinks   | BPS       |
| -------- | ---------:|
| 1        | 2_000_000 |
| 2        | 1_228_800 |
| 3        |   115_200 |
| 4        |    38_400 |
| 5        |    19_200 |
| 6        |     9_600 |


CAN important settings are under `CAN Configure`:
- Type: `Extended Frame`
- CAN bps: `100 kbps`.

Then click `Set and Start` button and then set field ID `00E00000` which is the request for all the Smartlite boxes versions.

To send the request click button `Send a single frame` having at least one Smartlite connected to CAN. Expect messages like those two:

| No | Direction | ... | Frame ID | Lenght | Data
|----|-----------|-----|----------|--------|-------
| 0  | Send      |     | 00E00000 | 0      |
| 1  | Receive   |     | 0e000020 | 1      | 1c


Then turn Smartlite off and then on and expect next rows as unsolicited messages:

| No | Direction | ... | Frame ID | Lenght | Data
|----|-----------|-----|----------|--------|-------
| 2  | Receive   |     | 0ee00021 | 0      | 
| 3  | Receive   |     | 0e000029 | 5      | 08 00 07 00 00 
| 4  | Receive   |     | 0e000029 | 5      | 02 01 07 00 00
| .  | ...       | ... | ...      | ...    | ...   
| 9  | Receive   |     | 0e00002e | 6      | 01 02 04 04 04 01
| 10 | Receive   |     | 0e00002e | 6      | 04 03 04 04 04 01


## Linux

In windows the CH340 was left programmed fo 115_200 USB and Extended 100khz for CAN.

Connect the dongle to a Linux USB slot (this one gets USB0) and set the bauds to `115_200`:

```
~$ stty -F /dev/ttyUSB0 speed 115200 cs8 -cstopb -parenb
9600
~$ stty -F /dev/ttyUSB0 speed 115200 cs8 -cstopb -parenb
115200
```

Then with a Smartlite connected, click a button and cat device `/dev/ttyUSB0` and expect the following:

```
$ od -t x1 /dev/ttyUSB0
0000000 aa e1 21 00 e0 0e 08 55 aa e6 2e 00 00 0e 08 00
0000020 28 28 00 0b 55 aa e5 29 00 00 0e 08 00 00 00 55
0000040 aa e6 2e 00 00 0e 08 00 00 28 00 08 55 aa e8 2b
0000060 00 00 0e 08 94 00 43 61 00 55 aa e2 2a 00 00 0e
0000100 08 10 55 aa e6 2e 00 00 0e 08 00
0000113

```
The dongle protocol includes:
- headers (HH) with value `aa`
- types (TT) with valueslike `e1`-`e8`
- four ID bytes I1,I2,I3,I4 and 0 to 8 data bytes
- footers (FF) with value `55`

The type format in bits is `11ESSSSS` where E=extended SSSSS=number of data bytes.

Rearranging the previous unsolicited Smartlite message we have this:

```          
HH TT I1 I2 I3 I4 PPPPPPPPPPPPPPPPP FF   |  --ERSSSS I4I3I2I1
== == =========== ================= ==
aa e1 21 00 e0 0e 08                55   |  11100001 0ee00021
aa e6 2e 00 00 0e 08 00 28 28 00 0b 55   |  11100110
aa e5 29 00 00 0e 08 00 00 00       55   |  11100101
aa e6 2e 00 00 0e 08 00 00 28 00 08 55   |  11100110
aa e8 2b 00 00 0e 08 94 00 43 61 00 55   |  11101000 0e00002b
aa e2 2a 00 00 0e 08 10             55 
aa e6 2e 00 00 0e 08 00 ...
```


## Boumatic equipment

Boumatic request 29 bits Id format is as follows:
```
,-----------,-----------,-----------,-------,
| 0000 LLLL | DDDD DDDD | SSSS SSSS | CCCCC |
'-----------'-----------'-----------'-------'
```
`0000` means PC line as source is sending a message to a destination line `LLLL`.
`DDDDDDDD` is the box ID to receive the request and `SSSSSSSS` is the source (0 always for PC).
`CCCCC` is the 0-31 command for every line.

Boumatic format is shifted for the generic ID of 29 bits as follows:
```
  0000L LLLD DDDD DDDS SSSS SSSC CCCC -> 29 bits
  ===== ==== ==== ==== ==== ==== ====
  00000 1110 0000 0000 0000 0000 0000 -> binary
      0    E    0    0    0    0    0 -> hexadecimal
```

Boumatic Values:
```
LLLL = 0000 -> PC (computer)
     = 0001 -> PM (pulsator)
     = 0111 -> SL (smartlite)
     = 1000 -> I4 (inputs-4)

SSSS-SSSS = 0 for source PC
DDDD-DDDD = 0 means broadcast

CCCCC = 00000 -> Get Version
        00001 -> Get Serial
        00010 -> Get Status
        00011 -> Get Config

```

## Code

This code connects CH340 dongle to read/write boumatic messages: [ch340.go](../ch340.go)
