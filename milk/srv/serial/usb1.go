package serial

import (
	"strconv"
	"strings"

	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/srv"
	"gitlab.com/jmireles/cans-base/milk/to"
)

// A Usb1 is a port of type serial-usb connected to linux.
// A path (dev/ttyUBSxxx) and a speed values are used to open the port.
type Usb1 struct {
	dongle *FT232R
	path   string
	speed  int
	counts *srv.Counts
}

// NewUsb1 creates a new Usb1 port. The port can be opened as
// a Usb1 serial port using path and speed parameters.
// can is a consecutive number starting in 1 to identify this Usb1
// among others. Rx is the receiver

// TODO generalize dongle to port

func NewUsb1(path string, speed int, resps chan from.Resp) *Usb1 {
	counts := srv.NewCounts()
	counts.AddListener(resps)
	return &Usb1{
		dongle: NewFT232R(counts),
		path:   path,
		speed:  speed,
		counts: counts,
	}
}

// Open opens the dongle serial Usb1 port.
func (u *Usb1) OpenPort(net byte) (closer chan bool, err error) {
	closer, err = u.dongle.OpenPort(u.path, u.speed, net)
	if err != nil {
		u.counts.RxError(err)
	}
	return
}

// Req sends a request to CAN after dongle.
// Since every dongle has a single net, net identifier is not needed just bytes
// Message bytes should be 4-12 bytes long to be a CAN valid message.
func (u *Usb1) Request(bytes to.Bytes) (n uint, err error) {
	n, err = u.dongle.Request(bytes)
	if err != nil {
		u.counts.RxError(err)
	} else {
		u.counts.TxReq()
	}
	return
}

// Json updates a buffer with JSON values as path, speed and rx metrics.
func (u *Usb1) Json(sb *strings.Builder) {
	sb.WriteString(`{`)
	sb.WriteString(`"path":"` + u.path + `"`)
	sb.WriteString(`,"speed":` + strconv.Itoa(u.speed))
	sb.WriteString(`,"counts":`)
	u.counts.Json(sb)
	sb.WriteString(`}`)
}
