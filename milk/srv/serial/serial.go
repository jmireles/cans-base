package serial

import (
	"fmt"

	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/to"
)

const (
	Serial_FT232R = "FT232R"
	Serial_CH340  = "CH340"
)

type SerialPort interface {

	GetUSB() *USB

	Req(bytes to.Bytes) ([]byte, error)

	Resp(bytes []byte)
}

func NewSerialPort(serial string, rx from.Rx) SerialPort {
	switch serial {
	
	case Serial_CH340:
		return NewCH340(rx)
	
	default:
		return NewFT232R(rx)
	}
}


// Serial is used in JSON format in browser to connect to a serial port already
// selected by USB vendor and product numbers
type Serial struct {
	Error string `json:"error,omitempty"`
	Name  string `json:"name,omitempty"`
	Port  *SPort `json:"port,omitempty"`
}

type SPort struct {
	BaudRate    int    `json:"baudRate"`
	BufferSize  int    `json:"bufferSize"`
	DataBits    int    `json:"dataBits"`
	FlowControl string `json:"flowControl"`
	Parity      string `json:"parity"`
	StopBits    int    `json:"stopBits"`
}

func NewSerial(vendor, product int) *Serial {
	if (vendor == 0x403 && product == 0x6001) {
		return &Serial{
			Name: Serial_FT232R,
			Port: &SPort {
				BaudRate:    115200,
				BufferSize:  255,
				DataBits:    8,
				FlowControl: "none",
				Parity:      "none",
				StopBits:    1,
			},
		}
	} else if (vendor == 0x1a86 && product == 0x7523) {
		return &Serial{
			Name: Serial_CH340,
			Port: &SPort {
				BaudRate:    115200,
				BufferSize:  255,
				DataBits:    8,
				FlowControl: "none",
				Parity:      "none",
				StopBits:    1,
			},
		}
	} else {
		return &Serial{
			Error: fmt.Sprintf(`Unsupported serial: vendor=0x%x product=0x%x`, vendor, product),
		}
	}
}

