package serial

import (
	"fmt"
	"io"

	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/to"
)

type USB struct {
	port  SerialPort
	rx    from.Rx
	data  []byte
	name  string
	net   byte
	rw    io.ReadWriter
}

func NewUSB(port SerialPort, rx from.Rx) *USB {
	return &USB{
		port: port,
		rx:   rx,
		data: make([]byte, 256),
	}
}

func (u *USB) Port() SerialPort {
	return u.port
}

func (u *USB) Serve(name string, net byte, rw io.ReadWriter) (chan error) {
	u.name = name
	u.net = net
	u.rw = rw
	done := make(chan error, 1)
	go func() {
		b := make([]byte, 256)
		for {
			if rw == nil {
				close(done)
				return
			} else if n, err := rw.Read(b); err != nil {
				close(done)
				return
			} else {
				u.port.Resp(b[:n])
			}
		}
	}()
	return done
}

// Request writes to the this serial port the tx payload 
// added with header/counter/crc for real serial dongles.
func (u *USB) Request(tx to.Bytes) (uint, error) {
	out, err := u.port.Req(tx)
	if err != nil {
		return 0, err
	}
	i, err := u.rw.Write(out)
	return uint(i), err
}

// RequestHex writes to this websocket port the tx payload 
// added with header/counter/crc and coverted to hex for websockets
// which in turn converts to binary again for browsers web-serial dongles.
func (u *USB) RequestHex(tx to.Bytes) (uint, error) {
	out, err := u.port.Req(tx)
	if err != nil {
		return 0, err
	}
	n, err := u.rw.Write([]byte(fmt.Sprintf("%02x", out)))
	return uint(n), err
}









