package serial

import(
	"fmt"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/to"
)

// AA-D8 are the consecutive states of reception
const (
	AA int = iota // none
	EE
	I1 
	I2
	I3
	I4
	D0
	D1
	D2
	D3
	D4
	D5
	D6
	D7
	D8
)

type CH340 struct {
	*USB
	state int
	max   []byte
}

// NewCH340 Returns a USB port to send and receive CAN-USB messages
func NewCH340(rx from.Rx) *CH340 {
	ch340 := &CH340{
		state: AA,
		max:   make([]byte, D8+1),
	}
	ch340.USB = NewUSB(ch340, rx)
	return ch340
}

func (c *CH340) GetUSB() *USB {
	return c.USB
}

func (c *CH340) Resp(bytes []byte) {
	//fmt.Printf("CH340.Resp % 02x\n", bytes)
	for _, b := range bytes {
		c.resp(b)
	}
}

func (c *CH340) resp(b byte) {

	next := func() {
		c.max[c.state] = b
		c.state += 1
	}
	mayComplete := func(payload int) {
		if b == 0x55 {
			c.respComplete(payload)
			c.state = AA
		} else if b == 0xAA {
			c.state = EE
		} else {
			c.state = AA
		}
	}

	data := func(ee byte, payload int) {
		if c.max[EE] == ee {
			mayComplete(payload)
		} else {
			next()
		}
	}

	switch c.state {
	case AA:
		if b == 0xAA {
			c.state = EE
		}
	
	case EE:
		next() // max[EE] e0,e1,e2,e3,e4,e5,e6,e7,e8
	
	case I1, I2, I3, I4:
		next() // 
	
	case D0: data(0xe0, 0)
	case D1: data(0xe1, 1)
	case D2: data(0xe2, 2)
	case D3: data(0xe3, 3)
	case D4: data(0xe4, 4)
	case D5: data(0xe5, 5)
	case D6: data(0xe6, 6)
	case D7: data(0xe7, 7)
	case D8: data(0xe8, 8)

	default:
		if b == 0xAA {
			c.state = EE
		} else {
			c.state = AA
		}
	}
}

// ,-----------,-----------,-----------,-------,
// | -to- from | DDDD DDDD | SSSS SSSS | CCCCC |
// '-----------'-----------'-----------'-------'
func (c *CH340) respComplete(payload int) {
	id := int(c.max[I1])       //                         11111111
	id |= int(c.max[I2]) <<  8 //                 2222222211111111
	id |= int(c.max[I3]) << 16 //         333333332222222211111111
	id |= int(c.max[I4]) << 24 // 44444444333333332222222211111111

	cmd := id & 0x1F; id >>= 5 //                            11111    
	src := id & 0xFF; id >>= 8 //                    22222111
	dst := id & 0xFF; id >>= 8 //            33333222
	to  := id & 0x0F; id >>= 4 //        4333
	fro := id & 0x0F           // ---4444
	//var data []byte
	//if payload > 0 {
	//	data = c.max[D0 : D0 + payload ]
	//}
	//if cmd == 0x1F {
	//	fmt.Printf("cans-base CH340.respComplete serial\n")
	//} else {
	//	fmt.Printf("cans-base CH340.respComplete to=%1x from=%1x dst=%02x src=%02x cmd=%02x data=%v\n",
	//		to, fro, dst, src, cmd, data,
	//	)
	//}
	out := []byte{
		byte(fro<<4) | byte(to),
		byte(dst),
		byte(src),
		byte(cmd),
	}
	for i := 0; i < payload; i++ {
		out = append(out, c.max[D0 + i])
	}
	//fmt.Printf("cans-base CH340.respComplete out=[% 02x]\n", out)
	if now, err := base.UTCNow(); err != nil {
		c.rx.RxError(err)
	} else if resp, err := from.NewResp(c.net, out, now); err != nil {
		c.rx.RxError(err)
	} else {
		c.rx.RxResp(resp)
	}
}


func (c *CH340) Req(bytes to.Bytes) ([]byte, error) {
	if c.rw == nil {
		return nil, fmt.Errorf("%s %s not started", Serial_CH340, c.name)
	}
	n := len(bytes)
	if n < 4 || n > 12 {
		return nil, fmt.Errorf("bytes wrong size %02x", bytes)
	}
	// out[0] = byte(r.to)
	// out[1] = r.dst
	// out[2] = byte(0) // src always 0
	// out[3] = r.comm
	// copy(out[4:], r.extra)
	// Example: 07 00 00 01 PC->SL [07] dst=[00] src=[00 always] cmd=[01] (serials)
	// ,-----------,-----------,-----------,-------,
	// | from -to- | DDDD DDDD | SSSS SSSS | CCCCC | 29 bits
	// '-----------'-----------'-----------'-------'
	id := int(bytes[3] & 0x1F)  //                            ccccc // s always 00000000
	id |= (int(bytes[1]) << 13) //            ddddddddssssssssccccc
	id |= (int(bytes[0]) << 21) // ---ffffttttddddddddssssssssccccc
                                // ---00000111000000000000000000001 // SL serial
                                // DDDDDDDDCCCCCCCCBBBBBBBBAAAAAAAA // 00 E0 00 01
	payload := n - 4
	ctrl := 0xe0 + byte(payload & 0x1F)
	out := []byte{
		0xAA,           // header
		ctrl,           // 111sssss e0-e8 0 <= sssss <= 8 
		byte(id),       // sssccccc 01
		byte(id >> 8),  // dddsssss 00
		byte(id >> 16), // fttttddd E0
		byte(id >> 24), //      fff 00
	}
	for i := 4; i < n; i++ {
		out = append(out, bytes[i])
	}
	out = append(out, 0x55)
	//fmt.Printf("cans-base CH340.Req bytes=[% 02x] id=%02x out=[% 02x]\n", bytes, id, out)
	return out, nil
}


/*

Smartlite turned off/on:
                                 0  1  2  3  4  5    6  7  8  9 19 11 12 13 14
                                AA EE I1 I2 I3 I4   D0 D1 D2 D3 D4 D5 D6 D7 D8
                                == == == == == == | == == == == == == == == ==
cans-base/src/serial.CH340.Resp aa e0 21 00 00 0e |                         55
cans-base/src/serial.CH340.Resp aa e5 29 00 00 0e | 08 00 07 00 00          55
cans-base/src/serial.CH340.Resp aa e5 29 00 00 0e | 02 01 07 00 00          55
cans-base/src/serial.CH340.Resp aa e5 29 00 00 0e | 01 02 07 00 00          55
cans-base/src/serial.CH340.Resp aa e5 29 00 00 0e | 04 03 07 00 00          55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 08 00 04 04 04 01       55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 02 01 04 04 04 01       55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 01 02 04 04 04 01       55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 04 03 04 04 04 01       55

Smartlite button click

cans-base/src/serial.CH340.Resp aa e1 21 00 e0 0e | 08                      55 ()
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 08 00 28 28 00 0b       55
cans-base/src/serial.CH340.Resp aa e5 29 00 00 0e | 08 00 04 00 00          55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 08 00 00 28 00 08       55
cans-base/src/serial.CH340.Resp aa e8 2b 00 00 0e | 08 94 00 04 04 61 00 04 55
cans-base/src/serial.CH340.Resp aa e2 2a 00 00 0e | 08 10                   55
cans-base/src/serial.CH340.Resp aa e6 2e 00 00 0e | 08 00 04 04 04 01       55

*/