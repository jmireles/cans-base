package serial

import (
	//"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
	"strings"
	"syscall"
	"sync"
	"testing"
	"time"

	"github.com/tarm/serial"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/milk/i4"
	"gitlab.com/jmireles/cans-base/milk/p1"
	"gitlab.com/jmireles/cans-base/milk/t4"
	"gitlab.com/jmireles/cans-base/milk/to"
	"gitlab.com/jmireles/cans-base/milk/srv"
)


// Following counts includes
// 14-heartbeats and
// 3 PM unsolicited events: startup and no-phases rear/front alarms (messages 0-h)
const dongleMultipleResps = `
 06 10 00 16 
 5a 04 06 10 00 16 
 5a 04 06 10 00 16
 5a 04 06 10 00 16 
 5a 04 06 10 00 16 
 5a 04 06 10 00 16
 5a 04 06 10 00 16 
 5a 04 06 10 00 16 
 5a 04 06 10 00 16
 5a 04 06 10 00 16 
 5a 07 05 10 10 00 01 01 05
 5a 04 06 10 00 16 
 5a 04 06 10 00 16 
 5a 04 06 10 00 16
 5a 04 06 10 00 16 
 5a 07 05 10 10 00 01 10 14
 5a 07 05 10 10 00 01 11 15 
 5a 04 06 10 00 16`

var (
	heartbeat = []byte{head, 4, classStatus, funcUnsol, 0x00, 0x16}
	heartbeatData = heartbeat[2:]
)

func TestDongleResp(t *testing.T) {
	tt := base.Test{t}
	// simulate to receive fabricated responses
	// received in groups of 11s to test dongle.Rx parsing
	counts := srv.NewCounts()
	dongle := NewFT232R(counts)
	dongle.net = 1

	// inject 1 heartbeat in response
	dongle.resp(heartbeat)
	
	// broke responses in groups of 11s to test partial parsing byte-by-byte
	// inject 14 heartbeats and 3 unsolicited
	fields := strings.Fields(dongleMultipleResps)
	elevens := 11 + (len(fields)/11)*11
	bytes := make([]byte, elevens)
	for pos, field := range fields {
		_, err := fmt.Sscanf(field, "%02x", &bytes[pos])
		tt.Ok(err)
	}
	resps := 0
	for i := 0; i < len(bytes); i += 11 {
		group := bytes[i : i+11]
		dongle.resp(group)
		t.Logf("resp %s% 02x%s", base.Yellow, group, base.Reset)
		resps++
	}
	tt.Equals(11, resps)
	var sb strings.Builder
	counts.Json(&sb)
	t.Logf("JSON %s%s%s", base.Green, sb.String(), base.Reset)
	t.Logf("string %s%s%s", base.Green, counts.String(), base.Reset)
	tt.Equals(`{"hbs":15,"errs":0,"reqs":0,"boxes":0,"sols":0,"unsols":3}`, sb.String())
	tt.Equals("h:15 e:0 r:0 b:0 s:0 u:3", counts.String())
}

func TestDongleServe(t *testing.T) {
	tt := base.Test{t}
	counts := srv.NewCounts()
	dongle := NewFT232R(counts)
	rxs := make(chan []byte, 1)
	rw := &RW{ rxs:rxs, txs:make([]byte, 0) }
	done := dongle.Serve("test", 1, rw)
	go func() {
		// push some valid dongle rxs to RW
		fields := strings.Fields(dongleMultipleResps)
		elevens := 11 + (len(fields)/11)*11
		bytes := make([]byte, elevens)
		for pos, field := range fields {
			fmt.Sscanf(field, "%02x", &bytes[pos])
		}
		for i := 0; i < len(bytes); i += 11 {
			rxs <-bytes[i : i+11]
			time.Sleep(10 * time.Millisecond)
		}
		// request some valid dongle txs to RW
		for _, toLine := range []to.To { to.P1, to.I4, to.T4 } {
			req := to.NewComm(toLine, 0, nil)
			req.GetVersion()
			dongle.Request(req.Bytes())
			time.Sleep(10 * time.Millisecond)
		}
		time.Sleep(10 * time.Millisecond)
		rxs<- nil // fires done returned by Serve
	}()
	<-done
	// validate rxs
	c := counts.String()
	tt.Equals("h:14 e:0 r:0 b:0 s:0 u:3", c) // validate rxs
	t.Logf("%s%s%s", base.Green, c, base.Reset)
	// validate txs
	exp := "5a0705030100000007" // p1
	exp += "5a070503080000000e" // i4
	exp += "5a0705030700000001" // t4
	txs := fmt.Sprintf("%02x", rw.txs)
	tt.Equals(exp, txs)
	t.Logf("%s%s%s", base.Blue, txs, base.Reset)
}

func TestDongleServeHex(t *testing.T) {
	tt := base.Test{t}
	counts := srv.NewCounts()
	dongle := NewFT232R(counts)
	rxs := make(chan []byte, 1)
	rw := &RW{ rxs:rxs, txs:make([]byte, 0) }
	done := dongle.Serve("test", 1, rw)
	go func() {
		// request some valid dongle txs to RW
		for _, toLine := range []to.To { to.P1 } {
			req := to.NewComm(toLine, 0, nil)
			req.GetVersion()
			dongle.RequestHex(req.Bytes())
			time.Sleep(10 * time.Millisecond)
		}
		time.Sleep(10 * time.Millisecond)
		rxs<- nil // fires done returned by Serve
	}()
	<-done
	txs := fmt.Sprintf("%02x", rw.txs)
	//          5 a 0 7 0 5 0 3 0 1 0 0 0 0 0 0 0 7
	tt.Equals("356130373035303330313030303030303037", txs)
	t.Logf("%s%s%s", base.Blue, txs, base.Reset)
}

type RW struct {
	rxs chan []byte
	txs []byte
}

func (rw *RW) Read(p []byte) (int, error) {
	read := <- rw.rxs
	if len(read) > 0 {
		copy(p, read)
		return len(read), nil
	} else {
		return 0, fmt.Errorf("error-read")
	}
}

func (rw *RW) Write(p []byte) (int, error) {
	if len(p) > 0 {
		rw.txs = append(rw.txs, p...)
		return len(p), nil
	} else {
		return 0, fmt.Errorf("error-write")
	}
}

func TestDongleSocats(t *testing.T) {
	tt := base.Test{t}
	txDev := "/tmp/ttyS10" // dongle sending
	rxDev := "/tmp/ttyS11" // dongle receiving
	cmd := exec.Command("socat", "PTY,link="+txDev, "PTY,link="+rxDev)
	defer func() {
		// prevent socat remains running at background
		if cmd.Process != nil {
			cmd.Process.Signal(syscall.SIGTERM)
		}
	}()
	if err := cmd.Start(); err != nil {
		t.Skipf("%ssocat program must be installed in linux!%s", base.Yellow, base.Reset)
		return
	}
	// wait socat ports be ready
	time.Sleep(1 * time.Second)


	// reception port
	rxs := make(chan []byte, 1)
	rxPort, err := serial.OpenPort(&serial.Config{Name: rxDev, Baud: 115200})
	tt.Ok(err)
	go func() {
		buf := make([]byte, 256)
		for {
			if n, err := rxPort.Read(buf); err != nil {
				rxs <- nil // error
				t.Log(err)
			} else {
				rxs <- buf[:n]
			}
		}
	}()

	getSerial := milk.Get01_Serial
	getConfig := p1.Get03_Config
	setConfig := p1.Set04_Config

	type R struct { box byte; get *milk.Get; set *milk.Set; extra []byte; exp []byte }
	reqs := []R {
		// get all pm boxes serial numbers
		R{ box:0, get:&getSerial, exp:[]byte{head,7,classCan,funcWrite,1,0,0,1,6 } },
		// get pm box 7 configuration
		R{ box:7, get:&getConfig, exp:[]byte{head,7,classCan,funcWrite,1,7,0,3,3 } },
		// set pm box 7 configuration
		R{ box:7, set:&setConfig, extra:[]byte{1,2,3,4,5,6,7,8}, exp:[]byte{head,15,classCan,funcWrite,1,7,0,4,1,2,3,4,5,6,7,8,0xc} },
	}

	// sending port
	net := byte(1)
	counts := srv.NewCounts()
	dongle := NewFT232R(counts)
	closer, err := dongle.OpenPort(txDev, 115200, net)
	tt.Ok(err)

	for _, r := range reqs {
		toLine := to.P1
		now, _ := base.UTCNow()
		req := to.NewComm(toLine, r.box, now)
		if r.get != nil {
			req.Get(*r.get, r.extra)
		} else if r.set != nil {
			req.Set(*r.set, r.extra)
		} else {
			continue
		}
		_, err = dongle.Request(req.Bytes())
		tt.Ok(err)
		// wait for response
		rx := <-rxs
		tt.Assert(rx != nil, "rx error")
		tt.Equals(r.exp, rx)
		t.Logf("Req:%s% 02x%s Read: %s% 02x%s", base.Yellow, req.Bytes(), base.Reset, base.Green, rx, base.Reset)
		time.Sleep(100 * time.Millisecond)
	}
	time.Sleep(100 * time.Millisecond)
	rxPort.Close()
	closer<- true
}

// TestDongleReal requires a real dongle connected at linux /dev/ttyUSB0
// First: waits 2 seconds to collect heartbeats. Skip if found none.
// Second: Send broadcasts to PMs, I4s and T4s for versions. For every version received 
// send to particular box request status.
func TestRealDongle(t *testing.T) {
	tt := base.Test{t}
	net := byte(1)
	resp := newResp(t)
	dongle := NewFT232R(resp)
	closer, err := dongle.OpenPort("/dev/ttyUSB0", 115200, net)
	if err != nil {
		t.Skipf("%sError %v%s", base.Yellow, err, base.Reset)
		return
	}
	t.Run("Heartbeats", func(t *testing.T) {
		time.Sleep(2 * time.Second)
		var sb strings.Builder
		resp.c.Json(&sb)
		jsonS := sb.String()
		t.Logf("%s%s%s", base.Green, jsonS, base.Reset)
		// TODO update cans-base and call api, err := srv.NewApiCounts(jsonS)
		api := &srv.ApiCounts{}
		err := json.Unmarshal([]byte(jsonS), api)
	    tt.Ok(err)
		if api.Hbs == 0 {
			t.Skipf("No heartbeats after 2 seconds opened. No dongle?")
		} else {
			t.Logf("heartbeats: %d", api.Hbs)
		}
	})
	t.Run("Version-Status", func(t *testing.T) {
		for _, toLine := range []to.To { to.P1, to.I4, to.T4 } {
			now, _ := base.UTCNow()
			req := to.NewComm(toLine, 0, now)
			req.GetVersion()
			bytes := req.Bytes()
			_, err := dongle.Request(bytes)
			tt.Ok(err)
			t.Logf("req version: %s% 02x%s", base.Yellow, bytes, base.Reset)
			time.Sleep(500 * time.Millisecond)
		}
		sb := &strings.Builder{}
		resp.c.Json(sb)
		t.Logf("%s%s%s", base.Green, sb.String(), base.Reset)
		// for every solicited version response so far, request status for that box
		resp.CommSols(func(lineF from.From, box byte, sol milk.Sol, extra []byte) {
			if sol == milk.Sol00_Version {
				t.Logf("%ssol line=%02x box=%d version=% 02x%s", base.Blue, lineF, box, extra, base.Reset)
				lineName, _ := lineF.Name()
				lineT, _ := to.New(lineName) 
				now, _ := base.UTCNow()
				req := to.NewComm(lineT, box, now)
				req.GetStatus()
				bytes := req.Bytes()
				_, err := dongle.Request(bytes)
				tt.Ok(err)
				t.Logf("req status: %s% 02x%s", base.Yellow, bytes, base.Reset)
				time.Sleep(500 * time.Millisecond)
			}
		})
		// inspect for status responses so far
		resp.CommSols(func(lineF from.From, box byte, sol milk.Sol, extra []byte) {
			if sol == milk.Sol02_Status {
				t.Logf("%ssol line=%02x box=%d status=% 02x%s", base.Blue, lineF, box, extra, base.Reset)
			}
		})
		sb = &strings.Builder{}
		resp.c.Json(sb)
		t.Logf("%s%s%s", base.Green, sb.String(), base.Reset)
	})

	t.Run("Serial-Show", func(t *testing.T) {
		for _, toLine := range []to.To { to.P1, to.I4, to.T4 } {
			now, _ := base.UTCNow()
			req := to.NewComm(toLine, 0, now)
			req.GetSerial()
			bytes := req.Bytes()
			_, err := dongle.Request(bytes)
			tt.Ok(err)
			t.Logf("req serial: %s% 02x%s", base.Yellow, bytes, base.Reset)
			time.Sleep(500 * time.Millisecond)
		}
		sb := &strings.Builder{}
		resp.c.Json(sb)
		t.Logf("%s%s%s", base.Green, sb.String(), base.Reset)
		// for every solicited serial response so far, request show for that serial-number box
		resp.Serials(func(lineF from.From, box byte, serial []byte) {
			t.Logf("%sbox line=%02x box=%d serial=%02x%s", base.Blue, lineF, box, serial, base.Reset)
			now, _ := base.UTCNow()
			req, err := to.NewSerial(serial, now)
			tt.Ok(err)
			bytes := req.ShowBytes()
			_, err = dongle.Request(bytes)
			tt.Ok(err)
			t.Logf("req show: %s% 02x%s", base.Yellow, bytes, base.Reset)
			time.Sleep(500 * time.Millisecond)
		})
	})
	closer<- true
	time.Sleep(1 * time.Second)
}

// TestUsbReal requires a real dongle present at /dev/ttyUSB0
// At single PM,I4,T4 with any id must be connected to respond to requests
func TestRealUsb(t *testing.T) {
	tt := base.Test{t}
	count := 0
	respF := func(bytes []byte) {
		if len(bytes) >= 4 {
			t.Logf("resp: %s% 02x%s", base.Green, bytes, base.Reset)
			count++
		}
	}
	end := make(chan bool)
	resp := make(chan from.Resp, 1)
	go func() {
		for {
			select {
			case r := <-resp:
				respF(r.Bytes())
			case <-end:
				return
			}
		}
	}()
	net := byte(1)
	usb := NewUsb1("/dev/ttyUSB0", 115200, resp)
	closer, err := usb.OpenPort(net)
	if err != nil {
		t.Skipf("%sError %v%s", base.Yellow, err, base.Reset)
		return
	}

	toPM := byte(to.P1)
	toI4 := byte(to.I4)
	toT4 := byte(to.T4)
	type R struct { r []byte; name string; ms time.Duration }
	reqs := []R{
		R{ r:[]byte{toPM, 0, 0, byte(milk.Get00_Version) }, name:"pm version",  ms: 200 },
		R{ r:[]byte{toPM, 0, 0, byte(milk.Get01_Serial)  }, name:"pm serial",   ms: 200 },
		R{ r:[]byte{toPM, 0, 0, byte(milk.Get02_Status)  }, name:"pm status",   ms: 200 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get03_Config)    }, name:"pm cfg(old)", ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get05_Values), 1 }, name:"pm values",   ms: 1800 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get06_Averages)  }, name:"pm averages", ms: 1000 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get07_Calibr)    }, name:"pm calibr",   ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get09_Options)   }, name:"pm options",  ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get11_Profiles)  }, name:"pm profiles", ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get13_Display)   }, name:"pm display",  ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get15_Config)    }, name:"pm config",   ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get17_Alarms)    }, name:"pm alarms",   ms: 500 },
		R{ r:[]byte{toPM, 0, 0, byte(p1.Get19_Stalls)    }, name:"pm stalls",   ms: 500 },

		R{ r:[]byte{toI4, 0, 0, byte(milk.Get00_Version) }, name:"i4 version",  ms: 200 },
		R{ r:[]byte{toI4, 0, 0, byte(milk.Get01_Serial)  }, name:"i4 serial",   ms: 200 },
		R{ r:[]byte{toI4, 0, 0, byte(milk.Get02_Status)  }, name:"i4 status",   ms: 200 },
		R{ r:[]byte{toI4, 0, 0, byte(i4.Get03_Config)    }, name:"i4 cfg",      ms: 500 },
		
		R{ r:[]byte{toT4, 0, 0, byte(milk.Get00_Version) }, name:"t4 version",  ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(milk.Get01_Serial)  }, name:"t4 serial",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(milk.Get02_Status)  }, name:"t4 status",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get03_Stalls)    }, name:"t4 stall",    ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get04_Equipment) }, name:"t4 equipm",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get05_Milking)   }, name:"t4 milkin",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get06_Profiles)  }, name:"t4 profiles", ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get07_Colors)    }, name:"t4 colors",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get08_Palette)   }, name:"t4 palette",  ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get15_Levels)    }, name:"t4 levels",   ms: 200 },
		R{ r:[]byte{toT4, 0, 0, byte(t4.Get16_Rotary)    }, name:"t4 rotary",   ms: 200 },
	}
	for _, r := range reqs {
		n, err := usb.Request(r.r)
		tt.Ok(err)
		t.Logf(" req: %s% 02x send bytes:%d%s %s", base.Yellow, r.r, n, base.Reset, r.name)
		time.Sleep(r.ms * time.Millisecond)
	}
	time.Sleep(1 * time.Second)
	end<- true
	closer<- true
	time.Sleep(1 * time.Second)
}

type resp struct {
	t       *testing.T
	c       *srv.Counter
	comms   []from.Comm
	serials []from.Serial
	m       sync.Mutex
}

func newResp(t *testing.T) *resp {
	return &resp{
		t:       t,
		c:       &srv.Counter{},
		comms:   make([]from.Comm, 0),
		serials: make([]from.Serial, 0),
	}
}

// implements from.Rx
func (r *resp) RxError(err error) {
	r.m.Lock()
	defer r.m.Unlock()
	r.c.Error()
}

// implements from.Rx
func (r *resp) RxHeartbeat() {
	r.m.Lock()
	defer r.m.Unlock()
	r.c.Heartbeat()
}

// implements from.Rx
func (r *resp) RxResp(resp from.Resp) (err error) {
	r.m.Lock()
	defer r.m.Unlock()
	r.c.Resp(resp)
	switch resp.(type) {
	case *from.Serial:
		serial := resp.(*from.Serial)
		r.serials = append(r.serials, serial.Clone())
	case *from.Comm:
		comm := resp.(*from.Comm)
		r.comms = append(r.comms, comm.Clone())
	default:
		err = fmt.Errorf("Invalid resp type %T", resp)
		r.t.Fatalf("%s%v%s", base.Red, err, base.Reset)
	}
	return
}

func (r *resp) CommSols(f func(line from.From, src byte, sol milk.Sol, extra []byte)) {
	r.m.Lock()
	defer r.m.Unlock()
	for _, c := range r.comms {
		if line, err := c.From(); err != nil {
			continue
		} else if _, sol, _, err := c.Comm(); err != nil {
			continue
		} else if sol != nil {
			f(line, c.BoxId(), *sol, c.Extra())
		}
	}
}

func (r *resp) CommUnsols(f func(line from.From, src byte, unsol milk.Unsol, extra []byte)) {
	r.m.Lock()
	defer r.m.Unlock()
	for _, c := range r.comms {
		if line, err := c.From(); err != nil {
			continue
		} else if _, _, unsol, err := c.Comm(); err != nil {
			continue
		} else if unsol != nil {
			f(line, c.BoxId(), *unsol, c.Extra())
		}
	}
}

func (r *resp) Serials(f func(line from.From, src byte, serial []byte)) {
	r.m.Lock()
	defer r.m.Unlock()
	for _, s := range r.serials {
		if line, err := s.From(); err != nil {
			continue
		} else {
			f(line, s.BoxId(), s.Serial())
		}
	}
}

func (r *resp) String() string {
	r.m.Lock()
	defer r.m.Unlock()
	var sb strings.Builder
	r.c.Json(&sb)
	return sb.String()
}




