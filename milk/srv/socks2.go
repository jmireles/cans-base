//go:build linux
// +build linux

package srv

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/jmireles/cans-base"
	"gitlab.com/jmireles/cans-base/milk/from"
	"gitlab.com/jmireles/cans-base/socket"
)

type Socks2 struct {
	socks []*Sock
}

func NewSocks2(c []string, resp chan from.Resp) (sock *Socks2, err error) {
	s := &Socks2{
		socks: make([]*Sock, 0),
	}
	if len(c) > 0 && c[0] != "" {
		s.socks = append(s.socks, NewSock(c[0], resp))
	}
	if len(c) > 1 && c[1] != "" {
		s.socks = append(s.socks, NewSock(c[1], resp))
	}
	if len(s.socks) == 0 {
		return nil, errors.New("No CANs")
	}
	return s, nil
}

func (s *Socks2) Socks() []*Sock {
	return s.socks
}

type Sock struct {
	name   string
	counts *Counts
	close  chan bool
}

func NewSock(name string, resp chan from.Resp) *Sock {
	counts := NewCounts()
	counts.AddListener(resp)
	return &Sock{
		name:   name,
		counts: counts,
		close:  make(chan bool, 1),
	}
}

func (s *Sock) Open(net byte) error {
	conn, err := socket.DialContextCan(context.Background(), s.name)
	if err != nil {
		return err
	}
	rxC := make(chan socket.Can, 1)
	rxErrC := make(chan error, 1)
	go func() { // consumer
		for {
			select {
			case can := <-rxC:
				if bytes, resp, err := SocketCanResp(can); err != nil {
					// message error
					s.counts.RxError(err)
				} else if resp {
					if now, err := base.UTCNow(); err != nil {
						s.counts.RxError(err)
					} else if resp, err := from.NewResp(net, bytes, now); err != nil {
						// response error
						s.counts.RxError(err)
					} else {
						// response ok (sol or unsol)
						s.counts.RxResp(resp)
					}
				} else {
					// drop requests
				}
			case <-rxErrC:
				return
			case <-s.close:
				return
			}
		}
	}()
	go func() { // receiver
		recv := socket.NewRx(conn)
		for recv.Scan() {
			// a frame was received
			rxC <-recv.Read()
		}
		if err := recv.Err(); err != nil {
			// receiver stop receiving frames
			rxErrC <-errors.New(fmt.Sprintf("Recv error %v", err.Error()))
			return
		} else {
			rxErrC <-errors.New("Recv stopped with no errors")
			return
		}
	}()
	return nil
}

func (s *Sock) Close() {
	if s.close != nil {
		s.close <- true
	}
}

func (s *Sock) Request(bytes []byte) (uint, error) {
	if can, err := NewSocketCan(bytes); err != nil {
		return 0, err
	} else if can == nil {
		return 0, errors.New("CAN is nil")
	} else if conn, err := socket.DialContextCan(context.Background(), s.name); err != nil {
		return 0, fmt.Errorf("req: name %s not available %w", s.name, err)
	} else {
		tx := socket.NewTx(conn)
		if err := tx.Write(context.Background(), *can); err != nil {
			return 0, err
		} else {
			return uint(len(bytes)), nil
		}
	}
}

func (s *Sock) Json(sb *strings.Builder) {
	sb.WriteString(`{`)
	sb.WriteString(fmt.Sprintf(`"name":%q`, s.name))
	sb.WriteString(`,"data":`)
	s.counts.Json(sb)
	sb.WriteString(`}`)
}

// SocketCanResp coverts a socket CAN to bytes
//	     0        1        2      3    4      5        6        7        8        9     10 11
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┬──┬──┐
//	│LLLL0000     0        S    comm  xx     xx       xx       xx       xx       xx     xx xx│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┴──┴──┘
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┐
//	│YYYMMMMD DDDDSSSS CCCCCCCC  31   sub 0000LLLL YYYMMMMD DDDDSSSS CCCCCCCC IIIIIIIII│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┘
func SocketCanResp(can socket.Can) (bytes []byte, resp bool, err error) {
	if !can.IsExtended {
		err = errors.New("CAN frame not extended") // is not 29 bits!
		return
	} else if invalid := can.Validate(); invalid != nil {
		err = invalid
		return
	}
	bytes = make([]byte, 12)
	id := can.ID // 29 bits first four bytes
	bytes[3] = byte(id & 0x1f) // comm  ------------------------xxxxx
	id >>= 5
	bytes[2] = byte(id & 0xff) // src   ----------------SSSSSSSS
	id >>= 8
	bytes[1] = byte(id & 0xff) // dst   --------00000000
	id >>= 8
	bytes[0] = byte(id & 0xff) // lines LLLL0000
	copy(bytes[4:], can.Data[:])   // 0 to 8 data bytes
	
	serial := bytes[3] == 0x1f
	if !serial && bytes[0] >= 0x10 { // not serial and with valid "from"
		resp = true
	}
	if serial && bytes[4] == 0 { // serial and sub=0
		resp = true
	}
	return
}

// TODO passed to cans-base/lines/can.go this can be deleted and used copied
// NewSocketCan coverts bytes into a socket CAN
//	     0        1        2      3    4      5        6        7        8        9     10 11
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┬──┬──┐
//	│0000LLLL     D        0    comm  xx     xx       xx       xx       xx       xx     xx xx│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┴──┴──┘
func NewSocketCan(bytes []byte) (*socket.Can, error) {
	if len(bytes) < 4 {
		return nil, errors.New("bytes length lower than four")
	}
	id := uint32(0)
	id |= uint32(bytes[3] & 0x1f) // comm  ------------------------xxxxx
	id |= uint32(bytes[2]) << 5   // src   ----------------00000000
	id |= uint32(bytes[1]) << 13  // dst   --------DDDDDDDD
	id |= uint32(bytes[0]) << 21  // lines 0000LLLL
	length := uint8(len(bytes) - 4)
	if length > 8 {
		return nil, errors.New("data length greater than eight")
	}
	var data socket.Data
	copy(data[:], bytes[4:])
	can := socket.Can{
		ID:         id,
		Length:     length,
		Data:       data,
		IsExtended: true,
	}
	if err := can.Validate(); err != nil {
		return nil, err
	}
	return &can, nil
}

// NewSocketCanFrom coverts from CAN into a socket CAN
//	     0        1        2      3    4      5        6        7        8        9     10 11
//	┌────────┬────────┬────────┬────┬────┬────────┬────────┬────────┬────────┬─────────┬──┬──┐
//	│0000LLLL     D        0    comm  xx     xx       xx       xx       xx       xx     xx xx│
//	└────────┴────────┴────────┴────┴────┴────────┴────────┴────────┴────────┴─────────┴──┴──┘
func NewSocketCanFrom(f *from.Can) (*socket.Can, error) {
	comm := byte(0)
	if f.Unsol != nil {
		comm = byte(*f.Unsol)
	} else if f.Sol != nil {
		comm = byte(*f.Sol)
	} else {
		return nil, fmt.Errorf("invalid unsol/sol")
	}
	id := uint32(0)
	id |= uint32(comm & 0x1f)            // comm  ------------------------xxxxx
	id |= uint32(f.Src.Box)        << 5  // src   ----------------00000000
	id |= uint32(0)                << 13 // dst   --------DDDDDDDD
	id |= uint32(byte(f.Src.Line)) << 21 // lines 0000LLLL
	length := uint8(len(f.Extra))
	if length > 8 {
		return nil, fmt.Errorf("data length greater than eight")
	}
	var data socket.Data
	copy(data[:], f.Extra)
	can := socket.Can{
		ID:         id,
		Length:     length,
		Data:       data,
		IsExtended: true,
	}
	if err := can.Validate(); err != nil {
		return nil, err
	}
	return &can, nil
}


