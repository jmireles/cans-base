package srv

import (
	"encoding/json"
)

// ApiNets is what a cans server response as health metrics
type ApiNets struct {
	// Error describes an server api health HTTP request error.
	// When this Error field is not nil, the rest of fields are not valid.
	Error error `json:"error,omitempty"`

	// Version field returns the cans-server software app version.
	Version string `json:"version"`

	// Since returns the epoch since cans-server is running.
	Since int64 `json:"since"`

	// Server contains ports, tcps and records metrics.
	Server ApiServer `json:"server,omitempty"`
}

type ApiServer struct {
	// Ports is an array of Port of type CAN configured and running for the can-server app.
	Ports []ApiPort `json:"ports"`

	// Tcps is the service for responses through TCP clients
	Tcps *ApiTcps  `json:"tcps,omitempty"`
	
	// Recs is the recording service to store requests and responses in a filesystem.
	Recs *ApiRecs  `json:"recs,omitempty`
}

type ApiTcps struct {
	// Addr is the TCP port accepting clients to receive CAN responses
	Addr string `json:"addr"`

	// Conns is the array of clients actually being served.
	Conns []bool `json:"conns"`
}

type ApiRecs struct {
	// Path is the filesystem path where records are stored.
	Path string `json:"path"`

	// Counts counts the recording metrics.
	Counts ApiCounts `json:"counts"`
}

type ApiPort struct {
	// Open informs if the port is opened.
	Open  bool    `json:"open"`
	// Net has the consecutive number of a CAN network
	Net   int     `json:"net"`
	// Error shows the last error of the port.
	Error string  `json:"error,omitempty"`
	// Usb when not nil has the USB type CAN port.
	Usb   *apiCan `json:"usb,omitempty"`
	// Spi4 when not nil has the SPI4 type CAN ports.
	Spi4  *apiCan `json:"spi4,omitempty"`
	// Usb when not nil has the USB type CAN port.
	Sock  *apiCan `json:"sock,omitempty`
}

type apiCan struct {
	// Name identify the CAN port.
	Name string  `json:"name"`
	
	// Counts counts the CAN port metrics
	Counts ApiCounts `json:"counts"`
}

type ApiCounts struct {
	// Reqs counts the number of CAN requests.
	Reqs   int `json:"reqs"`
	// Sols counts the number of CAN solicited responses.
	Sols   int `json:"sols"`
	// Unsols counts the number of CAN unsolicited responses.
	Unsols int `json:"unsols"`

	// Hbs counts the number of heartbeats for ports
	Hbs    int `json:"hbs,omitempty"`
	// Errs counts the number of CAN request/response errors.
	Errs   int `json:"errs,omitempty"`
	// Boxes counts the number of CAN serial requests for ports
	Boxes  int `json:"boxes,omitempty"`
}

func NewApiCounts(jsonString string) (*ApiCounts, error) {
	api := &ApiCounts{}
	err := json.Unmarshal([]byte(jsonString), api)
	return api, err
}

type ApiNet struct {
	Error error    `json:"error,omitempty"`
	Ports []string `json:"ports,omitempty"`
	//Port  *Port    `json:"port,omitempty"`
}

