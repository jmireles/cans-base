package milk

import (
	"fmt"
)

// Product line
const (
	PC = "pc" // computer
	P1 = "p1" // pulsator monitor
	TO = "to" // single takeoff
	MM = "mm" // milk-meter
	RF = "rf" // rfid
	SO = "so" // sorter
	RS = "rs" // rotary stall identifier
	T4 = "t4" // four takeoffs
	I4 = "i4" // four inputs
)

// Get is a Command used by to-requests to get (read) cfg parameters.
type Get byte

// Set is a Command used by to-requests to set (write) cfg parameters.
type Set byte

// Sol is a Command used by from-responses fired after a Get was received.
type Sol byte

// Unsol is a Command used by from-responses fired unsolicited.
type Unsol byte


const (
	Get00_Version Get = 0
	Get01_Serial  Get = 1
	Get02_Status  Get = 2

	Get28_IapRun  Get = 28
	Get29_IapPkg  Get = 29
	Get30_IapEnd  Get = 30
	Get31_Serial  Get = 31
)

const (
	Sol00_Version    Sol   = 0
	Unsol01_Restart  Unsol = 1
	Sol02_Status     Sol   = 2
	Sol30_IapReady   Sol   = 30
	Sol31_Serial     Sol   = 31
)

const (
	Serial00_Get  byte = 0x00
	Serial02_Set  byte = 0x02
	Serial03_Show byte = 0x03
)

// CAN data is from 4 to 12 bytes long.
//
//	    0     1     2     3      4    5    6    7    8    9   10   11
//	+------+-----+-----+------+----+----+----+----+----+----+----+----+
//	| line | dst | src | comm | e1 | e2 | e3 | e4 | e5 | e6 | e7 | e8 |
//	+------+-----+-----+------+----+----+----+----+----+----+----+----+
type Data []byte

func NewData(data []byte) (Data, error) {
	if len(data) < 4 {
		return nil, fmt.Errorf("data len < 4")
	}
	if len(data) > 12 {
		return nil, fmt.Errorf("data len > 12")	
	}
	return Data(data), nil
}

func (d Data) Bytes() []byte {
	return d
}

func (d Data) Line() byte {
	return d[0]
}

func (d Data) Dst() byte{
	return d[1]
}

func (d Data) Src() byte{
	return d[2]
}

func (d Data) Comm() byte {
	return d[3]
}

func (d Data) Extra() []byte {
	return d[4:]
}

func (d Data) Clone() Data {
	d2 := make([]byte, len(d))
	copy(d2, d)
	return Data(d2)
}

type SolResp func(sol Sol, extra []byte)

type SimSols interface {

	Req(data Data, resp SolResp)
}



