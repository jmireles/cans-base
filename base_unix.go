// +build !windows

package base

const (
	Blue   = "\u001b[34m"
	Green  = "\u001b[32m"
	Red    = "\u001b[31m"
	Reset  = "\u001b[0m"
	Yellow = "\u001b[33m"
	Return = "\033[0K\r"
)

