package base

import (
	"fmt"
	"time"
)

type Log func(string, ...any)

func LogF(topic, color string) Log {
	return func(debug string, args ...any) {
		now := time.Now().Format("15:04:05.000")
		text := fmt.Sprintf(debug, args...)
		fmt.Printf("%s %s: %s%s%s\n", now, topic, color, text, Reset)
	}
}

func LogBlue(debug string, args ...any) {
	fmt.Print(Blue)
	fmt.Printf(debug, args...)
	fmt.Print(Reset)
}

func LogGreen(debug string, args ...any) {
	fmt.Print(Green)
	fmt.Printf(debug, args...)
	fmt.Print(Reset)
}

func LogYellow(debug string, args ...any) {
	fmt.Print(Yellow)
	fmt.Printf(debug, args...)
	fmt.Print(Reset)
}
