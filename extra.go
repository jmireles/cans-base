package base

import (
	"fmt"
)

const ExtraCanDataMaxLength = 8

type Extra [ExtraCanDataMaxLength]byte

func ExtraValid(extra []byte) error {
	if len(extra) > 8 {
		return fmt.Errorf("extra length greater than 8")
	}
	return nil
}



