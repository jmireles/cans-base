package dom

import (
	"testing"

	"gitlab.com/jmireles/cans-base"
)

func TestOnclick(t *testing.T) {

	t1 := base.Test{t}

	uri := "?req"
	id := NewId("id")
	value := NewId("value")
	ons := []Class{
		NewClass("on1"),
		NewClass("on2"),
	}
	offs := []Class{
		NewClass("off1"),
		NewClass("off2"),
		NewClass("off3"),
	}

	a1 := NewOnclickAjaxValue(uri, id, value, ons, offs)
	t1.Equals("a1('?req','id','value',['on1','on2'],['off1','off2','off3'],{})", string(a1))

	inner2 := "inner-2"
	value2 := "value-2"
	class2 := Class("class-2")
	set := map[Id]OnclickSets {
		NewId("id1"): NewOnclickValsInnerEmpty(),
		NewId("id2"): NewOnclickValsDirect(inner2, value2, class2),
	}
	
	s2 := NewOnclickSets(ons, offs, set)
	t1.Equals("s2(['on1','on2'],['off1','off2','off3'],{'id1':{'h':''},'id2':{'h':'inner-2','v':'value-2','c':'class-2'}})", string(s2))
}